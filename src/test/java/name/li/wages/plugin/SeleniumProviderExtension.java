package name.li.wages.plugin;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

import name.li.wages.selenium.SeleniumProvider;

public class SeleniumProviderExtension implements ParameterResolver, BeforeAllCallback, AfterAllCallback {

	private SeleniumProvider selenium;

	@Override
	public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
			throws ParameterResolutionException {
		return SeleniumProvider.class.isAssignableFrom(parameterContext.getParameter().getType());
	}

	@Override
	public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
			throws ParameterResolutionException {
		return selenium;
	}

	@Override
	public void beforeAll(ExtensionContext context) throws Exception {
		this.selenium = new SeleniumProvider("http://localhost:4444/wd/hub");
	}

	@Override
	public void afterAll(ExtensionContext context) throws Exception {
		this.selenium = null;
	}

}
