package name.li.wages.plugin;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.junit.jupiter.MockServerExtension;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.openqa.selenium.WebDriver;

import name.li.wages.selenium.SeleniumProvider;

@Tag("slow")
@ExtendWith(MockServerExtension.class)
@ExtendWith(SeleniumProviderExtension.class)
public class MockServerTest {

	private ClientAndServer mockServer;
	private WebDriver seleniumDriver;

	public MockServerTest(ClientAndServer mockServer, SeleniumProvider selenium) {
		this.mockServer = mockServer;
		this.seleniumDriver = selenium.getPooledDriver();
	}

	public void respondWithFileContentOnRequest(String filePath, String requestPath) throws IOException {
		mockServer
				.when(HttpRequest.request()
						.withMethod("GET")
						.withPath("/" + requestPath))
				.respond(HttpResponse
						.response(Files.readString(
								new File("src/test/resources").toPath().resolve(filePath)))
						.withHeader("Content-Type", "charset=utf-8"));
	}

	public WebDriver openPage(String path) throws MalformedURLException {
		seleniumDriver.navigate().to(new URL("http", "172.17.0.1", mockServer.remoteAddress().getPort(), "/" + path));
		return seleniumDriver;
	}

	public ClientAndServer getMockServer() {
		return mockServer;
	}

}
