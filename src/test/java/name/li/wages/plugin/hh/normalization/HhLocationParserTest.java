package name.li.wages.plugin.hh.normalization;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import name.li.wages.plugin.api.Location;

public class HhLocationParserTest {

	@Test
	public void shouldParseHhLocations() {
		var parser = new HhLocationParser("russia");

		assertThat(parser.parseLocation("Москва"))
				.isEqualTo(Location.builder()
						.setCountry("russia")
						.setCity("Москва")
						.withoutArea()
						.build());

		assertThat(parser.parseLocation("Москва, Китай-город и еще 1 "))
				.isEqualTo(
						Location.builder()
								.setCountry("russia")
								.setCity("Москва")
								.setArea("Китай-город и еще 1")
								.build());
	}
}
