package name.li.wages.plugin.hh.process;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.common.parsing.SearchResultsParser;
import name.li.wages.plugin.hh.parsing.HhClient;
import name.li.wages.plugin.hh.parsing.HhClient.Location;
import name.li.wages.plugin.hh.parsing.HhClient.SearchConfiguration;
import name.li.wages.plugin.hh.persistence.HhRawJobEntry;
import name.li.wages.plugin.hh.persistence.HhRawJobEntryRepository;

@ExtendWith(MockitoExtension.class)
public class HhSearchParsingProcessTest {

	@Mock
	private HhRawJobEntryRepository rawJobEntryRepository;

	@Mock
	private HhClient hhClient;

	@Mock
	private PlatformTransactionManager tm;

	@Mock
	private SearchResultsParser<HhRawJobEntry> parser;

	private SearchConfiguration searchConfiguration = SearchConfiguration.builder()
			.setTerm("foo")
			.setLocation(Location.RYAZAN)
			.build();

	@Test
	public void testParsing() {
		when(hhClient.getSearchResultForLocationAndTerm(Mockito.any(), Mockito.any()))
				.thenReturn(parser);
		HhRawJobEntry rawEntry = new HhRawJobEntry();
		when(parser.generate()).thenReturn(Stream.of(rawEntry));
		var generation = Generation.create("fooGen");
		var process = new HhSearchParsingProcess(rawJobEntryRepository, searchConfiguration,
				hhClient, tm);

		process.createExecution(Optional.empty(), Optional.empty(), generation).start();

		var savedEntryCaptor = ArgumentCaptor.forClass(HhRawJobEntry.class);
		verify(rawJobEntryRepository).save(savedEntryCaptor.capture());
		var savedEntry = savedEntryCaptor.getValue();
		assertThat(savedEntry).isEqualTo(rawEntry);
		assertThat(savedEntry.getGeneration()).isEqualTo(generation);

	}

}
