package name.li.wages.plugin.hh.normalization;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Navigation;
import org.openqa.selenium.WebElement;

import name.li.wages.plugin.api.Buzzword;
import name.li.wages.plugin.hh.persistence.HhRawJobEntry;

@ExtendWith(MockitoExtension.class)
public class HhEnricherTest {

	@Mock
	private WebDriver selenium;
	@Mock
	private Navigation seleniumNavigation;
	@Mock
	private WebElement descriptionElement;

	private HhRawJobEntry entryToEnrich = new HhRawJobEntry();
	private HhEnricher enricher = new HhEnricher(() -> selenium);

	@BeforeEach
	public void init() {
		when(selenium.navigate()).thenReturn(seleniumNavigation);
		entryToEnrich.setJobUrl("far.away");
	}

	@Test
	public void testArchivedEntry() {
		when(selenium.findElement(By.className("vacancy-archive-description")))
				.thenReturn(Mockito.mock(WebElement.class));

		enricher.enrich(entryToEnrich);

		verify(seleniumNavigation).to(entryToEnrich.getJobUrl());
		assertThat(entryToEnrich.getLongDescription()).isEmpty();
		assertThat(entryToEnrich.getBuzzwords()).isEmpty();
		assertThat(entryToEnrich.isArchieved()).isTrue();
	}

	@Test
	public void testLongDescriptionIsFilled() {
		when(selenium.findElement(By.className("vacancy-archive-description")))
				.thenThrow(new NoSuchElementException(""));
		when(selenium.findElement(By.className("vacancy-description"))).thenReturn(descriptionElement);
		when(descriptionElement.getText()).thenReturn("long description");
		var skills = Arrays.asList(mockSkillElement("java"), mockSkillElement("spring"));
		when(selenium.findElements(By.cssSelector("[data-qa~='skills-element']"))).thenReturn(skills);

		enricher.enrich(entryToEnrich);

		verify(seleniumNavigation).to(entryToEnrich.getJobUrl());
		assertThat(entryToEnrich.getLongDescription()).isEqualTo("long description");
		assertThat(entryToEnrich.getBuzzwords()).containsExactly(Buzzword.of("spring"), Buzzword.of("java"));
		assertThat(entryToEnrich.isArchieved()).isFalse();

	}

	private WebElement mockSkillElement(String skill) {
		var mock = mock(WebElement.class);
		when(mock.getText()).thenReturn(skill);
		return mock;
	}

}
