package name.li.wages.plugin.hh.normalization;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.OptionalInt;

import org.junit.jupiter.api.Test;

import name.li.wages.plugin.api.Salary;
import name.li.wages.plugin.api.Salary.Period;
import name.li.wages.plugin.common.ParsingResult;

public class HhSalaryParserTest {

	private HhSalaryParser parser = new HhSalaryParser();

	@Test
	public void testIntervalParsing() {
		assertThat(parser.parse("Q__100 - 200 руб.")).isEqualTo(ParsingResult.failed());
		assertThat(parser.parse("100 - 200руб.")).isEqualTo(parsingResult(100, 200, "RUB"));
		assertThat(parser.parse("100 - 200 грн.")).isEqualTo(parsingResult(100, 200, "UAH"));
		assertThat(parser.parse("300 00 0 -50 0000EUR")).isEqualTo(parsingResult(300_000, 500_000, "EUR"));
		assertThat(parser.parse("50 000-100 000 USD")).isEqualTo(parsingResult(50_000, 100_000, "USD"));
		assertThat(parser.parse("1 100-1 100 бел. руб.")).isEqualTo(parsingResult(1_100, 1_100, "BYR"));
	}

	@Test
	public void testRightOpenInterval() {
		assertThat(parser.parse("отидо100 руб.")).isEqualTo(ParsingResult.failed());
		assertThat(parser.parse("от 100 руб.")).isEqualTo(parsingResult(100, 0, "RUB"));
		assertThat(parser.parse("от 100 000 EUR")).isEqualTo(parsingResult(100_000, 0, "EUR"));
		assertThat(parser.parse("от 120 000 KZT")).isEqualTo(parsingResult(120_000, 0, "KZT"));
		assertThat(parser.parse("от 400 000 KZT")).isEqualTo(parsingResult(400_000, 0, "KZT"));// nbsp char
	}

	@Test
	public void testLeftOpenInterval() {
		assertThat(parser.parse("отидо100 руб.")).isEqualTo(ParsingResult.failed());
		assertThat(parser.parse("до100 руб.")).isEqualTo(parsingResult(0, 100, "RUB"));
		assertThat(parser.parse("до100 000 EUR")).isEqualTo(parsingResult(0, 100_000, "EUR"));
		assertThat(parser.parse("до 120 000 руб.")).isEqualTo(parser.parse("до 120 000 руб."));
	}

	@Test
	public void testUnknownCurrency() {
		assertThrows(IllegalStateException.class, () -> parser.parse("от 100 RUB"));
	}

	@Test
	public void testUndefined() {
		assertThat(parser.parse("з/п не указана")).isEqualTo(ParsingResult.matched());
	}

	private ParsingResult<Salary> parsingResult(int min, int max, String currency) {
		return ParsingResult.parsed(Salary.builder()
				.setPeriod(Period.MONTH)
				.setMinSalary(min == 0 ? OptionalInt.empty() : OptionalInt.of(min))
				.setMaxSalary(max == 0 ? OptionalInt.empty() : OptionalInt.of(max))
				.setCurrency(currency).build());
	}

}
