package name.li.wages.plugin.hh.parsing;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.util.Optional;

import javax.ws.rs.core.UriBuilder;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.openqa.selenium.remote.RemoteWebDriver;

import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.hh.HhConfiguration;
import name.li.wages.plugin.hh.parsing.HhClient.Location;
import name.li.wages.plugin.hh.parsing.HhClient.SearchConfiguration;
import name.li.wages.selenium.SeleniumProvider;

@ExtendWith(MockitoExtension.class)
public class HhClientTest {
	@Mock
	private HhSearchParserFactory parserFactory;
	@Mock
	private SeleniumProvider selenium;
	@Mock
	private RemoteWebDriver driver;
	@Mock
	private HhConfiguration hhConfig;

	private HhClient hhClient;

	private SearchConfiguration searchConfig;

	@BeforeEach
	public void init() {
		when(hhConfig.defaultSearchPeriod()).thenReturn(2L);
		hhClient = new HhClient(selenium, parserFactory, hhConfig);
		searchConfig = SearchConfiguration.builder()
				.setLocation(Location.RYAZAN)
				.setTerm("truth")
				.build();
	}

	@Test
	public void searchPeriodIsDefaultIfNoLastGenerationPresent() {
		hhClient.getSearchResultForLocationAndTerm(
				searchConfig,
				Optional.empty());
		var urlCaptor = ArgumentCaptor.forClass(URI.class);

		verify(parserFactory).newSearch(urlCaptor.capture(), Mockito.any(), Mockito.eq(searchConfig));

		assertThat(urlCaptor.getValue()).isEqualTo(
				UriBuilder.fromPath("https://hh.ru/search/vacancy")
						.queryParam("st", "searchVacancy")
						.queryParam("search_period", 2)
						.queryParam("area", Location.RYAZAN.code())
						.queryParam("text", "truth").build());
	}

	@Test
	public void searchPeriodCalculatedFromLastGeneration() {
		hhClient.getSearchResultForLocationAndTerm(
				searchConfig,
				Optional.of(Generation.create("foo")));
		var urlCaptor = ArgumentCaptor.forClass(URI.class);

		verify(parserFactory).newSearch(urlCaptor.capture(), Mockito.any(), Mockito.eq(searchConfig));

		assertThat(urlCaptor.getValue()).isEqualTo(
				UriBuilder.fromPath("https://hh.ru/search/vacancy")
						.queryParam("st", "searchVacancy")
						.queryParam("search_period", 1)
						.queryParam("area", Location.RYAZAN.code())
						.queryParam("text", "truth").build());

	}

}
