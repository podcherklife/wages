package name.li.wages.plugin.hh.process;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.hh.normalization.HhEnricher;
import name.li.wages.plugin.hh.persistence.HhRawJobEntry;
import name.li.wages.plugin.hh.persistence.HhRawJobEntryRepository;

@ExtendWith(MockitoExtension.class)
public class HhDetailsParsingProcessTest {

	@Mock
	private HhRawJobEntryRepository rawJobEntryRepository;
	@Mock
	private PlatformTransactionManager tm;
	@Mock
	private HhEnricher enricher;

	private Generation generation;

	private HhDetailsParsingProcess process;

	@BeforeEach
	public void init() {
		this.process = new HhDetailsParsingProcess(rawJobEntryRepository, tm, enricher);
		this.generation = Generation.create("sampleGen");
	}

	@Test
	public void testNormalGeneration() {
		var rawEntryWithExtendedData = new HhRawJobEntry();
		rawEntryWithExtendedData.setHasExtendedData(true);
		var rawEntryWithoutExtendedData = new HhRawJobEntry();

		when(rawJobEntryRepository.getAllEntriesForGeneration(anyString()))
				.thenReturn(Arrays.asList(rawEntryWithExtendedData, rawEntryWithoutExtendedData));
		var parsingStep = process.createExecution(Optional.empty(), Optional.empty(), generation);

		parsingStep.start();

		assertThat(rawEntryWithoutExtendedData.hasExtendedData()).isTrue();
		verify(rawJobEntryRepository).getAllEntriesForGeneration(generation.getId());
		verify(rawJobEntryRepository).save(rawEntryWithoutExtendedData);
		verifyNoMoreInteractions(rawJobEntryRepository);
	}

}
