package name.li.wages.plugin.hh.parsing;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import name.li.wages.plugin.SeleniumProviderExtension;
import name.li.wages.plugin.hh.HhConfiguration;
import name.li.wages.plugin.hh.normalization.HhEnricher;
import name.li.wages.plugin.hh.parsing.HhClient.Location;
import name.li.wages.plugin.hh.parsing.HhClient.SearchConfiguration;
import name.li.wages.selenium.SeleniumProvider;

@Tag("slow")
@ExtendWith(SeleniumProviderExtension.class)
public class HhSanityTest {

	private final SeleniumProvider seleniumProvider;

	public HhSanityTest(SeleniumProvider seleniumProvider) {
		this.seleniumProvider = seleniumProvider;

	}

	@Test
	public void testSanity() {
		var hhClient = new HhClient(
				seleniumProvider,
				new HhSearchParserFactory(),
				new HhConfiguration(2));

		var items = hhClient.getSearchResultForLocationAndTerm(
				SearchConfiguration.builder()
						.setTerm("java")
						.setLocation(Location.RYAZAN)
						.build(),
				Optional.empty())
				.generate()
				.collect(Collectors.toList());

		assertThat(items).isNotEmpty();
		assertThat(items).allMatch(item -> !item.getTitle().isEmpty());
		assertThat(items).allMatch(item -> !item.getShortDescription().isEmpty());
		assertThat(items).allMatch(item -> item.getParsingProblems().isEmpty());

		var item = items.get(0);

		new HhEnricher(seleniumProvider::getPooledDriver).enrich(item);

		assertThat(item.getParsingProblems()).isEmpty();
		assertThat(item.getLongDescription()).isNotEmpty();

	}

}
