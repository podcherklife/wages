package name.li.wages.plugin.hh.parsing;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.mockserver.integration.ClientAndServer;

import name.li.wages.plugin.MockServerTest;
import name.li.wages.plugin.hh.parsing.HhClient.Location;
import name.li.wages.plugin.hh.parsing.HhClient.SearchConfiguration;
import name.li.wages.selenium.SeleniumProvider;

public class HhHtmlParsingTest extends MockServerTest {

	private static final String PAGE_2_RELATIVE_URL = "/search/vacancy?L_is_autosearch=false&clusters=true&enable_snippets=true&search_period=3&text=java&page=1";

	public HhHtmlParsingTest(ClientAndServer mockServer, SeleniumProvider selenium) {
		super(mockServer, selenium);
	}

	@Test
	public void testHtmlParsing() throws IOException {

		respondWithFileContentOnRequest(
				"hh/hh_search_result_page1.html", "page1");
		respondWithFileContentOnRequest(
				"hh/hh_search_result_page37.html", "page37");

		var driver = openPage("page1");

		var paginationHandler = new HhPaginationHandler();
		var nextPageUrl = paginationHandler.getNextPageUrl(driver);
		var totalPages = paginationHandler.getPagesTotal(driver);
		assertThat(totalPages).isEqualTo(36);
		assertThat(nextPageUrl).isPresent();
		assertThat(nextPageUrl.get()).endsWith(PAGE_2_RELATIVE_URL);

		var pageHandler = new HhSearchPageHandler(
				SearchConfiguration.builder().setLocation(Location.MOSCOW).setTerm("java").build());
		var item = pageHandler.parseItems(driver).skip(2).findFirst().get();
		assertThat(item.getCreationDate()).isEqualTo("14 апреля");
		assertThat(item.getTitle()).isEqualTo("Системный аналитик Java");
		assertThat(item.getLocation()).isEqualTo("Екатеринбург");
		assertThat(item.getCountry()).isEqualTo("Russia");
		assertThat(item.getShortDescription()).isEqualTo(
				"Выявление и анализ требований к ИС. "
						+ "Документирование требований к ИС. "
						+ "Согласование требований с заинтересованными лицами. "
						+ "Консультирование архитектора\\ разработчика на этапе...\n"
						+ "Опыт работы на аналогичной позиции не менее 3 лет в проектах на Java, C#, С++. "
						+ "Базовые знания методологий разработки Waterfall...");
		assertThat(item.getSearchedBy()).isEqualTo("java");
		assertThat(item.getExternalId()).isEqualTo("36634826");
		assertThat(item.getSalary()).isEqualTo("от 100 000 руб.");

		driver = openPage("page37");

		assertThat(paginationHandler.getNextPageUrl(driver)).isEmpty();

		assertThat(pageHandler.parseItems(driver))
				.anyMatch(job -> job.getTitle().equals("Фронтенд разработчик"));

	}

}
