package name.li.wages.plugin.hh.parsing;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;

public class HhUrlParserTest {

	@Test
	public void test() {
		var parser = new HhUrlIdExtractor();

		assertThat(parser.tryParse("")).isEqualTo(Optional.empty());
		assertThat(parser.tryParse("https://hhcdn.ru/click")).isEqualTo(Optional.empty());
		assertThat(parser.tryParse("https://ryazan.hh.ru/vacancy/2527622")).isEqualTo(Optional.of("2527622"));
		assertThat(parser.tryParse("https://novomichurinsk.hh.ru/vacancy/17330937/"))
				.isEqualTo(Optional.of("17330937"));
		assertThat(parser.tryParse("https://novomichurinsk.hh.ru/vacancy/36090859?query=java"))
				.isEqualTo(Optional.of("36090859"));
		assertThat(parser.tryParse("https://novomichurinsk.hh.ru/vacancy/17330937/qwe"))
				.isEqualTo(Optional.of("17330937"));
	}

}
