package name.li.wages.plugin.common.process;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.internal.stubbing.answers.Returns;
import org.mockito.internal.stubbing.answers.ReturnsArgumentAt;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.plugin.api.process.ProcessDescriptor.ContinuationToken;
import name.li.wages.plugin.api.process.Step;

@ExtendWith(MockitoExtension.class)
public class TransformTransactionallyStepTest {

	private static final String STEP_ID = "step_id";

	@Mock
	private PlatformTransactionManager tm;
	@Mock
	private Supplier<Collection<String>> supplier;
	@Mock
	private Consumer<String> consumer;
	@Mock
	private Function<String, String> transformer;

	private TransformTransactionallyStep<String, String> step;

	@BeforeEach
	public void init() {
		this.step = new TransformTransactionallyStep<>(STEP_ID, "step description", supplier, transformer, consumer,
				tm);
	}

	@Test
	public void testEmptyTransformation() {
		when(supplier.get()).thenReturn(Collections.emptyList());

		step.start();

		verifyNoInteractions(consumer);
	}

	@Test
	public void testNormalTransformation() {
		when(supplier.get()).thenReturn(Arrays.asList("foo", "bar"));
		when(transformer.apply(anyString())).thenAnswer(new ReturnsArgumentAt(0));

		step.start();

		verify(consumer, times(2)).accept(anyString());
	}

	@Test
	public void testContinuationTokenGeneration() {
		var supplierException = new RuntimeException();
		doThrow(supplierException).when(supplier).get();

		var exception = assertThrows(Step.ProcessError.class, () -> step.start());

		assertThat(exception.getCause())
				.isEqualTo(supplierException);
		assertThat(exception.getToken())
				.isEqualTo(ContinuationToken.builder().setStepId(STEP_ID).setValue(STEP_ID).build());
	}

	@Test
	public void testProgress() throws InterruptedException, BrokenBarrierException {
		var input = Arrays.asList("foo", "bar");
		when(supplier.get()).thenReturn(Arrays.asList("foo", "bar"));

		var stageStarted = new CyclicBarrier(2);
		var stageFinished = new CyclicBarrier(2);

		when(supplier.get())
				.thenAnswer(withBarriers(new Returns(input), stageStarted, stageFinished));
		when(transformer.apply(anyString()))
				.thenAnswer(withBarriers(new ReturnsArgumentAt(0), stageStarted, stageFinished));

		new Thread(() -> step.start()).start();

		assertThat(step.status().working()).isFalse();
		assertThat(step.status().percentsCompleted()).isEqualTo(0);

		stageStarted.await(); // supplier started
		stageFinished.await(); // supplier finished
		stageStarted.await(); // transformer1 started

		assertThat(step.status().working()).isTrue();
		assertThat(step.status().percentsCompleted()).isEqualTo(0);

		stageFinished.await(); // transformer1 finished
		stageStarted.await(); // transformer2 started

		assertThat(step.status().working()).isTrue();
		assertThat(step.status().percentsCompleted()).isEqualTo(50);

		stageFinished.await(); // transformer2 finished

	}

	private <T> Answer<T> withBarriers(Answer<T> answer, CyclicBarrier before, CyclicBarrier after) {
		return invocation -> {
			try {
				before.await();
				return answer.answer(invocation);
			} finally {
				after.await();
			}
		};
	}

}
