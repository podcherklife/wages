package name.li.wages.plugin.common.process;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.function.Consumer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.plugin.api.process.ProcessDescriptor.ContinuationToken;
import name.li.wages.plugin.api.process.Step;

@ExtendWith(MockitoExtension.class)
public class PersistTransactionallyStepTest {

	private static final String STEP_ID = "stepId";
	@Mock
	private Consumer<String> savingFunc;
	@Mock
	private PlatformTransactionManager tm;
	@Mock
	private SupplierWithStatus<String> providerFunc;

	private PersistTransactionallyStep<String> parsingStep;

	@BeforeEach
	public void init() {
		this.parsingStep = new PersistTransactionallyStep<String>(STEP_ID, providerFunc, savingFunc, tm);
	}

	@Test
	public void testNormalFlow() {
		when(providerFunc.get()).thenReturn("foo");

		parsingStep.start();

		verify(savingFunc).accept("foo");
	}

	@Test
	public void testFailedFlow() {
		var parserException = new RuntimeException();
		doThrow(parserException).when(providerFunc).get();

		var exception = assertThrows(Step.ProcessError.class, () -> parsingStep.start());

		assertThat(exception.getCause())
				.isEqualTo(parserException);
		assertThat(exception.getToken())
				.isEqualTo(ContinuationToken.builder().setStepId(STEP_ID).setValue(STEP_ID).build());
	}

}
