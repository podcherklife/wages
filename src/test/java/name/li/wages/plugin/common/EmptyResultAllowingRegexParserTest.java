package name.li.wages.plugin.common;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import java.util.Arrays;
import java.util.function.Consumer;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class EmptyResultAllowingRegexParserTest {

	private EmptyResultAllowingRegexParser<String> parser = new EmptyResultAllowingRegexParser<>(
			new CompositeRegexParser<>(Arrays
					.asList(new CompositeRegexParser.SingleRegexParser<>("(a)", matcher -> matcher.group(1)))),
			Arrays.asList("(b)"));

	@Mock
	private Consumer<String> consumer;
	@Mock
	private Runnable runnable;

	@Test
	void testWithResult() {
		parse("a");

		expectConsumerCalled("a");
		expectRunnableNotCalled();
	}

	@Test
	void testWithMatch() {
		parse("b");

		expectConsumerNotCalled();
		expectRunnableNotCalled();
	}

	@Test
	void testWithoutMatch() {
		parse("c");

		expectConsumerNotCalled();
		expectRunnableCalled();
	}

	private void parse(String str) {
		parser.tryParse(str).ifMatchedOrElse(consumer, runnable);
	}

	private void expectConsumerCalled(String str) {
		verify(consumer).accept(str);
	}

	private void expectConsumerNotCalled() {
		verifyNoInteractions(consumer);
	}

	private void expectRunnableCalled() {
		verify(runnable).run();
	}

	private void expectRunnableNotCalled() {
		verifyNoInteractions(runnable);
	}
}
