package name.li.wages.plugin.indeed.normalization;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import name.li.wages.plugin.api.Salary;
import name.li.wages.plugin.api.Salary.Period;
import name.li.wages.plugin.common.ParsingResult;

public class IndeedSalaryParserTest {

	@Test
	public void test() {

		var parser = new IndeedSalaryParser();

		assertThat(parser.parse("$75 - $85 an hour"))
				.isEqualTo(ParsingResult.parsed(Salary.builder()
						.setPeriod(Period.HOUR)
						.setCurrency("USD")
						.setMaxSalary(85)
						.setMinSalary(75)
						.build()));

		assertThat(parser.parse("$85 an hour"))
				.isEqualTo(ParsingResult.parsed(Salary.builder()
						.setPeriod(Period.HOUR)
						.setCurrency("USD")
						.setMaxSalary(85)
						.setMinSalary(85)
						.build()));

		assertThat(parser.parse("$140,000 - $145,000 a year"))
				.isEqualTo(ParsingResult.parsed(Salary.builder()
						.setPeriod(Period.YEAR)
						.setCurrency("USD")
						.setMaxSalary(145000)
						.setMinSalary(140000)
						.build()));

		assertThat(parser.parse("$140,000 a year"))
				.isEqualTo(ParsingResult.parsed(Salary.builder()
						.setPeriod(Period.YEAR)
						.setCurrency("USD")
						.setMaxSalary(140000)
						.setMinSalary(140000)
						.build()));

		assertThat(parser.parse(""))
				.isEqualTo(ParsingResult.matched());

		assertThat(parser.parse("meSsY stRiNg"))
				.isEqualTo(ParsingResult.failed());
	}

}
