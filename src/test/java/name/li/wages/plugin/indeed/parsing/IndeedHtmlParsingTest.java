package name.li.wages.plugin.indeed.parsing;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.mockserver.integration.ClientAndServer;

import name.li.wages.plugin.MockServerTest;
import name.li.wages.plugin.indeed.IndeedSearchConfiguration;
import name.li.wages.plugin.indeed.IndeedSearchConfiguration.Location;
import name.li.wages.selenium.SeleniumProvider;

public class IndeedHtmlParsingTest extends MockServerTest {

	private static final String PAGE_2_URL = "/jobs?q=Java&l=New+York+State&sort=date&limit=50&fromage=50&radius=25&start=150";

	public IndeedHtmlParsingTest(ClientAndServer mockServer, SeleniumProvider selenium) {
		super(mockServer, selenium);
	}

	@Test
	public void testHtmlParsing() throws IOException {

		respondWithFileContentOnRequest(
				"indeed/indeed_search_result_page1.html", "page1");
		respondWithFileContentOnRequest(
				"indeed/indeed_search_result_page20.html", "page20");

		var driver = openPage("page1");

		var paginationHandler = new IndeedPaginationHandler();
		var nextPageUrl = paginationHandler.getNextPageUrl(driver);
		var totalPages = paginationHandler.getPagesTotal(driver);
		assertThat(totalPages).isEqualTo(67);
		assertThat(nextPageUrl).hasValueSatisfying(e -> e.contains(PAGE_2_URL));

		var pageHandler = new IndeedSearchPageHandler(
				IndeedSearchConfiguration.builder().setLocation(Location.NEW_YORK).setTerm("java").build());
		var item = pageHandler.parseItems(driver).skip(3).findFirst().get();
		assertThat(item.getAge()).isEqualTo("Today");
		assertThat(item.getTitle()).isEqualTo("Integration Specialist");
		assertThat(item.getLocation()).isEqualTo("New York, NY");
		assertThat(item.getEmployer()).isEqualTo("Tekcogno");
		assertThat(item.getSearchedBy()).isEqualTo("java");
		assertThat(item.getExternalId()).isEqualTo("c270ea41a6533c09");
		assertThat(item.getJobUrl()).satisfies(s -> s.contains("/company/Tekcogno/jobs/Integration-Specialist-c270ea41a6533c09?fccid=360f48ee217c77a6&vjs=3"));
		assertThat(item.getSalary()).isEqualTo("$70,000 - $140,000 a year");
		assertThat(item.getDateParsed()).isEqualTo(LocalDate.now());

		driver = openPage("page20");

		assertThat(paginationHandler.getNextPageUrl(driver)).isEmpty();

		assertThat(pageHandler.parseItems(driver))
				.anyMatch(job -> job.getTitle().equals("Software engineer - Mulesoft developer"));

	}

}
