package name.li.wages.plugin.indeed.parsing;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import name.li.wages.plugin.SeleniumProviderExtension;
import name.li.wages.plugin.indeed.IndeedConfiguration;
import name.li.wages.plugin.indeed.IndeedSearchConfiguration;
import name.li.wages.plugin.indeed.IndeedSearchConfiguration.Location;
import name.li.wages.selenium.SeleniumProvider;

@Tag("slow")
@ExtendWith(SeleniumProviderExtension.class)
public class IndeedSanityTest {

	private final SeleniumProvider seleniumProvider;

	public IndeedSanityTest(SeleniumProvider seleniumProvider) {
		this.seleniumProvider = seleniumProvider;

	}

	@Test
	public void testSanity() {
		var indeedClient = new IndeedClient(
				seleniumProvider,
				new IndeedConfiguration(1));

		var items = indeedClient.getSearchResultForLocationAndTerm(
				IndeedSearchConfiguration.builder()
						.setTerm("java")
						.setLocation(Location.NEW_YORK)
						.build(),
				Optional.empty())
				.generate()
				.collect(Collectors.toList());

		assertThat(items).isNotEmpty();
		assertThat(items).allMatch(item -> !item.getTitle().isEmpty());
		assertThat(items).allMatch(item -> !item.getShortDescription().isEmpty());
		assertThat(items).allMatch(item -> !item.getJobUrl().isEmpty());
		assertThat(items).allMatch(item -> item.getParsingProblems().isEmpty());
		assertThat(items).hasSizeGreaterThan(IndeedClient.ITEMS_PER_PAGE);

	}

}
