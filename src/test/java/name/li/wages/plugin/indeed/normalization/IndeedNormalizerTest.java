package name.li.wages.plugin.indeed.normalization;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import name.li.wages.plugin.api.Location;
import name.li.wages.plugin.api.Salary;
import name.li.wages.plugin.api.Salary.Period;
import name.li.wages.plugin.indeed.persistence.IndeedRawJobEntry;

public class IndeedNormalizerTest {

	@Test
	public void test() {

		var normalizer = new IndeedNormalizer();

		var rawEntity = new IndeedRawJobEntry();
		rawEntity.setAge("2 days ago");
		rawEntity.setCountry("USA");
		rawEntity.setLocation("New York,NY");
		rawEntity.setDateParsed(LocalDate.now());
		rawEntity.setEmployer("mafia");
		rawEntity.setExternalId("100500");
		rawEntity.setTitle("big boss");
		rawEntity.setSalary("$79,972 a year");
		rawEntity.setJobUrl("url");

		var result = normalizer.normalize(rawEntity).get();

		assertThat(result.getBuzzwords()).isEmpty();
		assertThat(result.getCompany()).isEqualTo("mafia");
		assertThat(result.getCreatedOn()).isEqualTo(LocalDate.now().minusDays(2));
		assertThat(result.getExternalId()).isEqualTo("id|100500");
		assertThat(result.getLocation())
				.isEqualTo(Location.builder().setCountry("USA").setCity("New York").setArea("NY").build());
		assertThat(result.getParsingProblems()).isEmpty();
		assertThat(result.getPosition()).isEqualTo("big boss");
		assertThat(result.getSalary()).isEqualTo(Optional.of(Salary.builder()
				.setPeriod(Period.YEAR)
				.setMinSalary(79972)
				.setMaxSalary(79972)
				.setCurrency("USD")
				.build()));
		assertThat(result.getJobUrl()).isEqualTo("url");

	}

}
