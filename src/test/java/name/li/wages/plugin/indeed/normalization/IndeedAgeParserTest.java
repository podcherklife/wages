package name.li.wages.plugin.indeed.normalization;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;

public class IndeedAgeParserTest {

	@Test
	public void test() {
		var parser = new IndeedAgeParser();

		assertThat(parser.parseAgeInDays("1 day ago")).isEqualTo(Optional.of(1));
		assertThat(parser.parseAgeInDays("2 days ago")).isEqualTo(Optional.of(2));
		assertThat(parser.parseAgeInDays("100 days ago")).isEqualTo(Optional.of(100));
		assertThat(parser.parseAgeInDays("Today")).isEqualTo(Optional.of(0));
		assertThat(parser.parseAgeInDays("Just posted")).isEqualTo(Optional.of(0));
		assertThat(parser.parseAgeInDays("klvlwefewai")).isEqualTo(Optional.empty());
	}

}
