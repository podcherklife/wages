package name.li.wages.plugin.indeed.normalization;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import name.li.wages.plugin.api.Location;

public class IndeedLocationParserTest {

	@Test
	public void test() {
		var parser = new IndeedLocationParser("Russia");

		assertThat(parser.parse("foobar"))
				.isEqualTo(Optional.empty());
		assertThat(parser.parse("   Albany   ,    NY    "))
				.isEqualTo(Optional.of(Location.builder()
						.setCountry("Russia")
						.setCity("Albany")
						.setArea("NY")
						.build()));
		assertThat(parser.parse("Armonk, NY 10504"))
				.isEqualTo(Optional.of(Location.builder()
						.setCountry("Russia")
						.setCity("Armonk")
						.setArea("NY 10504")
						.build()));
		assertThat(parser.parse("New York, NY 10019 (Midtown area)"))
				.isEqualTo(Optional.of(Location.builder()
						.setCountry("Russia")
						.setCity("New York")
						.setArea("NY 10019 (Midtown area)")
						.build()));

	}

}
