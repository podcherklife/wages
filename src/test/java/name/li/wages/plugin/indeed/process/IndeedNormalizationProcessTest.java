package name.li.wages.plugin.indeed.process;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.persistence.JobEntryRepository;
import name.li.wages.plugin.api.JobEntry;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.indeed.normalization.IndeedNormalizer;
import name.li.wages.plugin.indeed.persistence.IndeedRawJobEntry;
import name.li.wages.plugin.indeed.persistence.IndeedRawJobEntryRepository;

@ExtendWith(MockitoExtension.class)
public class IndeedNormalizationProcessTest {

	@Mock
	private IndeedRawJobEntryRepository rawJobEntryRepository;
	@Mock
	private JobEntryRepository jobEntryRepository;
	@Mock
	private PlatformTransactionManager ptm;
	@Mock
	private IndeedNormalizer normalizer;

	private IndeedNormalizationProcess process;

	@BeforeEach
	public void init() {
		this.process = new IndeedNormalizationProcess(rawJobEntryRepository, jobEntryRepository, ptm, normalizer);
	}

	@Test
	public void testNormalProcess() {
		var rawEntry = new IndeedRawJobEntry();
		when(rawJobEntryRepository.getLastVersionsOfRawEntities()).thenReturn(Arrays.asList(rawEntry));
		var normalizedEntry = new JobEntry();
		when(normalizer.normalize(rawEntry)).thenReturn(Optional.of(normalizedEntry));
		var generation = Generation.create("foo");
		var parsingStep = process.createExecution(Optional.empty(), Optional.empty(), generation);

		parsingStep.start();

		verify(rawJobEntryRepository).getLastVersionsOfRawEntities();
		verify(normalizer).normalize(rawEntry);
		verify(jobEntryRepository).save(normalizedEntry);
		assertThat(normalizedEntry.getGeneration()).isEqualTo(generation);
	}

}
