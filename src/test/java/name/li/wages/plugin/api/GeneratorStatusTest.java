package name.li.wages.plugin.api;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.Test;

public class GeneratorStatusTest {

	@Test
	public void simpleStatusNotWrokingIfMinus1PercentCompleted() {
		var val = new AtomicInteger();
		val.set(0);

		var status = GeneratorStatus.simple(() -> val.get());
		assertThat(status.working()).isTrue();
		assertThat(status.percentsCompleted()).isEqualTo(0);

		val.set(-1);

		assertThat(status.working()).isFalse();
		assertThat(status.percentsCompleted()).isEqualTo(-1);
	}

	@Test
	public void simpleStatusFinishesWorkWhen100PercentCompleted() {
		var val = new AtomicInteger();
		val.set(0);

		var status = GeneratorStatus.simple(() -> val.get());
		assertThat(status.working()).isTrue();
		assertThat(status.percentsCompleted()).isEqualTo(0);

		val.set(100);

		assertThat(status.working()).isFalse();
		assertThat(status.percentsCompleted()).isEqualTo(100);
	}

	@Test
	public void compositeStatusAvegaresSubstatuses() {

		var val1 = new AtomicInteger();
		var val2 = new AtomicInteger();

		var status = GeneratorStatus.composite("compositeexample", Arrays.asList(
				GeneratorStatus.simple(() -> val1.get()),
				GeneratorStatus.simple(() -> val2.get())));

		assertThat(status.working()).isTrue();
		assertThat(status.percentsCompleted()).isEqualTo(0);

		val1.set(100);

		assertThat(status.working()).isTrue();
		assertThat(status.percentsCompleted()).isEqualTo(50);

		val2.set(100);

		assertThat(status.working()).isFalse();
		assertThat(status.percentsCompleted()).isEqualTo(100);

	}

}
