package name.li.wages.plugin.api;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class StatefulStatusWithKnownElementCountTest {

	private static String DESCRIPTION = "desc";
	private StatefulStatusWithKnownElementCount status = new StatefulStatusWithKnownElementCount("desc");

	@Test
	public void testInitialState() {
		expectCurrentStatus(false, 0);
	}

	@Test
	public void testWorkingState() {
		status.start(100);

		expectCurrentStatus(true, 0);

		status.incrementCompletedCount();

		expectCurrentStatus(true, 1);
	}

	@Test
	public void testFinishedState() {
		status.start(100);
		status.finish();

		expectCurrentStatus(false, 100);
	}

	@Test
	public void testErrorState() {
		status.start(100);
		status.incrementCompletedCount();
		status.finishWithError();

		expectCurrentStatus(false, 1);
	}

	public void expectCurrentStatus(boolean working, int percentCompleted) {
		assertThat(status.percentsCompleted()).isEqualTo(percentCompleted);
		assertThat(status.working()).isEqualTo(working);
		assertThat(status.description()).isEqualTo(DESCRIPTION);
	}

}
