package name.li.wages.plugin.japandevcom.normalization;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import name.li.wages.plugin.api.Location;

public class JapanDevLocationParserTest {

	@Test
	public void testParsing() {
		JapanDevLocationParser parser = new JapanDevLocationParser();

		assertThat(parser.parse("Tokyo (sublocation, Chiyoda-ku)"))
				.isEqualTo(Optional.of(Location.builder()
						.setCountry("japan")
						.setCity("Tokyo")
						.setArea("sublocation, Chiyoda-ku")
						.build()));
		assertThat(parser.parse("Tokyo (Tamachi)"))
				.isEqualTo(Optional.of(Location.builder()
						.setCountry("japan")
						.setCity("Tokyo")
						.setArea("Tamachi")
						.build()));
		assertThat(parser.parse("Tokyo"))
				.isEqualTo(Optional.of(
						Location.builder()
								.setCountry("japan")
								.setCity("Tokyo")
								.withoutArea()
								.build()));
		assertThat(parser.parse("Remote (Tokyo / San Francisco)"))
				.isEqualTo(Optional.of(
						Location.builder()
								.setCountry("remote")
								.setCity("remote")
								.withoutArea()
								.build()));
		assertThat(parser.parse("")).isEqualTo(Optional.empty());
	}

}
