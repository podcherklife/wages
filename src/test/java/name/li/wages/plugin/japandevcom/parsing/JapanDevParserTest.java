package name.li.wages.plugin.japandevcom.parsing;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import name.li.wages.plugin.api.JobEntry;
import name.li.wages.plugin.japandevcom.dataclasses.JapanDevCompany;
import name.li.wages.plugin.japandevcom.dataclasses.JapanDevJob;
import name.li.wages.plugin.japandevcom.dataclasses.JapanDevJobAttributes;
import name.li.wages.plugin.japandevcom.dataclasses.JapanDevResponse;
import name.li.wages.plugin.japandevcom.dataclasses.JapanDevSkill;
import name.li.wages.plugin.japandevcom.normalization.JapanDevNormalizer;

public class JapanDevParserTest {

	@Test
	public void savedResponseIsParsedWithoutIssues() {
		var parser = new JapanDevParser(new JapanDevResponseParser(), JapanDevSource
				.fromInputStream(this.getClass().getClassLoader().getResourceAsStream("japanDevResponse.json")));

		Collection<JobEntry> result = parser.generate()
				.map(new JapanDevNormalizer()::normalize)
				.flatMap(e -> e.stream())
				.collect(Collectors.toList());

		assertThat(result).allMatch(e -> e.getParsingProblems().isEmpty());
	}

	@Test
	public void testParsingSingleItem() throws JsonParseException, JsonMappingException, IOException {

		byte[] input = ("{'data':["
				+ "{'type':'job', 'id':'1', 'attributes': "
				+ "  {"
				+ "   'skills': [{ 'id': 'skill' , 'system_name': 'testSkill' }],"
				+ "   'job_post_date': 'Dec 18th 2019', 'technologies': 'java, Spring',"
				+ "   'salary_max':15, 'salary_min':11, 'location': 'tokyo', 'company': {'name': 'INDEED'}"
				+ "  }"
				+ "}"
				+ "]}").replaceAll("'", "\"").getBytes();

		JapanDevResponse data = new JapanDevResponseParser()
				.parse(new ByteArrayInputStream(input));

		assertThat(data).isEqualTo(JapanDevResponse.builder()
				.setData(Arrays.asList(
						JapanDevJob.builder()
								.setType("job")
								.setId(1)
								.setAttributes(JapanDevJobAttributes.builder()
										.setTechnologies("java, Spring")
										.setSalaryMax(15)
										.setSalaryMin(11)
										.setLocation("tokyo")
										.setCompany(JapanDevCompany.builder().setName("INDEED").build())
										.setJobPostDate("Dec 18th 2019")
										.setSkills(Arrays.asList(JapanDevSkill.builder()
												.setId("skill")
												.setSystemName("testSkill")
												.build()))
										.build())
								.build()))
				.build());
	}

	@Test
	public void testParsingSingleItem1() throws JsonParseException, JsonMappingException, IOException {
		JapanDevResponse data = new JapanDevResponseParser().parse(new ByteArrayInputStream(
				("{'data':["
						+ "{'type':'job', 'id':'1', 'attributes': "
						+ "  {"
						+ "   'skills': [{ 'id': 'skill' , 'system_name': 'testSkill' }],"
						+ "   'job_post_date': 'Dec 18th 2019', 'technologies': 'java, Spring',"
						+ "   'salary_max':15, 'salary_min':11, 'location': 'tokyo', 'company': {'name': 'INDEED'}"
						+ "  }"
						+ "}"
						+ "]}")
								.replaceAll("'", "\"")
								.getBytes()));

		JapanDevResponse expectedResponse = JapanDevResponse.builder()
				.setData(Arrays.asList(
						JapanDevJob.builder()
								.setType("job")
								.setId(1)
								.setAttributes(JapanDevJobAttributes.builder()
										.setTechnologies("java, Spring")
										.setSalaryMax(15)
										.setSalaryMin(11)
										.setLocation("tokyo")
										.setCompany(JapanDevCompany.builder().setName("INDEED").build())
										.setJobPostDate("Dec 18th 2019")
										.setSkills(Arrays.asList(JapanDevSkill.builder()
												.setId("skill")
												.setSystemName("testSkill")
												.build()))
										.build())
								.build()))
				.build();

		assertThat(data).isEqualTo(expectedResponse);
	}
}
