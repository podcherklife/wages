package name.li.wages.plugin.japandevcom.normalization;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

class JapanDevDateParserTest {

	@Test
	void test() {
		var parser = new JapanDevDateParser();

		assertThat(parser.parse("March 30th 2020")).isEqualTo(LocalDate.of(2020, 3, 30));
		assertThat(parser.parse("March 30th, 2020")).isEqualTo(LocalDate.of(2020, 3, 30));
		assertThat(parser.parse("Mar 5th 2020")).isEqualTo(LocalDate.of(2020, 3, 5));
		assertThat(parser.parse("October 1st 2019")).isEqualTo(LocalDate.of(2019, 10, 1));
		assertThat(parser.parse("Nov 3rd 2020")).isEqualTo(LocalDate.of(2020, 11, 3));
		assertThat(parser.parse("Nov  9, 2020")).isEqualTo(LocalDate.of(2020, 11, 9));
		assertThat(parser.parse("Aug 10 2020")).isEqualTo(LocalDate.of(2020, 8, 10));
		assertThat(parser.parse("Aug 10, 2020")).isEqualTo(LocalDate.of(2020, 8, 10));
		assertThat(parser.parse("2019-12-01")).isEqualTo(LocalDate.of(2019, 12, 1));
	}

}
