package name.li.wages.plugin.japandevcom.parsing;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collection;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import name.li.wages.plugin.api.JobEntry;
import name.li.wages.plugin.japandevcom.normalization.JapanDevNormalizer;

public class JapanDevSanityTest {

	@Test
	void testSanity() {
		var parser = new JapanDevParser(new JapanDevResponseParser(), JapanDevSource.fromRestEndpoint());

		Collection<JobEntry> result = parser.generate()
				.map(new JapanDevNormalizer()::normalize)
				.flatMap(e -> e.stream())
				.collect(Collectors.toList());

		assertThat(result).allMatch(e -> e.getParsingProblems().isEmpty());
		assertThat(result).allMatch(e -> !e.getPosition().isEmpty());
		assertThat(result).allMatch(e -> !e.getCompany().isEmpty());
		assertThat(result).allMatch(e -> !e.getExternalId().isEmpty());
		assertThat(result)
				.allMatch(e -> e.getLocation().country().equals("japan") || e.getLocation().country().equals("remote"));
	}

}
