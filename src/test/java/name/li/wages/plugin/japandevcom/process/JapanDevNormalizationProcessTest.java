package name.li.wages.plugin.japandevcom.process;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.transaction.PlatformTransactionManager;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;

import name.li.wages.persistence.JobEntryRepository;
import name.li.wages.plugin.api.Buzzword;
import name.li.wages.plugin.api.JobEntry;
import name.li.wages.plugin.api.Location;
import name.li.wages.plugin.api.Salary;
import name.li.wages.plugin.api.Salary.Period;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.japandevcom.dataclasses.JapanDevCompany;
import name.li.wages.plugin.japandevcom.dataclasses.JapanDevJob;
import name.li.wages.plugin.japandevcom.dataclasses.JapanDevJobAttributes;
import name.li.wages.plugin.japandevcom.normalization.JapanDevNormalizer;
import name.li.wages.plugin.japandevcom.persistence.JapanDevRawJobEntry;
import name.li.wages.plugin.japandevcom.persistence.JapanDevRawJobEntryRepository;

@ExtendWith(MockitoExtension.class)
public class JapanDevNormalizationProcessTest {

	@Mock
	private JapanDevRawJobEntryRepository rawJobEntryRepository;

	@Mock
	private JobEntryRepository jobEntryRepository;

	@Mock
	private PlatformTransactionManager tm;

	private JapanDevNormalizer normalizer = new JapanDevNormalizer();

	@Test
	public void testNormalization() throws JsonParseException, JsonMappingException, IOException {
		var responseItems = Arrays.asList(
				JapanDevJob.builder()
						.setId(99)
						.setType("job")
						.setAttributes(JapanDevJobAttributes.builder()
								.setTitle("developer")
								.setCompany(JapanDevCompany.builder().setName("yakuza").build())
								.setLocation("Tokyo (Otemachi)")
								.setTechnologies("spring, java")
								.setSalaryMax(10_000_000)
								.setSalaryMin(5_000_000)
								.setSlug("slug")
								.setJobPostDate("Dec 18th 2019")
								.build())
						.build(),
				JapanDevJob.builder()
						.setId(100)
						.setType("job")
						.setAttributes(JapanDevJobAttributes.builder()
								.setTitle("bigboss")
								.setCompany(JapanDevCompany.builder().setName("company").build())
								.setLocation("Tokyo")
								.setTechnologies("foo, bar")
								.setSlug("otherslug")
								.setJobPostDate("2019-10-16")
								.build())
						.build(),
				JapanDevJob.builder()
						.setId(101)
						.setType("job")
						.setAttributes(JapanDevJobAttributes.builder()
								.setTitle("example")
								.setCompany(JapanDevCompany.builder().setName("company").build())
								.setLocation("Tokyo (Otemachi)")
								.setTechnologies("foo, bar")
								.setSlug("otherslug")
								.setJobPostDate("2019-10-16")
								.build())
						.build());

		var generation = Generation.create("generation");
		JapanDevNormalizationProcess normalizationProcess = new JapanDevNormalizationProcess(
				rawJobEntryRepository,
				jobEntryRepository,
				normalizer,
				tm);

		when(rawJobEntryRepository.getLastVersionsOfRawEntities())
				.thenReturn(
						responseItems.stream()
								.map(e -> JapanDevRawJobEntry.fromResponseItem(e, new ObjectMapper()))
								.collect(Collectors.toList()));

		normalizationProcess.createExecution(Optional.empty(), Optional.empty(), generation).start();

		ArgumentCaptor<JobEntry> captorOfSavedJobEntries = ArgumentCaptor.forClass(JobEntry.class);
		verify(jobEntryRepository, times(2)).save(captorOfSavedJobEntries.capture());
		var allSavedJobEntries = captorOfSavedJobEntries.getAllValues();

		assertThat(allSavedJobEntries).hasSize(2);
		var item1 = allSavedJobEntries.get(0);
		assertThat(item1.getParsingProblems()).isEmpty();
		assertThat(item1.getLocation())
				.isEqualTo(Location.builder()
						.setCountry("japan")
						.setCity("Tokyo")
						.setArea("Otemachi")
						.build());
		assertThat(item1.getBuzzwords()).isEqualTo(Sets.newHashSet(Buzzword.of("spring"), Buzzword.of("java")));
		assertThat(item1.getExternalId()).isEqualTo("japan-dev.com/jobs/yakuza/slug");
		assertThat(item1.getJobUrl()).isEqualTo("japan-dev.com/jobs/yakuza/slug");
		assertThat(item1.getCompany()).isEqualTo("yakuza");
		assertThat(item1.getPosition()).isEqualTo("developer");
		assertThat(item1.getCreatedOn()).isEqualTo(LocalDate.of(2019, 12, 18));
		assertThat(item1.getSalary())
				.isEqualTo(Optional.of(Salary.builder()
						.setPeriod(Period.YEAR)
						.setMinSalary(5_000_000)
						.setMaxSalary(10_000_000)
						.setCurrency("JPY")
						.build()));
		assertThat(item1.getGeneration()).isEqualTo(generation);

		var item2 = allSavedJobEntries.get(1);
		assertThat(item2.getParsingProblems()).isEmpty();
		assertThat(item2.getLocation())
				.isEqualTo(Location.builder()
						.setCountry("japan")
						.setCity("Tokyo")
						.withoutArea()
						.build());
		assertThat(item2.getBuzzwords()).isEqualTo(Sets.newHashSet(Buzzword.of("foo"), Buzzword.of("bar")));
		assertThat(item2.getExternalId()).isEqualTo("japan-dev.com/jobs/company/otherslug");
		assertThat(item2.getJobUrl()).isEqualTo("japan-dev.com/jobs/company/otherslug");
		assertThat(item2.getCompany()).isEqualTo("company");
		assertThat(item2.getPosition()).isEqualTo("bigboss");
		assertThat(item2.getCreatedOn()).isEqualTo(LocalDate.of(2019, 10, 16));
		assertThat(item2.getSalary())
				.isEqualTo(Optional.empty());
		assertThat(item1.getGeneration()).isEqualTo(generation);
	}

}
