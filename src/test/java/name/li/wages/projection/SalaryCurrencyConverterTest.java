package name.li.wages.projection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import javax.money.Monetary;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import name.li.wages.plugin.api.Salary;
import name.li.wages.projection.currency.CurrencyConverter.ToCurrencyConverter;

@ExtendWith(MockitoExtension.class)
class SalaryCurrencyConverterTest {

	@Mock
	private ToCurrencyConverter toUsdConverter;

	private SalaryCurrencyConverter converter;

	@BeforeEach
	public void init() {
		this.converter = new SalaryCurrencyConverter(toUsdConverter);
	}

	@Test
	void testNonEmpty() {
		when(toUsdConverter.convert(Money.of(100, "RUB"))).thenReturn(Money.of(200, "USD"));
		when(toUsdConverter.currency()).thenReturn(Monetary.getCurrency("USD"));

		var inputSalary = Salary.builder()
				.yearly()
				.setCurrency("RUB")
				.setMinSalary(100)
				.setMaxSalary(100)
				.build();

		assertThat(converter.convert(inputSalary))
				.isEqualTo(Salary.builder()
						.yearly()
						.setCurrency("USD")
						.setMinSalary(200)
						.setMaxSalary(200)
						.build());
	}

}
