package name.li.wages.projection;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class CyrillicRomanizerTest {

	@Test
	public void test() {
		var romanizer = new CyrillicRomanizer();
		assertThat(romanizer.romanize("москва")).isEqualTo("moskva");
		assertThat(romanizer.romanize("hello")).isEqualTo("hello");
		assertThat(romanizer.romanize("привет мирщч!")).isEqualTo("privet mirschch!");
		assertThat(romanizer.romanize("фoo bar")).isEqualTo("foo bar");
	}

}
