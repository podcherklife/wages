package name.li.wages.projection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import name.li.wages.plugin.api.Salary;
import name.li.wages.plugin.api.Salary.Period;

class SalaryPeriodConverterTest {

	@BeforeEach
	public void init() {
	}

	@TestFactory
	public Stream<DynamicTest> allPeriodCombinations() {
		return Sets.cartesianProduct(ImmutableSet.copyOf(Period.values()), ImmutableSet.copyOf(Period.values()))
				.stream()
				.map(pair -> dynamicTest(String.format("Conversion pair %s", pair),
						() -> checkConversionIsSuccessful(pair.get(0), pair.get(1))));

	}

	void checkConversionIsSuccessful(Period from, Period to) {
		var converter = new SalaryPeriodConverter(Period.YEAR);

		assertThat(converter.convert(Salary.builder()
				.setCurrency("USD")
				.setMaxSalary(100)
				.setMinSalary(100)
				.setPeriod(from)
				.build())).isNotNull();
	}

	@Test
	void testConversion() {
		testConversion(Period.HOUR, 10, 20, Period.YEAR, 10 * 160 * 12, 20 * 160 * 12);
		testConversion(Period.MONTH, 10, 20, Period.YEAR, 120, 240);
	}

	private void testConversion(Period from, int max, int min, Period to, int expectedMax, int expectedMin) {
		var converter = new SalaryPeriodConverter(Period.YEAR);

		assertThat(converter.convert(Salary.builder()
				.setCurrency("USD")
				.setMaxSalary(max)
				.setMinSalary(min)
				.setPeriod(from)
				.build()))
						.isEqualTo(Salary.builder()
								.setCurrency("USD")
								.setMaxSalary(expectedMax)
								.setMinSalary(expectedMin)
								.setPeriod(to)
								.build());
	}

}
