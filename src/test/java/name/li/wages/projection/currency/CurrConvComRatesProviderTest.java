package name.li.wages.projection.currency;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.data.Percentage.withPercentage;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;

import javax.money.Monetary;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import name.li.wages.projection.currency.CurrencyConverter.ToCurrencyConverter;

@Tag("slow")
public class CurrConvComRatesProviderTest {

	private ToCurrencyConverter toUsd = new SimpleCurrencyConverter(
			new CurrConvComRatesProvider("f5f26ee099d52231f8dc"))
					.toCurrency(Monetary.getCurrency("USD"));

	@Test
	public void convert() throws JsonParseException, JsonMappingException, IOException {

		assertThat(toUsd.convert(Money.of(1000, Monetary.getCurrency("RUB"))).getNumber().doubleValue())
				.isCloseTo(15, withPercentage(30));

		assertThat(toUsd.convert(Money.of(1000, Monetary.getCurrency("JPY"))).getNumber().doubleValue())
				.isCloseTo(10, withPercentage(30));

	}

	@Test
	public void convertWithUnsupportedCurrency() {
		assertThrows(RetrievingRatesException.class, () -> toUsd.convert(Money.of(100, Monetary.getCurrency("XXX"))));
	}

}
