package name.li.wages.projection.currency;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import javax.money.CurrencyUnit;
import javax.money.Monetary;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import name.li.wages.persistence.RateEntryRepository;
import name.li.wages.projection.currency.RatesProvider.Rate;

@ExtendWith(MockitoExtension.class)
public class DbRatesProviderDecoratorTest {

	private static LocalDate NOW = LocalDate.now();

	private static CurrencyUnit USD = Monetary.getCurrency("USD");
	private static CurrencyUnit EUR = Monetary.getCurrency("EUR");
	private static CurrencyUnit RUB = Monetary.getCurrency("RUB");

	private static Rate USD_TO_RUB_RATE = Rate.builder()
			.setFrom(USD)
			.setTo(RUB)
			.setOnDate(NOW)
			.setRate(0.5)
			.build();

	private static Rate EUR_TO_RUB_RATE = Rate.builder()
			.setFrom(EUR)
			.setTo(RUB)
			.setOnDate(NOW)
			.setRate(0.1)
			.build();

	@Mock
	private RateEntryRepository ratesRepository;
	@Mock
	private RatesProvider ratesProvider;

	private DbRatesProviderDecorator decorator;

	@BeforeEach
	public void init() {
		this.decorator = new DbRatesProviderDecorator(ratesRepository, ratesProvider);
	}

	@Test
	public void testDbRate() {
		when(ratesRepository.getRate(USD, RUB, NOW))
				.thenReturn(Optional.of(RateEntity.fromRate(USD_TO_RUB_RATE)));

		assertThat(decorator.getRate(USD, RUB, NOW)).isEqualTo(USD_TO_RUB_RATE);

		verifyNoInteractions(ratesProvider);
	}

	@Test
	public void testProviderRate() {
		when(ratesRepository.getRate(Mockito.any(), Mockito.any(), Mockito.any()))
				.thenReturn(Optional.empty());
		when(ratesProvider.getRate(EUR, RUB, NOW))
				.thenReturn(EUR_TO_RUB_RATE);

		assertThat(decorator.getRate(EUR, RUB, NOW)).isEqualTo(EUR_TO_RUB_RATE);

		var captor = ArgumentCaptor.forClass(RateEntity.class);
		verify(ratesRepository).save(captor.capture());
		var savedEntity = captor.getValue();
		assertThat(savedEntity.toRate()).isEqualTo(EUR_TO_RUB_RATE);
	}

}
