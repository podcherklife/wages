package name.li.wages.projection;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;

import com.google.common.collect.Sets;

import name.li.wages.plugin.api.Buzzword;
import name.li.wages.plugin.api.JobEntry;
import name.li.wages.plugin.api.Location;
import name.li.wages.plugin.api.Salary;
import name.li.wages.plugin.api.Salary.Period;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.projection.currency.CurrencyConverter;

public class JobEntryProjectorTest {

	private final String COMPANY = "company";
	private final Location LOCATION = Location.builder()
			.setCountry("russia")
			.setCity("ryazan")
			.setArea("sokolovka")
			.build();
	private final LocalDate CREATED_ON = LocalDate.now();
	private final String EXTERNAL_ID = "externalId";
	private final Collection<String> PARSING_PROBLEMS = Arrays.asList("problem");
	private final String POSITION = "position";
	private final Salary SALARY = Salary.builder()
			.setPeriod(Period.MONTH)
			.setMinSalary(100)
			.setMaxSalary(200)
			.setCurrency("RUB")
			.build();
	private final Salary EXPECTED_SALARY = Salary.builder()
			.setPeriod(Period.YEAR)
			.setMinSalary(2400)
			.setMaxSalary(4800)
			.setCurrency("USD")
			.build();
	private final Set<Buzzword> BUZZWORDS = Sets.newHashSet(Buzzword.of("spring"));
	private final String JOB_URL = "job_url";

	@Test
	public void test() {
		var jobEntry = new JobEntry();
		jobEntry.setCompany(COMPANY);
		jobEntry.setLocation(LOCATION);
		jobEntry.setCreatedOn(CREATED_ON);
		jobEntry.setExternalId(EXTERNAL_ID);
		jobEntry.setParsingProblems(PARSING_PROBLEMS);
		jobEntry.setPosition(POSITION);
		jobEntry.setSalary(SALARY);
		jobEntry.setBuzzwords(BUZZWORDS);
		jobEntry.setJobUrl(JOB_URL);

		var generation = new Generation("");
		var projector = new JobEntryProjector(generation, salaryConverterFactory(), new SalaryPeriodConverterFactory());

		var projectedEntry = projector.project(jobEntry);

		assertThat(projectedEntry.getBuzzwords()).isEqualTo(BUZZWORDS);
		assertThat(projectedEntry.getCompany()).isEqualTo(COMPANY);
		assertThat(projectedEntry.getCreatedOn()).isEqualTo(CREATED_ON);
		assertThat(projectedEntry.getExternalId()).isEqualTo(EXTERNAL_ID);
		assertThat(projectedEntry.getGeneration()).isEqualTo(generation);
		assertThat(projectedEntry.getLocation()).isEqualTo(LOCATION);
		assertThat(projectedEntry.getPosition()).isEqualTo(POSITION);
		assertThat(projectedEntry.getSalary()).isEqualTo(Optional.of(SALARY));
		assertThat(projectedEntry.getUsdSalary()).isEqualTo(Optional.of(EXPECTED_SALARY));
		assertThat(projectedEntry.getJobUrl()).isEqualTo(JOB_URL);

		assertThat(jobEntry.getProjectionToken()).isEqualTo(projector.projectionToken());

	}

	private SalaryConverterFactory salaryConverterFactory() {
		return new SalaryConverterFactory(new CurrencyConverter() {

			@Override
			public Money convert(MonetaryAmount amount, CurrencyUnit toCurrency, LocalDate onDate) {
				return Money.of(amount.getNumber().intValue() * 2, EXPECTED_SALARY.currency());
			}

		});
	}

}
