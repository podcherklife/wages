package name.li.wages.selenium;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SeleniumProvider {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public static class DriverError extends RuntimeException {
		private static final long serialVersionUID = 1L;

		private DriverError(Throwable e) {
			super(e);
		}
	}

	private final String url;
	private volatile int numberOfGets;
	private volatile WebDriver driverInstance;

	public SeleniumProvider(String url) {
		this.url = url;
	}

	private WebDriver getNewDriver() {
		try {
			return new RemoteWebDriver(new URL(url),
					new FirefoxOptions().setHeadless(true)
							.addPreference("javascript.enabled", false)
							.addPreference("permissions.default.image", 2));
		} catch (MalformedURLException e) {
			throw new DriverError(e);
		}
	}

	public WebDriver getPooledDriver() {
		numberOfGets += 1;
		if (this.driverInstance == null) {
			logger.info("creating shared selenium driver instance");
			this.driverInstance = getNewDriver();
		} else if (numberOfGets % 5 == 0) {
			logger.info("reloading shared selenium driver instance");
			this.driverInstance.quit();
			this.driverInstance = getNewDriver();
		}
		logger.info("returning shared selenium driver instance");
		return (WebDriver) Proxy.newProxyInstance(WebDriver.class.getClassLoader(), new Class<?>[] { WebDriver.class },
				(self, method, args) -> {
					try {
						var methodResult = method.invoke(driverInstance, args);
						if (method.getName().equals("quit")) {
							logger.info("quiting shared selenium driver instance");
							this.driverInstance = null;
						}
						return methodResult;
					} catch (InvocationTargetException e) {
						throw e.getCause();
					}
				});
	}
}
