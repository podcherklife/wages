package name.li.wages;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

import name.li.wages.persistence.GenerationRepository;
import name.li.wages.plugin.api.GenerationAttempt;
import name.li.wages.plugin.api.GeneratorStatus;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.Generation.Status;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.api.process.Step.ProcessError;

@Named
public class ProcessManager {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private Collection<ProcessDescriptor> allKnownProcessDescriptors;

	private volatile ConcurrentHashMap<ProcessDescriptor, Step> executions = new ConcurrentHashMap<>();

	private GenerationRepository generationRepository;

	private TransactionTemplate tt;

	@Inject
	public ProcessManager(
			Collection<ProcessDescriptor> allKnownProcessDescriptors,
			GenerationRepository generationRepository,
			PlatformTransactionManager tm) {
		this.allKnownProcessDescriptors = allKnownProcessDescriptors;
		this.generationRepository = generationRepository;
		this.tt = new TransactionTemplate(tm);
		this.tt.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
	}

	private Optional<ProcessDescriptor> getProcess(String id) {
		return allKnownProcessDescriptors.stream().filter(e -> e.id().equals(id)).findAny();
	}

	private Optional<Step> getExecution(ProcessDescriptor descriptor) {
		return Optional.ofNullable(executions.get(descriptor));
	}

	public Stream<ProcessDescriptor> allProcesses() {
		return allKnownProcessDescriptors.stream();
	}

	public Optional<GeneratorStatus> info(String processId) {
		return getProcess(processId)
				.flatMap(this::getExecution)
				.map(e -> e.status());
	}

	@Transactional
	public synchronized void tryContinueExecution(String processId) {
		createExecution(processId, generationRepository.getLastCompletedGeneration(processId));
	}

	@Transactional
	public synchronized void cancelUnfinishedExecution(String processId) {
		getProcess(processId)
				.ifPresent(process -> {
					executions.remove(process);
					generationRepository.getMostRecentGeneration(processId)
							.ifPresentOrElse(generation -> {
								if (generation.isCancelled()) {
									throw new IllegalStateException("Execution is already cancelled");
								} else {
									generation.clearContinuationToken();
									generation.setStatus(Status.CANCELLED);
								}
							}, () -> {
								throw new IllegalArgumentException("No execution to cancel");
							});
				});
	}

	@Transactional
	public synchronized void createNewExecution(String processId) {
		createExecution(processId, Optional.empty());
	}

	private synchronized void createExecution(
			String processId,
			Optional<Generation> lastGeneration) {
		getProcess(processId)
				.ifPresentOrElse(process -> {
					if (executions.containsKey(process)) {
						throw new IllegalStateException("Execution already exists");
					}
					var currentGeneration = generationRepository.getMostRecentGeneration(processId)
							.filter(generation -> !generation.isCompleted())
							.orElse(Generation.create(String.format("Generation for process '%s'", processId)));
					currentGeneration.setStatus(Status.PENDING);
					currentGeneration.setProcessId(processId);
					logger.info("About to create execution. Token: {}, lastGen: {}",
							currentGeneration.getContinuationToken(), lastGeneration);
					generationRepository.save(currentGeneration);
					var execution = process.createExecution(currentGeneration.getContinuationToken(), lastGeneration,
							currentGeneration);
					executions.put(process, execution);
				}, () -> {
					throw new IllegalArgumentException("Unknown process id");
				});
	}

	@Transactional
	public synchronized void startExecution(String processId) {
		getProcess(processId)
				.flatMap(this::getExecution)
				.ifPresentOrElse(execution -> {
					var ongoingGeneration = generationRepository.getMostRecentGeneration(processId).get();
					try {
						logger.info("Starting process {}", processId);
						tt.executeWithoutResult((transactionStatus) -> ongoingGeneration.setStatus(Status.IN_PROGRESS));

						// execute process in a separate transaction
						// so that current transaction will not rolled back in case of error
						tt.executeWithoutResult((transactionStatus) -> {
							execution.start();
						});
						ongoingGeneration.clearContinuationToken();
						ongoingGeneration.setStatus(Status.COMPLETED);
						ongoingGeneration.addAttempt(GenerationAttempt.create("success"));
						logger.info("Successfully completed process {}", processId);
					} catch (ProcessError e) {
						ongoingGeneration.setStatus(Status.FAILED);
						ongoingGeneration.addAttempt(GenerationAttempt.create(e.getMessage()));
						ongoingGeneration.setContinuationToken(e.getToken());
						logger.error("Process failed: {}", e.getMessage(), e);
					} catch (Exception e) {
						ongoingGeneration.setStatus(Status.FAILED);
						ongoingGeneration.addAttempt(GenerationAttempt.create(e.getMessage()));
						logger.error("Unknown error", e);
					} finally {
						executions.remove(getProcess(processId).get());
					}
				}, () -> {
					throw new IllegalArgumentException("Unknown process id");
				});
	}

}
