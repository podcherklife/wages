package name.li.wages;

import java.util.stream.Collectors;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import name.li.wages.plugin.api.GeneratorStatus;
import name.li.wages.plugin.api.process.ProcessDescriptor;

@RestController
@RequestMapping("/process/")
public class ProcessRunnerController {
	private ProcessManager processmanager;

	@Inject
	public ProcessRunnerController(ProcessManager processmanager) {
		this.processmanager = processmanager;
	}

	@GetMapping("list")
	public String list() {
		return processmanager.allProcesses().map(ProcessDescriptor::id).collect(Collectors.joining("<br>"));
	}

	@GetMapping("/{id}/info")
	public String info(@PathVariable String id) {
		return processmanager
				.info(id)
				.map(status -> stringifyStatus(0, status))
				.orElse("No such process or no such execution");
	}

	@GetMapping("/{id}/createExecution")
	public String createExecution(@PathVariable String id) {
		processmanager.tryContinueExecution(id);
		return "ok";
	}

	@GetMapping("/{id}/start")
	public synchronized String start(@PathVariable String id) {
		processmanager.startExecution(id);
		return "ok";
	}

	@GetMapping("/{id}/cancel")
	public String cancelExecution(@PathVariable String id) {
		processmanager.cancelUnfinishedExecution(id);
		return "ok";
	}

	private String stringifyStatus(int level, GeneratorStatus status) {
		return String.format("%s, %s, %d",
				status.description(), status.working(), status.percentsCompleted())
				+ "<br>"
				+ status.substatuses().stream().map((sub) -> "|".repeat(level + 1) + stringifyStatus(level + 1, sub))
						.collect(Collectors.joining(""));
	}

}
