package name.li.wages.plugin.api.process;

import java.util.Collection;
import java.util.Collections;

import name.li.wages.plugin.api.GeneratorStatus;
import name.li.wages.plugin.api.process.ProcessDescriptor.ContinuationToken;

public interface Step {

	class ProcessError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		private ContinuationToken token;

		public ProcessError(ContinuationToken token, Throwable reason) {
			super(reason);
			this.token = token;
		}

		public ContinuationToken getToken() {
			return token;
		}

	}

	String id();

	GeneratorStatus status();

	default Collection<Step> steps() {
		return Collections.emptyList();
	}

	void start() throws ProcessError;

	static Step composite(
			String id,
			String description,
			Collection<Step> substeps) {
		return new CompositeStep(id, description, substeps);
	}

	static Step skipped(String id) {
		return new SkippedStep(id);
	}

}
