package name.li.wages.plugin.api.process;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.google.common.base.MoreObjects;

import name.li.wages.plugin.api.GenerationAttempt;
import name.li.wages.plugin.api.process.ProcessDescriptor.ContinuationToken;

@Entity
@Table(name = "generation")
public class Generation {

	public enum Status {
		PENDING,
		IN_PROGRESS,
		COMPLETED,
		FAILED,
		CANCELLED
	}

	@Id
	@Column(name = "id")
	private String id = UUID.randomUUID().toString();

	@OneToMany(cascade = CascadeType.ALL)
	private Set<GenerationAttempt> attempts = new HashSet<>();

	@Column(name = "description")
	private String description = "";

	@Column(name = "processId")
	private String processId = "";

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private Status status;

	@Column(name = "instant")
	private Instant instant = Instant.now();

	@Column(name = "date")
	private LocalDate date;

	@Embedded
	private ContinuationTokenEntity continuationToken;

	public Generation(String description) {
		this();
		this.description = description;
	}

	private Generation() {
		this.date = Objects.requireNonNullElse(this.date, calcDate());
	}

	public LocalDate calcDate() {
		return LocalDate.ofInstant(instant, ZoneOffset.UTC);
	}

	public LocalDate getDate() {
		return Objects.requireNonNullElse(this.date, calcDate());
	}

	public Collection<GenerationAttempt> getAttempts() {
		return attempts;
	}

	public String getDescription() {
		return description;
	}

	public void setAttempts(Set<GenerationAttempt> attempts) {
		this.attempts = attempts;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public static Generation create(String description) {
		return new Generation(description);
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getProcessId() {
		return processId;
	}

	public boolean isFailed() {
		return this.status == Status.FAILED;
	}

	public boolean isCompleted() {
		return this.status == Status.COMPLETED;
	}

	public boolean isCancelled() {
		return this.status == Status.CANCELLED;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Status getStatus() {
		return status;
	}

	public void addAttempt(GenerationAttempt attempt) {
		this.attempts.add(attempt);
	}

	public Optional<ContinuationToken> getContinuationToken() {
		return Optional.ofNullable(continuationToken).map(ContinuationTokenEntity::token);
	}

	public void setContinuationToken(ContinuationToken token) {
		this.continuationToken = ContinuationTokenEntity.fromToken(token);
	}

	public void clearContinuationToken() {
		this.continuationToken = null;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(Generation.class)
				.add("description", description)
				.add("attempts", attempts)
				.add("processId", processId)
				.add("status", status)
				.toString();
	}
}
