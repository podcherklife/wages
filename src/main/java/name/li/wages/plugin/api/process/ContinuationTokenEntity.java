package name.li.wages.plugin.api.process;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.google.common.base.MoreObjects;

import name.li.wages.plugin.api.process.ProcessDescriptor.ContinuationToken;

@Embeddable
public class ContinuationTokenEntity {

	@Column(name = "continuation_token_process_id")
	private String processId;

	@Column(name = "continuation_token_value")
	private String value;

	public String getProcessId() {
		return processId;
	}

	public String getValue() {
		return value;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getStepId() {
		return processId;
	}

	public void setStepId(String stepId) {
		this.processId = stepId;
	}

	public ContinuationToken token() {
		return ContinuationToken.builder()
				.setStepId(processId)
				.setValue(value)
				.build();
	}

	public static ContinuationTokenEntity fromToken(ContinuationToken token) {
		var entity = new ContinuationTokenEntity();
		entity.setProcessId(token.stepId());
		entity.setValue(token.value());
		return entity;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this.getClass())
				.add("processId", processId)
				.add("value", value)
				.toString();
	}

}
