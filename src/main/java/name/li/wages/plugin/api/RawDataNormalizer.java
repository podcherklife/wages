package name.li.wages.plugin.api;

public interface RawDataNormalizer<T> {

	JobEntry normalize(T item);

	Class<? extends T> canNormalize();

	String externalIdProperty();

	String id();

}