package name.li.wages.plugin.api;

import java.util.concurrent.atomic.AtomicInteger;

import com.google.common.base.MoreObjects;

public class StatefulStatusWithKnownElementCount implements GeneratorStatus {

	private volatile GeneratorStatus actualStatus;
	private final String description;

	public StatefulStatusWithKnownElementCount(String description) {
		this.description = description;
		this.actualStatus = new NotStartedStatus(description);
	}

	@Override
	public int percentsCompleted() {
		return actualStatus.percentsCompleted();
	}

	@Override
	public boolean working() {
		return actualStatus.working();
	}

	@Override
	public String description() {
		return actualStatus.description();
	}

	public void start(Integer expectedTotal) {
		this.actualStatus = new WorkingStatus(description, expectedTotal);
	}

	public void finish() {
		this.actualStatus = new FinishedStatus(description);
	}

	public void finishWithError() {
		this.actualStatus = new FinishedWithErrorStatus(description, this.actualStatus.percentsCompleted());
	}

	public void incrementCompletedCount() {
		if (actualStatus instanceof WorkingStatus) {
			((WorkingStatus) actualStatus).incrementCompletedCount();
		} else {
			throw new IllegalStateException(
					String.format(
							"Can increment completed count only for %s, but actual state was %s",
							WorkingStatus.class,
							actualStatus.getClass()));
		}
	}

	private static class NotStartedStatus implements GeneratorStatus {
		private final String description;

		private NotStartedStatus(String description) {
			this.description = description;
		}

		@Override
		public int percentsCompleted() {
			return 0;
		}

		@Override
		public boolean working() {
			return false;
		}

		@Override
		public String description() {
			return description;
		}

		@Override
		public String toString() {
			return MoreObjects.toStringHelper(getClass())
					.add("description", description)
					.toString();
		}

	}

	private static class WorkingStatus implements GeneratorStatus {
		private final String description;
		private AtomicInteger expectedTotal = new AtomicInteger(0);
		private AtomicInteger soFar = new AtomicInteger(0);

		private WorkingStatus(String description, Integer expectedTotal) {
			this.description = description;
			this.expectedTotal.set(expectedTotal);
		}

		@Override
		public String description() {
			return description;
		}

		@Override
		public int percentsCompleted() {
			var soFarInt = soFar.get();
			var extectedTotalInt = expectedTotal.get();
			if (extectedTotalInt == 0 && soFarInt != 0) {
				throw new IllegalStateException(
						String.format("Incorrect status state: %s out of %s", soFarInt, expectedTotal));
			} else if (extectedTotalInt == 0 && soFarInt == 0) {
				return 100;
			} else {
				return (soFarInt * 100) / extectedTotalInt;
			}
		}

		public void incrementCompletedCount() {
			soFar.incrementAndGet();
		}

		@Override
		public boolean working() {
			return true;
		}

		@Override
		public String toString() {
			return MoreObjects.toStringHelper(getClass())
					.add("description", description)
					.add("expectedTotal", expectedTotal)
					.add("soFar", soFar)
					.toString();
		}
	}

	private static class FinishedStatus implements GeneratorStatus {
		private final String description;

		private FinishedStatus(String description) {
			this.description = description;
		}

		@Override
		public int percentsCompleted() {
			return 100;
		}

		@Override
		public boolean working() {
			return false;
		}

		@Override
		public String description() {
			return description;
		}

		@Override
		public String toString() {
			return MoreObjects.toStringHelper(getClass())
					.add("description", description)
					.toString();
		}
	}

	private static class FinishedWithErrorStatus implements GeneratorStatus {
		private final String description;
		private final Integer percentsCompleted;

		private FinishedWithErrorStatus(String description, Integer percentsCompleted) {
			this.description = description;
			this.percentsCompleted = percentsCompleted;
		}

		@Override
		public int percentsCompleted() {
			return percentsCompleted;
		}

		@Override
		public boolean working() {
			return false;
		}

		@Override
		public String description() {
			return description;
		}

		@Override
		public String toString() {
			return MoreObjects.toStringHelper(getClass())
					.add("description", description)
					.add("percentsCompleted", percentsCompleted)
					.toString();
		}
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(getClass())
				.add("actualStatus", actualStatus)
				.toString();
	}
}
