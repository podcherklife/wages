package name.li.wages.plugin.api;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.sparkproject.guava.collect.Lists;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Sets;

import name.li.wages.plugin.api.process.Generation;

@Entity
@Table(name = "job_entry")
public class JobEntry {
	@Id
	@Column(name = "id")
	private String externalId;

	@Column(name = "job_url")
	private String jobUrl;

	@Column(name = "company")
	private String company;

	@Column(name = "location")
	private LocationEmbeddable location;

	@Column(name = "position")
	private String position;

	@AttributeOverrides({
			@AttributeOverride(name = "from", column = @Column(name = "salary_original_min")),
			@AttributeOverride(name = "to", column = @Column(name = "salary_original_max")),
			@AttributeOverride(name = "currency", column = @Column(name = "salary_original_currency")),
			@AttributeOverride(name = "period", column = @Column(name = "salary_original_period")),
	})
	@Embedded
	private SalaryEmbeddable salary;

	@Column(name = "created_on")
	private LocalDate createdOn;

	@ElementCollection
	private Collection<String> parsingProblems;

	@ManyToOne
	private Generation generation;

	@Convert(converter = BuzzWordFlattener.class)
	@Column(name = "flat_buzzwords", length = 10_000)
	private Set<Buzzword> flatBuzzwords = new HashSet<>();

	@Column(name = "projectionToken")
	private String projectionToken = "";

	public Generation getGeneration() {
		return generation;
	}

	public void setGeneration(Generation generation) {
		this.generation = generation;
	}

	public Set<Buzzword> getBuzzwords() {
		return Objects.requireNonNullElse(flatBuzzwords, Collections.emptySet());
	}

	public void setBuzzwords(Set<Buzzword> buzzwords) {
		this.flatBuzzwords = buzzwords;
	}

	public LocalDate getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDate createdOn) {
		this.createdOn = createdOn;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Location getLocation() {
		return Optional.ofNullable(location)
				.orElse(new LocationEmbeddable())
				.location();
	}

	public void setLocation(Location location) {
		this.location = LocationEmbeddable.from(location);
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public Collection<String> getParsingProblems() {
		return Objects.requireNonNullElse(parsingProblems, Collections.emptyList());
	}

	public void setParsingProblems(Collection<String> parsingProblems) {
		this.parsingProblems = parsingProblems;
	}

	public Optional<Salary> getSalary() {
		return Optional.ofNullable(salary).flatMap(SalaryEmbeddable::salary);
	}

	public void setSalary(Optional<Salary> mayBeSalary) {
		this.salary = mayBeSalary.map(SalaryEmbeddable::from).orElse(null);
	}

	public void setSalary(Salary salary) {
		this.salary = SalaryEmbeddable.from(salary);
	}

	public void addParsingProblem(String problem) {
		if (this.parsingProblems == null) {
			this.parsingProblems = new ArrayList<>();
		}
		this.parsingProblems.add(problem);
	}

	public String getProjectionToken() {
		return projectionToken;
	}

	public void setProjectionToken(String projectionToken) {
		this.projectionToken = projectionToken;
	}

	public String getJobUrl() {
		return jobUrl;
	}

	public void setJobUrl(String jobUrl) {
		this.jobUrl = jobUrl;
	}

	public Job job() {
		return Job.builder()
				.setBuzzwords(Sets.newHashSet(getBuzzwords()))
				.setCompany(getCompany())
				.setCreationDate(Optional.ofNullable(getCreatedOn()))
				.setExternalId(getExternalId())
				.setLocation(getLocation())
				.setParsingProblems(Lists.newArrayList(getParsingProblems()))
				.setPosition(getPosition())
				.setSalary(getSalary())
				.setJobUrl(getJobUrl())
				.build();

	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(getClass())
				.add("externalId", externalId)
				.add("company", company)
				.add("location", location)
				.add("position", position)
				.add("salary", salary)
				.add("createdOn", createdOn)
				.add("parsingProblems", parsingProblems)
				.add("generation", generation)
				.add("buzzwords", flatBuzzwords)
				.add("projectionToken", projectionToken)
				.add("jobUrl", jobUrl)
				.toString();
	}

}
