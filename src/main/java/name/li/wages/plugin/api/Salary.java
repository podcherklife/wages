package name.li.wages.plugin.api;

import java.util.OptionalInt;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Salary {

	public enum Period {
		HOUR, MONTH, YEAR
	}

	public abstract OptionalInt minSalary();

	public abstract OptionalInt maxSalary();

	public abstract String currency();

	public abstract Period period();

	public abstract Builder toBuilder();

	public static Salary.Builder builder() {
		return new AutoValue_Salary.Builder();
	}

	@AutoValue.Builder
	public static abstract class Builder {

		public abstract Salary.Builder setMinSalary(int minSalary);

		public abstract Salary.Builder setMinSalary(OptionalInt minSalary);

		public abstract Salary.Builder setMaxSalary(int maxSalary);

		public abstract Salary.Builder setMaxSalary(OptionalInt maxSalary);

		public abstract Salary.Builder setCurrency(String currency);

		public abstract Salary.Builder setPeriod(Period period);

		public Salary.Builder yearly() {
			return setPeriod(Period.YEAR);
		}

		public Salary.Builder monthly() {
			return setPeriod(Period.MONTH);
		}

		public Salary.Builder hourly() {
			return setPeriod(Period.HOUR);
		}

		public abstract Salary build();
	}
}