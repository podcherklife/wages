package name.li.wages.plugin.api;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import com.esotericsoftware.kryo.DefaultSerializer;
import com.esotericsoftware.kryo.serializers.FieldSerializer;
import com.google.auto.value.AutoValue;

@AutoValue
@DefaultSerializer(FieldSerializer.class)
public abstract class Job implements Serializable {

	private static final long serialVersionUID = 1L;

	public abstract String jobUrl();

	public abstract String externalId();

	public abstract String company();

	public abstract Location location();

	public abstract String position();

	public abstract Optional<Salary> salary();

	public abstract Optional<LocalDate> creationDate();

	public abstract Collection<String> parsingProblems();

	public abstract Set<Buzzword> buzzwords();

	public static Job.Builder builder() {
		return new AutoValue_Job.Builder();
	}

	@AutoValue.Builder
	public static abstract class Builder {

		public abstract Job.Builder setExternalId(String externalId);

		public abstract Job.Builder setJobUrl(String jobUrl);

		public abstract Job.Builder setCompany(String company);

		public abstract Job.Builder setLocation(Location location);

		public abstract Job.Builder setCreationDate(Optional<LocalDate> creationDate);

		public abstract Job.Builder setPosition(String position);

		public abstract Job.Builder setSalary(Optional<Salary> salary);

		public abstract Job.Builder setParsingProblems(Collection<String> parsingProblems);

		public abstract Job.Builder setBuzzwords(Set<Buzzword> buzzwords);

		public abstract Job build();
	}
}
