package name.li.wages.plugin.api;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Location {

	public abstract String city();

	public abstract String area();

	public abstract String country();

	public static Location.Builder builder() {
		return new AutoValue_Location.Builder();
	}

	@AutoValue.Builder
	public static abstract class Builder {

		public abstract Location.Builder setCity(String city);

		public abstract Location.Builder setCountry(String country);

		public abstract Location.Builder setArea(String area);

		public Location.Builder withoutArea() {
			return this.setArea("");
		}

		public abstract Location build();
	}
}
