package name.li.wages.plugin.api;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class BuzzwordEntity {

	@Id
	@Column(name = "value")
	private String value = "";

	public static BuzzwordEntity from(Buzzword buzzword) {
		var result = new BuzzwordEntity();
		result.value = buzzword.getValue();
		return result;
	}
}
