package name.li.wages.plugin.api;

import java.util.OptionalInt;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class OptionalIntConverter implements AttributeConverter<OptionalInt, Integer> {

	@Override
	public Integer convertToDatabaseColumn(OptionalInt attribute) {
		return (attribute == null || attribute.isEmpty()) ? null : attribute.getAsInt();
	}

	@Override
	public OptionalInt convertToEntityAttribute(Integer dbData) {
		return dbData == null ? OptionalInt.empty() : OptionalInt.of(dbData);
	}
}
