package name.li.wages.plugin.api;

import javax.persistence.Embeddable;

@Embeddable
public class LocationEmbeddable {

	private String city = "";
	private String country = "";
	private String area = "";

	public String getArea() {
		return area;
	}

	public String getCity() {
		return city;
	}

	public String getCountry() {
		return country;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Location location() {
		return Location.builder()
				.setArea(area)
				.setCity(city)
				.setCountry(country)
				.build();
	}

	public static LocationEmbeddable from(Location location) {
		return LocationEmbeddable.create(location.country(), location.city(), location.area());
	}

	private static LocationEmbeddable create(String country, String city, String area) {
		var res = new LocationEmbeddable();
		res.setCity(city);
		res.setArea(area);
		res.setCountry(country);
		return res;
	}

	@Override
	public String toString() {
		return location().toString();
	}

}
