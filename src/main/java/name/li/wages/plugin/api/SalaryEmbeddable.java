package name.li.wages.plugin.api;

import java.util.Optional;
import java.util.OptionalInt;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;

import name.li.wages.plugin.api.Salary.Period;

@Embeddable
public class SalaryEmbeddable {

	@Convert(converter = OptionalIntConverter.class)
	@Column(name = "salary_min")
	private OptionalInt from = OptionalInt.empty();

	@Convert(converter = OptionalIntConverter.class)
	@Column(name = "salary_max")
	private OptionalInt to = OptionalInt.empty();

	@Column(name = "salary_period")
	private String period;

	@Column(name = "salary_currency")
	private String currency;

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setFrom(OptionalInt from) {
		this.from = from;
	}

	public void setTo(OptionalInt to) {
		this.to = to;
	}

	public String getCurrency() {
		return currency;
	}

	public OptionalInt getFrom() {
		return from;
	}

	public OptionalInt getTo() {
		return to;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getPeriod() {
		return period;
	}

	public Optional<Salary> salary() {
		return (from == null || to == null || currency == null || currency.isEmpty())
				? Optional.empty()
				: Optional.of(Salary.builder()
						.setCurrency(currency)
						.setMaxSalary(to)
						.setMinSalary(from)
						.setPeriod(Period.valueOf(period))
						.build());
	}

	@Override
	public String toString() {
		return salary().toString();
	}

	public static SalaryEmbeddable from(Salary salary) {
		var result = new SalaryEmbeddable();
		result.setFrom(salary.minSalary());
		result.setTo(salary.maxSalary());
		result.setCurrency(salary.currency());
		result.setPeriod(salary.period().name());
		return result;
	}

}
