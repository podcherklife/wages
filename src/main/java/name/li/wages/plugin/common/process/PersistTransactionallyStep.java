package name.li.wages.plugin.common.process;

import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

import name.li.wages.plugin.api.GeneratorStatus;
import name.li.wages.plugin.api.process.ProcessDescriptor.ContinuationToken;
import name.li.wages.plugin.api.process.Step;

public class PersistTransactionallyStep<T> implements Step {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private Consumer<T> saveFunction;
	private SupplierWithStatus<T> provider;
	private String stepId;
	private TransactionTemplate tt;

	public PersistTransactionallyStep(
			String stepId,
			SupplierWithStatus<T> provider,
			Consumer<T> saveFunction,
			PlatformTransactionManager tm) {
		this.stepId = stepId;
		this.provider = provider;
		this.saveFunction = saveFunction;
		this.tt = new TransactionTemplate(tm);
		this.tt.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
	}

	@Override
	public String id() {
		return stepId;
	}

	@Override
	public GeneratorStatus status() {
		return provider.status();
	}

	@Override
	public void start() throws ProcessError {
		logger.info("Starting step '{}'", id());
		try {
			tt.executeWithoutResult((transaction) -> {
				var value = provider.get();
				saveFunction.accept(value);
			});
		} catch (Exception e) {
			logger.info("Error during step '{}' : {}", id(), e.getMessage());
			throw new ProcessError(
					ContinuationToken.builder()
							.setStepId(id())
							.setValue(id())
							.build(),
					e);
		} finally {
			logger.info("Finished step '{}'", id());
		}
	}

}
