package name.li.wages.plugin.common.parsing;

import java.util.stream.Stream;

import name.li.wages.plugin.api.GeneratorStatus;

public interface SearchResultsParser<T> {

	public static class ParserException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public ParserException(Throwable e) {
			super(e);
		}

	}

	GeneratorStatus status();

	Stream<T> generate() throws ParserException;
}
