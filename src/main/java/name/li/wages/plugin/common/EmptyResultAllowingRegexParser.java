package name.li.wages.plugin.common;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

public class EmptyResultAllowingRegexParser<T> {

	private final CompositeRegexParser<T> resultReturningParser;
	private final CompositeRegexParser<Boolean> resultlessParser;

	public EmptyResultAllowingRegexParser(
			CompositeRegexParser<T> resultReturningParser,
			Collection<String> validResultlessPatterns) {
		this.resultReturningParser = resultReturningParser;
		this.resultlessParser = new CompositeRegexParser<Boolean>(validResultlessPatterns
				.stream()
				.map(pattern -> new CompositeRegexParser.SingleRegexParser<Boolean>(pattern,
						matcher -> Boolean.TRUE))
				.collect(Collectors.toList()));

	}

	public ParsingResult<T> tryParse(String str) {
		return parseWithResultlessParser(str)
				.or(() -> parseWithResultYieldingParser(str))
				.orElse(ParsingResult.failed());
	}

	public Optional<ParsingResult<T>> parseWithResultYieldingParser(String str) {
		return resultReturningParser.parse(str).map(ParsingResult::parsed);
	}

	public Optional<ParsingResult<T>> parseWithResultlessParser(String str) {
		return resultlessParser.parse(str).map(val -> ParsingResult.matched());
	}

}
