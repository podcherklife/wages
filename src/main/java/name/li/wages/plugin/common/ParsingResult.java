package name.li.wages.plugin.common;

import java.util.Optional;
import java.util.function.Consumer;

import com.google.auto.value.AutoValue;

public interface ParsingResult<T> {
	void ifMatchedOrElse(Consumer<T> consumer, Runnable runnable);

	public static <T> ParsingResult<T> matched() {
		return new AutoValue_Matching<T>(Optional.empty());
	}

	public static <T> ParsingResult<T> parsed(T value) {
		return new AutoValue_Matching<T>(Optional.of(value));
	}

	@SuppressWarnings("unchecked")
	public static <T> ParsingResult<T> failed() {
		return (ParsingResult<T>) NonMatching.INSTANCE;
	}
}

class NonMatching<T> implements ParsingResult<T> {

	@Override
	public void ifMatchedOrElse(Consumer<T> consumer, Runnable runnable) {
		runnable.run();
	}

	static final NonMatching<?> INSTANCE = new NonMatching<>();

}

@AutoValue
abstract class Matching<T> implements ParsingResult<T> {

	@Override
	public void ifMatchedOrElse(Consumer<T> consumer, Runnable runnable) {
		getValue().ifPresent(consumer);
	}

	public abstract Optional<T> getValue();

}
