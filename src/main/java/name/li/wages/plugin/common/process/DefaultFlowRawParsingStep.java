package name.li.wages.plugin.common.process;

import java.util.stream.Stream;

import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.common.parsing.SearchResultsParser;
import name.li.wages.plugin.common.persistence.DefaultFlowRawEntity;
import name.li.wages.plugin.common.persistence.DefaultParsingFlowRawRepository;

public class DefaultFlowRawParsingStep<T extends DefaultFlowRawEntity> extends PersistTransactionallyStep<Stream<T>> {

	public DefaultFlowRawParsingStep(
			Generation ongoingGeneration,
			String stepId,
			SearchResultsParser<T> parser,
			DefaultParsingFlowRawRepository<T, ?> repository,
			PlatformTransactionManager tm) {
		super(stepId,
				SupplierWithStatus.fromParser(parser),
				(items) -> {
					items.forEach(item -> {
						item.setGeneration(ongoingGeneration);
						repository.save(item);
					});
				},
				tm);
	}

}
