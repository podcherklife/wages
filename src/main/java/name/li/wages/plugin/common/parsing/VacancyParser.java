package name.li.wages.plugin.common.parsing;

import org.openqa.selenium.WebDriver;

public interface VacancyParser<T> {
	T parse(WebDriver page);
}
