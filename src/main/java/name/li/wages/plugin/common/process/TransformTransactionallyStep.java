package name.li.wages.plugin.common.process;

import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

import name.li.wages.plugin.api.GeneratorStatus;
import name.li.wages.plugin.api.StatefulStatusWithKnownElementCount;
import name.li.wages.plugin.api.process.ProcessDescriptor.ContinuationToken;
import name.li.wages.plugin.api.process.Step;

public class TransformTransactionallyStep<T, R> implements Step {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private String id;
	private Supplier<Collection<T>> provider;
	private Function<T, R> transform;
	private Consumer<R> save;
	private TransactionTemplate tt;
	private final StatefulStatusWithKnownElementCount status;

	public TransformTransactionallyStep(
			String id,
			String description,
			Supplier<Collection<T>> provider,
			Function<T, R> transform,
			Consumer<R> save,
			PlatformTransactionManager tm) {
		this.id = id;
		this.provider = provider;
		this.transform = transform;
		this.save = save;
		this.tt = new TransactionTemplate(tm);
		this.tt.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
		this.status = new StatefulStatusWithKnownElementCount(description);
	}

	@Override
	public String id() {
		return id;
	}

	@Override
	public GeneratorStatus status() {
		return status;
	}

	@Override
	public void start() throws ProcessError {
		logger.info("Starting step '{}'", id());

		try {
			var providedItems = provider.get();
			status.start(providedItems.size());

			for (var item : providedItems) {
				tt.executeWithoutResult(perItemTransaction -> {
					var transformedItem = transform.apply(item);
					save.accept(transformedItem);
					status.incrementCompletedCount();
				});
			}
			status.finish();
		} catch (Exception e) {
			status.finishWithError();
			logger.error("Failed to complete step '{}': {}", id(), e.getMessage());
			throw new ProcessError(
					ContinuationToken.builder()
							.setValue(id())
							.setStepId(id())
							.build(),
					e);
		} finally {
			logger.info("Finished step '{}'", id());
		}

	}

}
