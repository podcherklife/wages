package name.li.wages.plugin.common.process;

import java.util.function.Function;
import java.util.stream.Stream;

import name.li.wages.plugin.api.GeneratorStatus;
import name.li.wages.plugin.common.parsing.SearchResultsParser;

public interface SupplierWithStatus<T> {
	GeneratorStatus status();

	T get();

	default <Q> SupplierWithStatus<Q> transform(Function<T, Q> transformer) {
		var outerSupplier = SupplierWithStatus.this;
		return new SupplierWithStatus<Q>() {

			@Override
			public GeneratorStatus status() {
				return outerSupplier.status();
			}

			@Override
			public Q get() {
				return transformer.apply(outerSupplier.get());
			}
		};
	}

	public static <T> SupplierWithStatus<Stream<T>> fromParser(SearchResultsParser<T> parser) {
		return new SupplierWithStatus<Stream<T>>() {
			public GeneratorStatus status() {
				return parser.status();
			}

			@Override
			public Stream<T> get() {
				return parser.generate();
			}
		};
	}
}
