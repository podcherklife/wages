package name.li.wages.plugin.common.parsing.paginated;

import java.util.stream.Stream;

import org.openqa.selenium.WebDriver;

public interface SearchPageHandler<T> {

	Stream<T> parseItems(WebDriver page);

}
