package name.li.wages.plugin.common.parsing.paginated;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import name.li.wages.plugin.api.GeneratorStatus;
import name.li.wages.plugin.api.StatefulStatusWithKnownElementCount;
import name.li.wages.plugin.common.parsing.SearchResultsParser;

public class PaginatedSearchParser<T> implements SearchResultsParser<T> {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final Supplier<WebDriver> getNewSeleniumDriver;
	private final URI searchResultPageUrl;
	private final SearchPageHandler<T> pageHandler;
	private final PaginationHandler paginationHandler;
	private final StatefulStatusWithKnownElementCount status;

	public PaginatedSearchParser(
			URI searchResultPageUrl,
			String statusDescription,
			Supplier<WebDriver> getNewSeleniumDriver,
			PaginationHandler paginationHandler,
			SearchPageHandler<T> pageHandler) {
		this.getNewSeleniumDriver = getNewSeleniumDriver;
		this.searchResultPageUrl = searchResultPageUrl;
		this.paginationHandler = paginationHandler;
		this.pageHandler = pageHandler;
		this.status = new StatefulStatusWithKnownElementCount(statusDescription);
	}

	private Stream<T> parsePageData(WebDriver searchPage) {
		return pageHandler.parseItems(searchPage);
	}

	public GeneratorStatus status() {
		return status;
	}

	private void setupPageCount() {
		var browser = getNewSeleniumDriver.get();
		try {
			browser.navigate().to(searchResultPageUrl.toString());
			status.start(paginationHandler.getPagesTotal(browser));
		} finally {
			browser.quit();
		}
	}

	public Stream<T> generate() {
		setupPageCount();

		var nextPageSearchUrl = Optional.of(searchResultPageUrl.toString());
		Collection<T> result = new ArrayList<>();
		logger.info("Loading all data. Status: {}", status);

		do {
			var page = getNewSeleniumDriver.get();
			try {
				page.navigate().to(nextPageSearchUrl.get());
				result.addAll(parsePageData(page).collect(Collectors.toList()));
				status.incrementCompletedCount();
				nextPageSearchUrl = paginationHandler.getNextPageUrl(page);
				logger.info("Finished one more page. Status: {}", status);
			} finally {
				page.quit();
			}
		} while (nextPageSearchUrl.isPresent());

		logger.info("Finished loading all data.");
		return result.stream();
	}

}
