package name.li.wages.plugin.common.persistence;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.google.common.base.MoreObjects;
import com.google.common.base.MoreObjects.ToStringHelper;

import name.li.wages.plugin.api.process.Generation;

@MappedSuperclass
public class DefaultFlowRawEntity {

	@Id
	@Column(name = "id")
	private String id = UUID.randomUUID().toString();

	@Column(name = "external_id")
	private String externalId = "";

	@ManyToOne
	private Generation generation;

	public String getId() {
		return id;
	}

	public String getExternalId() {
		return externalId;
	}

	public Generation getGeneration() {
		return generation;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public void setGeneration(Generation generation) {
		this.generation = generation;
	}

	public ToStringHelper baseToString(Class<? extends DefaultFlowRawEntity> clazz) {
		return MoreObjects.toStringHelper(clazz)
				.add("id", id)
				.add("externalId", externalId)
				.add("generation", generation);
	}

}
