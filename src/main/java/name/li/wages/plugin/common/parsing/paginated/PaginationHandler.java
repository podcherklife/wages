package name.li.wages.plugin.common.parsing.paginated;

import java.util.Optional;

import org.openqa.selenium.WebDriver;

public interface PaginationHandler {

	int getPagesTotal(WebDriver page);

	Optional<String> getNextPageUrl(WebDriver page);

}
