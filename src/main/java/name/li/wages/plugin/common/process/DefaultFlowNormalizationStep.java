package name.li.wages.plugin.common.process;

import java.util.Optional;
import java.util.function.Function;

import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.persistence.JobEntryRepository;
import name.li.wages.plugin.api.JobEntry;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.common.persistence.DefaultFlowRawEntity;
import name.li.wages.plugin.common.persistence.DefaultParsingFlowRawRepository;

public class DefaultFlowNormalizationStep<T extends DefaultFlowRawEntity>
		extends TransformTransactionallyStep<T, Optional<JobEntry>> {

	public DefaultFlowNormalizationStep(
			String id,
			DefaultParsingFlowRawRepository<T, ?> rawRepository,
			JobEntryRepository normalizedRepository,
			Function<T, Optional<JobEntry>> transform,
			Generation ongoingGeneration,
			PlatformTransactionManager tm) {
		super(id,
				"Normalizing job entries",
				rawRepository::getLastVersionsOfRawEntities,
				transform,
				mayBeItem -> mayBeItem.ifPresent(item -> {
					item.setGeneration(ongoingGeneration);
					normalizedRepository.save(item);
				}),
				tm);
	}

}
