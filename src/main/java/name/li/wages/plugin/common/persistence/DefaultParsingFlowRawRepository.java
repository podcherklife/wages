package name.li.wages.plugin.common.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface DefaultParsingFlowRawRepository<T extends DefaultFlowRawEntity, ID>
		extends CrudRepository<T, ID>, JpaSpecificationExecutor<T> {

	@Query("select t from #{#entityName} t where t.generation.instant in (select max(q.generation.instant) from #{#entityName} q group by q.externalId )")
	List<T> getLastVersionsOfRawEntities();
}
