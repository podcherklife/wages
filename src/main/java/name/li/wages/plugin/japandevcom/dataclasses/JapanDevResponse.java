package name.li.wages.plugin.japandevcom.dataclasses;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_JapanDevResponse.Builder.class)
@JsonSerialize
public abstract class JapanDevResponse {

	@JsonProperty
	public abstract Collection<JapanDevJob> data();

	public static Builder builder() {
		return new AutoValue_JapanDevResponse.Builder();
	}

	@AutoValue.Builder
	@JsonPOJOBuilder(buildMethodName = "build", withPrefix = "set")
	public abstract static class Builder {

		public abstract Builder setData(Collection<JapanDevJob> data);

		public abstract JapanDevResponse build();
	}
}
