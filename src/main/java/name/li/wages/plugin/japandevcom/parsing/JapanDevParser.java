package name.li.wages.plugin.japandevcom.parsing;

import java.io.IOException;
import java.util.Optional;
import java.util.stream.Stream;

import name.li.wages.plugin.api.GeneratorStatus;
import name.li.wages.plugin.api.StatefulStatusWithKnownElementCount;
import name.li.wages.plugin.common.parsing.SearchResultsParser;
import name.li.wages.plugin.japandevcom.dataclasses.JapanDevJob;
import name.li.wages.plugin.japandevcom.persistence.JapanDevRawJobEntry;

public class JapanDevParser implements SearchResultsParser<JapanDevRawJobEntry> {

	private final JapanDevResponseParser responseParser;
	private final StatefulStatusWithKnownElementCount status;
	private final JapanDevSource source;

	public JapanDevParser(JapanDevResponseParser responseParser, JapanDevSource source) {
		this.source = source;
		this.status = new StatefulStatusWithKnownElementCount("JapanDev parsing");
		this.responseParser = responseParser;
	}

	public Stream<JapanDevJob> generateResponseItems() {
		try {
			status.start(1);
			var result = responseParser.parse(source.get())
					.data()
					.stream()
					.filter(e -> Optional.ofNullable(e.attributes().title())
							.map(title -> !title.toLowerCase().contains("example")).orElse(false));
			status.finish();
			return result;
		} catch (IOException e) {
			status.finishWithError();
			throw new ParserException(e);
		}

	}

	@Override
	public Stream<JapanDevRawJobEntry> generate() {
		return generateResponseItems()
				.map(item -> JapanDevRawJobEntry.fromResponseItem(item, responseParser.getObjectMapper()));

	}

	@Override
	public GeneratorStatus status() {
		return status;
	}

}
