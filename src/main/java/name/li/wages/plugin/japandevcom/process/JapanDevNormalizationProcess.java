package name.li.wages.plugin.japandevcom.process;

import java.util.Optional;

import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.persistence.JobEntryRepository;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.common.process.DefaultFlowNormalizationStep;
import name.li.wages.plugin.japandevcom.normalization.JapanDevNormalizer;
import name.li.wages.plugin.japandevcom.persistence.JapanDevRawJobEntryRepository;

public class JapanDevNormalizationProcess implements ProcessDescriptor {

	private final JapanDevRawJobEntryRepository rawJobEntryRepository;
	private final JobEntryRepository jobEntryRepository;
	private final JapanDevNormalizer normalizer;
	private final PlatformTransactionManager tm;

	public JapanDevNormalizationProcess(
			JapanDevRawJobEntryRepository rawJobEntryRepository,
			JobEntryRepository jobEntryRepository,
			JapanDevNormalizer normalizer,
			PlatformTransactionManager tm) {
		this.rawJobEntryRepository = rawJobEntryRepository;
		this.jobEntryRepository = jobEntryRepository;
		this.normalizer = normalizer;
		this.tm = tm;
	}

	@Override
	public String id() {
		return "japanDevNormalization";
	}

	@Override
	public Step createExecution(Optional<ContinuationToken> token, Optional<Generation> lastGeneration,
			Generation ongoingGeneration) {
		return new DefaultFlowNormalizationStep<>(
				id(),
				rawJobEntryRepository,
				jobEntryRepository,
				normalizer::normalize,
				ongoingGeneration,
				tm);

	}

	@Override
	public boolean ownsToken(ContinuationToken token) {
		return token.stepId().equals(id());
	}

}
