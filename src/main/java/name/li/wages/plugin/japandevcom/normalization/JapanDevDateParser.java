package name.li.wages.plugin.japandevcom.normalization;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;

import com.google.common.collect.ImmutableSet;

public class JapanDevDateParser {
	private static final ImmutableSet<DateTimeFormatter> KNOWN_FORMATS = ImmutableSet.<DateTimeFormatter>builder()
			.add(DateTimeFormatter.ofPattern("MMM [ ]d[d]['th']['st']['rd'][,] yyyy"))
			.add(DateTimeFormatter.ofPattern("MMMM [ ]d[d]['th']['st']['rd'][,] yyyy"))
			.add(DateTimeFormatter.ofPattern("yyyy-M[M]-dd"))
			.build();

	public LocalDate parse(String str) throws DateTimeParseException {
		Optional<LocalDate> parsedValue = KNOWN_FORMATS.stream()
				.map(format -> {
					try {
						return Optional.of(LocalDate.parse(str, format));
					} catch (DateTimeParseException e) {
						return Optional.<LocalDate>empty();
					}
				})
				.filter(Optional::isPresent)
				.map(Optional::get)
				.findFirst();
		if (parsedValue.isPresent()) {
			return parsedValue.get();
		} else {
			throw new DateTimeParseException("Failed to parse date using any known format " + str, str, 0);
		}

	}

}
