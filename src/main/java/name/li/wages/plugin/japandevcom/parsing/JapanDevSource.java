package name.li.wages.plugin.japandevcom.parsing;

import java.io.InputStream;
import java.util.function.Supplier;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

public interface JapanDevSource extends Supplier<InputStream> {

	public static JapanDevSource fromInputStream(InputStream stream) {
		return new JapanDevSource() {

			@Override
			public InputStream get() {
				return stream;
			}
		};
	}

	public static JapanDevSource fromRestEndpoint() {
		return new JapanDevSource() {

			@Override
			public InputStream get() {
				return ClientBuilder.newClient().target("https://japan-dev.com/api/v1/jobs")
						.request(MediaType.APPLICATION_JSON).get(InputStream.class);
			}
		};
	}

}
