package name.li.wages.plugin.japandevcom.normalization;

import java.util.Arrays;
import java.util.Optional;

import name.li.wages.plugin.api.Location;
import name.li.wages.plugin.common.CompositeRegexParser;

public class JapanDevLocationParser {

	private CompositeRegexParser<Location> compositeParser;

	public JapanDevLocationParser() {
		this.compositeParser = new CompositeRegexParser<Location>(
				Arrays.asList(
						new CompositeRegexParser.SingleRegexParser<Location>("^([A-z]+)\\s*\\(([A-z\\-\\s,]*)\\)$",
								matcher -> Location.builder()
										.setCountry("japan")
										.setCity(matcher.group(1))
										.setArea(matcher.group(2))
										.build()),
						new CompositeRegexParser.SingleRegexParser<Location>("([Rr]emote)",
								matcher -> Location.builder()
										.setCountry("remote")
										.setCity("remote")
										.withoutArea()
										.build()),
						new CompositeRegexParser.SingleRegexParser<Location>("^([A-z]+)$",
								matcher -> Location.builder()
										.setCountry("japan")
										.setCity(matcher.group(1))
										.withoutArea()
										.build())));
	}

	public Optional<Location> parse(String str) {
		return compositeParser.parse(str);
	}

}
