package name.li.wages.plugin.japandevcom.dataclasses;

import java.util.Collection;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_JapanDevJobAttributes.Builder.class)
@JsonSerialize
public abstract class JapanDevJobAttributes {
	@JsonProperty
	@Nullable
	public abstract String title();

	@JsonProperty
	@Nullable
	public abstract String slug();

	@JsonProperty("salary_max")
	@Nullable
	public abstract Integer salaryMax();

	@JsonProperty
	@Nullable
	public abstract JapanDevCompany company();

	@JsonProperty
	@Nullable
	public abstract String location();

	@JsonProperty("salary_min")
	@Nullable
	public abstract Integer salaryMin();

	@JsonProperty
	@Nullable
	public abstract Collection<JapanDevSkill> skills();

	@JsonProperty
	@Nullable
	public abstract String technologies();

	@JsonProperty("job_post_date")
	@Nullable
	public abstract String jobPostDate();

	public static Builder builder() {
		return new AutoValue_JapanDevJobAttributes.Builder();
	}

	@AutoValue.Builder
	@JsonPOJOBuilder(buildMethodName = "build", withPrefix = "set")
	@JsonIgnoreProperties(ignoreUnknown = true)
	public abstract static class Builder {
		public abstract Builder setTitle(String title);

		@JsonProperty("salary_max")
		public abstract Builder setSalaryMax(Integer salaryMax);

		@JsonProperty("salary_min")
		public abstract Builder setSalaryMin(Integer salaryMin);

		@JsonProperty("job_post_date")
		public abstract Builder setJobPostDate(String date);

		public abstract Builder setCompany(JapanDevCompany company);

		public abstract Builder setLocation(String location);

		public abstract Builder setSkills(Collection<JapanDevSkill> skills);

		public abstract Builder setTechnologies(String technologies);

		public abstract Builder setSlug(String slug);

		public abstract JapanDevJobAttributes build();
	}
}
