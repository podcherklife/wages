package name.li.wages.plugin.japandevcom.dataclasses;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_JapanDevJob.Builder.class)
@JsonSerialize
public abstract class JapanDevJob {

	@JsonProperty
	public abstract int id();

	@JsonProperty
	public abstract String type();

	@JsonProperty
	public abstract JapanDevJobAttributes attributes();

	public static Builder builder() {
		return new AutoValue_JapanDevJob.Builder();
	}

	@AutoValue.Builder
	@JsonPOJOBuilder(buildMethodName = "build", withPrefix = "set")
	public abstract static class Builder {
		public abstract Builder setId(int id);

		public abstract Builder setType(String id);

		public abstract Builder setAttributes(JapanDevJobAttributes attributes);

		public abstract JapanDevJob build();
	}
}
