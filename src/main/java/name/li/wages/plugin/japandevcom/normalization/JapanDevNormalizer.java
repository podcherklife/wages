package name.li.wages.plugin.japandevcom.normalization;

import java.time.format.DateTimeParseException;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.databind.ObjectMapper;

import name.li.wages.plugin.api.Buzzword;
import name.li.wages.plugin.api.JobEntry;
import name.li.wages.plugin.api.Salary;
import name.li.wages.plugin.japandevcom.persistence.JapanDevRawJobEntry;

public class JapanDevNormalizer {

	private final JapanDevDateParser dateParser;
	private final JapanDevLocationParser locationParser;
	private final ObjectMapper objectMapper;

	public JapanDevNormalizer() {
		this.dateParser = new JapanDevDateParser();
		this.locationParser = new JapanDevLocationParser();
		this.objectMapper = new ObjectMapper();
	}

	public Optional<JobEntry> normalize(JapanDevRawJobEntry rawEntry) {
		var responseItem = rawEntry.toResponseItem(objectMapper);
		var title = responseItem.attributes().title();
		if (title == null
				|| title.toLowerCase().contains("example")) {
			return Optional.empty();
		}
		var jobEntry = new JobEntry();
		Optional.ofNullable(responseItem.attributes().location())
				.flatMap(locationParser::parse)
				.ifPresentOrElse(jobEntry::setLocation,
						() -> jobEntry.addParsingProblem(
								"Failed to parse location from '" + responseItem.attributes().location() + "'"));
		Optional.ofNullable(responseItem.attributes().company())
				.ifPresent(company -> jobEntry.setCompany(company.name()));
		Optional.ofNullable(responseItem.attributes().jobPostDate())
				.ifPresent(date -> {
					try {
						jobEntry.setCreatedOn(dateParser.parse(date));
					} catch (DateTimeParseException e) {
						jobEntry.addParsingProblem(e.getMessage());
					}
				});
		jobEntry.setJobUrl(
				String.format("japan-dev.com/jobs/%s/%s", jobEntry.getCompany(),
						responseItem.attributes().slug()));
		jobEntry.setExternalId(jobEntry.getJobUrl());
		var minSalary = Optional.ofNullable(responseItem.attributes().salaryMin())
				.map(OptionalInt::of)
				.orElseGet(OptionalInt::empty);
		var maxSalary = Optional.ofNullable(responseItem.attributes().salaryMax())
				.map(OptionalInt::of)
				.orElseGet(OptionalInt::empty);
		if (minSalary.isPresent() || maxSalary.isPresent()) {
			jobEntry.setSalary(Salary.builder()
					.yearly()
					.setMinSalary(minSalary)
					.setMaxSalary(maxSalary)
					.setCurrency("JPY")
					.build());
		}
		jobEntry.setPosition(Optional.ofNullable(responseItem.attributes().title()).orElse(""));
		jobEntry.setBuzzwords(Optional.ofNullable(responseItem.attributes().technologies())
				.map(technologies -> technologies.split(","))
				.stream()
				.flatMap(Stream::of)
				.map(String::trim)
				.map(Buzzword::of)
				.collect(Collectors.toSet()));
		return Optional.of(jobEntry);
	}

}
