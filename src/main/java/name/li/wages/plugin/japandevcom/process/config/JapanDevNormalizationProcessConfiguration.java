package name.li.wages.plugin.japandevcom.process.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.persistence.JobEntryRepository;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.japandevcom.normalization.JapanDevNormalizer;
import name.li.wages.plugin.japandevcom.persistence.JapanDevRawJobEntryRepository;
import name.li.wages.plugin.japandevcom.process.JapanDevNormalizationProcess;

@Configuration
public class JapanDevNormalizationProcessConfiguration {

	@Bean
	public ProcessDescriptor japanDevNormalization(
			JapanDevRawJobEntryRepository rawJobEntryRepository,
			JobEntryRepository jobEntryRepository,
			PlatformTransactionManager tm) {
		return new JapanDevNormalizationProcess(
				rawJobEntryRepository,
				jobEntryRepository,
				new JapanDevNormalizer(),
				tm);
	}
}
