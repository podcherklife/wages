package name.li.wages.plugin.japandevcom.parsing;

import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import name.li.wages.plugin.japandevcom.dataclasses.JapanDevResponse;

public class JapanDevResponseParser {

	private ObjectMapper objectMapper = new ObjectMapper();

	public JapanDevResponse parse(InputStream is) throws JsonParseException, JsonMappingException, IOException {
		return objectMapper.readValue(is, JapanDevResponse.class);
	}

	public ObjectMapper getObjectMapper() {
		return objectMapper;
	}

}
