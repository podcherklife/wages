package name.li.wages.plugin.japandevcom.process;

import java.util.Optional;

import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.common.process.DefaultFlowRawParsingStep;
import name.li.wages.plugin.japandevcom.parsing.JapanDevParser;
import name.li.wages.plugin.japandevcom.parsing.JapanDevResponseParser;
import name.li.wages.plugin.japandevcom.persistence.JapanDevRawJobEntryRepository;

public class JapanDevParsingProcess implements ProcessDescriptor {

	private final JapanDevRawJobEntryRepository rawJobEntryRepository;
	private final PlatformTransactionManager tm;
	private final JapanDevParser japanDevParser;

	public JapanDevParsingProcess(
			JapanDevParser japanDevParser,
			JapanDevResponseParser responseParser,
			JapanDevRawJobEntryRepository rawJobEntryRepository,
			PlatformTransactionManager tm) {
		this.japanDevParser = japanDevParser;
		this.rawJobEntryRepository = rawJobEntryRepository;
		this.tm = tm;
	}

	@Override
	public String id() {
		return "japanDevParsing";
	}

	@Override
	public Step createExecution(
			Optional<ContinuationToken> token,
			Optional<Generation> lastGeneration,
			Generation ongoingGeneration) {
		return new DefaultFlowRawParsingStep<>(ongoingGeneration, id(), japanDevParser, rawJobEntryRepository, tm);
	}

	@Override
	public boolean ownsToken(ContinuationToken token) {
		return token.stepId().equals(id());
	}

}
