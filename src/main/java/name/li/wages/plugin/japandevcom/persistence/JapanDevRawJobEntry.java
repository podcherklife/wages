package name.li.wages.plugin.japandevcom.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.ObjectMapper;

import name.li.wages.plugin.common.persistence.DefaultFlowRawEntity;
import name.li.wages.plugin.japandevcom.dataclasses.JapanDevJob;

@Entity
@Table(name = "japan_dev_raw_job_entry")
public class JapanDevRawJobEntry extends DefaultFlowRawEntity {

	@Lob
	@Column(name = "json_content")
	private String jsonContent = "";

	public String getJsonContent() {
		return jsonContent;
	}

	public void setJsonContent(String jsonContent) {
		this.jsonContent = jsonContent;
	}

	@Override
	public String toString() {
		return baseToString(getClass())
				.add("json_content", jsonContent)
				.toString();
	}

	public JapanDevJob toResponseItem(ObjectMapper objectMapper) {
		try {
			return objectMapper.readValue(jsonContent, JapanDevJob.class);
		} catch (Exception e) {
			throw new RuntimeException("Failed to create responseItem from japanDev raw entity", e);
		}
	}

	public static JapanDevRawJobEntry fromResponseItem(JapanDevJob responseItem, ObjectMapper objectMapper) {
		try {
			String json = objectMapper.writeValueAsString(responseItem);
			var result = new JapanDevRawJobEntry();
			result.setExternalId(String.valueOf(responseItem.id()));
			result.setJsonContent(json);
			return result;
		} catch (Exception e) {
			throw new RuntimeException("Failed to create raw entity for japanDevResponse item", e);
		}
	}

}
