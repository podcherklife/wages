package name.li.wages.plugin.japandevcom.process.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.japandevcom.parsing.JapanDevParser;
import name.li.wages.plugin.japandevcom.parsing.JapanDevResponseParser;
import name.li.wages.plugin.japandevcom.parsing.JapanDevSource;
import name.li.wages.plugin.japandevcom.persistence.JapanDevRawJobEntryRepository;
import name.li.wages.plugin.japandevcom.process.JapanDevParsingProcess;

@Configuration
public class JapanDevParsingProcessConfiguration {

	@Bean
	public ProcessDescriptor japanDevProcess(
			JapanDevRawJobEntryRepository rawJobEntryRepository,
			PlatformTransactionManager tm) {
		var responseParser = new JapanDevResponseParser();
		return new JapanDevParsingProcess(
				new JapanDevParser(responseParser, JapanDevSource.fromRestEndpoint()),
				responseParser,
				rawJobEntryRepository,
				tm);
	}

}
