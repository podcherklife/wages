package name.li.wages.plugin.japandevcom.persistence;

import name.li.wages.plugin.common.persistence.DefaultParsingFlowRawRepository;

public interface JapanDevRawJobEntryRepository extends DefaultParsingFlowRawRepository<JapanDevRawJobEntry, String> {

}
