package name.li.wages.plugin.japandevcom.dataclasses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_JapanDevSkill.Builder.class)
@JsonSerialize
public abstract class JapanDevSkill {

	@JsonProperty
	public abstract String id();

	@JsonProperty("system_name")
	public abstract String systemName();

	public static Builder builder() {
		return new AutoValue_JapanDevSkill.Builder();
	}

	@AutoValue.Builder
	@JsonPOJOBuilder(buildMethodName = "build", withPrefix = "set")
	@JsonIgnoreProperties(ignoreUnknown = true)
	public abstract static class Builder {

		public abstract Builder setId(String id);

		@JsonProperty("system_name")
		public abstract Builder setSystemName(String systemName);

		public abstract JapanDevSkill build();

	}

}
