package name.li.wages.plugin.hh.parsing;

import java.net.URI;
import java.util.List;
import java.util.stream.Stream;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import name.li.wages.plugin.common.parsing.paginated.SearchPageHandler;
import name.li.wages.plugin.hh.parsing.HhClient.SearchConfiguration;
import name.li.wages.plugin.hh.persistence.HhRawJobEntry;

public class HhSearchPageHandler implements SearchPageHandler<HhRawJobEntry> {

	private HhUrlIdExtractor idExtractor;
	private SearchConfiguration searchConfiguration;

	public HhSearchPageHandler(SearchConfiguration searchConfiguration) {
		this.searchConfiguration = searchConfiguration;
		this.idExtractor = new HhUrlIdExtractor();
	}

	@Override
	public Stream<HhRawJobEntry> parseItems(WebDriver page) {
		List<WebElement> vacancyBlocks = page.findElements(By.className("vacancy-serp-item"));
		return vacancyBlocks.stream().map(this::createEntryFromVacancyBlock);
	}

	private HhRawJobEntry createEntryFromVacancyBlock(WebElement block) {
		HhRawJobEntry entry = new HhRawJobEntry();
		try {
			var company = block.findElement(By.cssSelector("[data-qa=vacancy-serp__vacancy-employer]")).getText();
			entry.setEmployer(company);
		} catch (NoSuchElementException e) {
			// in some very rare cases employer is not available
			entry.setEmployer("N/A");
		}

		try {
			// this block should be present if salary is not specified
			var headerSidebar = block
					.findElement(By.cssSelector(".vacancy-serp-item__row_header .vacancy-serp-item__sidebar"));
			try {
				// this block is missing if salary is not specified
				var rawSalary = headerSidebar
						.findElement(By.cssSelector("[data-qa=vacancy-serp__vacancy-compensation]"))
						.getText();
				entry.setSalary(rawSalary);
			} catch (NoSuchElementException e) {
				// its fine, salary is not specified
			}
		} catch (NoSuchElementException e) {
			entry.addParsingProblem("Failed to parse salary, cant find element");
		}

		try {
			var link = block.findElement(By.cssSelector("[data-qa=vacancy-serp__vacancy-title]")).getAttribute("href");
			var uri = new URI(link);
			entry.setJobUrl(uri.getScheme() + "://" + uri.getHost() + uri.getPath());
			idExtractor.tryParse(link).ifPresentOrElse(
					entry::setExternalId,
					() -> entry.addParsingProblem("Failed to parse hh id"));
		} catch (Exception e) {
			entry.addParsingProblem("Failed to parse link");
		}

		String title = block.findElement(By.cssSelector("[data-qa=vacancy-serp__vacancy-title]")).getText();
		String address = block.findElement(By.cssSelector("[data-qa=vacancy-serp__vacancy-address]")).getText();
		String shortDescription = block.findElement(By.cssSelector("div.g-user-content")).getText();
		entry.setTitle(title);
		entry.setLocation(address);
		entry.setCountry(searchConfiguration.location().country());
		entry.setShortDescription(shortDescription);
		entry.setSearchedBy(searchConfiguration.term());
		try {
			var date = block.findElement(By.cssSelector("[data-qa=vacancy-serp__vacancy-date]")).getText();
			entry.setCreationDate(date);
		} catch (Exception e) {
			entry.addParsingProblem("Failed to parse creationDate, cant find element");
		}
		return entry;
	}

}
