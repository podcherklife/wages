package name.li.wages.plugin.hh.parsing;

import java.net.URI;
import java.util.function.Supplier;

import org.openqa.selenium.WebDriver;

import name.li.wages.plugin.common.parsing.SearchResultsParser;
import name.li.wages.plugin.common.parsing.paginated.PaginatedSearchParser;
import name.li.wages.plugin.hh.parsing.HhClient.SearchConfiguration;
import name.li.wages.plugin.hh.persistence.HhRawJobEntry;

public class HhSearchParserFactory {

	public SearchResultsParser<HhRawJobEntry> newSearch(
			URI searchResultPageUrl,
			Supplier<WebDriver> seleniumDriverSupplier,
			SearchConfiguration searchConfiguration) {
		return new PaginatedSearchParser<HhRawJobEntry>(searchResultPageUrl,
				String.format(
						"Parsing raw data for location '%s' and term '%s' ",
						searchConfiguration.location().name(),
						searchConfiguration.term()),
				seleniumDriverSupplier,
				new HhPaginationHandler(),
				new HhSearchPageHandler(searchConfiguration));
	}
}
