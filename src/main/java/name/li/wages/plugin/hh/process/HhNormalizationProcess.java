package name.li.wages.plugin.hh.process;

import java.util.Optional;

import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.persistence.JobEntryRepository;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.common.process.DefaultFlowNormalizationStep;
import name.li.wages.plugin.hh.normalization.HhDataNormalizer;
import name.li.wages.plugin.hh.persistence.HhRawJobEntryRepository;

public class HhNormalizationProcess implements ProcessDescriptor {

	private HhDataNormalizer normalizer;
	private HhRawJobEntryRepository rawJobEntryRepository;
	private JobEntryRepository jobEntryRepository;
	private PlatformTransactionManager ptm;

	public HhNormalizationProcess(
			HhDataNormalizer normalizer,
			HhRawJobEntryRepository rawJobEntryRepository,
			JobEntryRepository jobEntryRepository,
			PlatformTransactionManager ptm) {
		this.normalizer = normalizer;
		this.rawJobEntryRepository = rawJobEntryRepository;
		this.jobEntryRepository = jobEntryRepository;
		this.ptm = ptm;
	}

	@Override
	public String id() {
		return "hhNormalizationProcess";
	}

	@Override
	public Step createExecution(
			Optional<ContinuationToken> token,
			Optional<Generation> lastGeneration,
			Generation ongoingGeneration) {
		return new DefaultFlowNormalizationStep<>(
				id(),
				rawJobEntryRepository,
				jobEntryRepository,
				normalizer::normalize,
				ongoingGeneration, ptm);
	}

	@Override
	public boolean ownsToken(ContinuationToken token) {
		return id().equals(token.stepId());
	}
}
