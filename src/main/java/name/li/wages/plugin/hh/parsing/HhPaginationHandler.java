package name.li.wages.plugin.hh.parsing;

import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import name.li.wages.plugin.common.parsing.paginated.PaginationHandler;

public class HhPaginationHandler implements PaginationHandler {

	public HhPaginationHandler() {
	}

	@Override
	public int getPagesTotal(WebDriver page) {
		List<WebElement> pageControls = page.findElements(By.cssSelector("[data-qa=pager-page]"));
		OptionalInt maxPage = pageControls.stream()
				.mapToInt(e -> Integer.valueOf(Optional
						.ofNullable(e.getAttribute("data-page"))
						.orElse("0")))
				.max();
		return maxPage.orElse(1);
	}

	@Override
	public Optional<String> getNextPageUrl(WebDriver page) {
		try {
			return Optional.of(page.findElement(By.cssSelector("[data-qa=pager-next]")))
					.map(e -> e.getAttribute("href"));
		} catch (NoSuchElementException e) {
			return Optional.empty();
		}
	}

}
