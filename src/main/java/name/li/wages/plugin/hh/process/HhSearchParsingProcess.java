package name.li.wages.plugin.hh.process;

import java.util.Optional;

import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.common.process.DefaultFlowRawParsingStep;
import name.li.wages.plugin.hh.parsing.HhClient;
import name.li.wages.plugin.hh.parsing.HhClient.SearchConfiguration;
import name.li.wages.plugin.hh.persistence.HhRawJobEntry;
import name.li.wages.plugin.hh.persistence.HhRawJobEntryRepository;

public class HhSearchParsingProcess implements ProcessDescriptor {

	private HhRawJobEntryRepository rawJobEntryRepository;
	private SearchConfiguration searchConfig;
	private HhClient hhClient;
	private PlatformTransactionManager tm;

	public HhSearchParsingProcess(
			HhRawJobEntryRepository rawJobEntryRepository,
			SearchConfiguration searchConfig,
			HhClient hhClient,
			PlatformTransactionManager tm) {
		this.rawJobEntryRepository = rawJobEntryRepository;
		this.searchConfig = searchConfig;
		this.hhClient = hhClient;
		this.tm = tm;
	}

	@Override
	public String id() {
		return "hh search results parsing " + searchConfig.term() + " in " + searchConfig.location();
	}

	@Override
	public Step createExecution(
			Optional<ContinuationToken> token,
			Optional<Generation> lastGeneration,
			Generation ongoingGeneration) {
		return new DefaultFlowRawParsingStep<HhRawJobEntry>(
				ongoingGeneration,
				id(),
				hhClient.getSearchResultForLocationAndTerm(searchConfig, lastGeneration),
				rawJobEntryRepository,
				tm);
	}

	@Override
	public boolean ownsToken(ContinuationToken token) {
		return id().equals(token.stepId());
	}

}
