package name.li.wages.plugin.hh.normalization;

import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import name.li.wages.plugin.api.Buzzword;
import name.li.wages.plugin.hh.persistence.HhRawJobEntry;

public class HhEnricher {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private Supplier<WebDriver> seleniumProvider;

	public HhEnricher(Supplier<WebDriver> seleniumProvider) {
		this.seleniumProvider = seleniumProvider;
	}

	public HhRawJobEntry enrich(HhRawJobEntry entry) {
		logger.info("Enriching entry {}", entry.getJobUrl());
		var jobPage = seleniumProvider.get();
		jobPage.navigate().to(entry.getJobUrl());

		try {
			// this vacancy is archieved, do nothing
			jobPage.findElement(By.className("vacancy-archive-description"));
			entry.setArchieved(true);
		} catch (NoSuchElementException ex) {
			var description = jobPage.findElement(By.className("vacancy-description")).getText();
			var skills = jobPage.findElements(By.cssSelector("[data-qa~='skills-element']")).stream()
					.map(e -> e.getText());
			entry.setLongDescription(description);
			entry.setBuzzwords(skills.map(Buzzword::of).collect(Collectors.toSet()));
		}
		return entry;
	}

}
