package name.li.wages.plugin.hh.normalization;

import java.util.Arrays;

import name.li.wages.plugin.api.Salary;
import name.li.wages.plugin.common.CompositeRegexParser;
import name.li.wages.plugin.common.CompositeRegexParser.SingleRegexParser;
import name.li.wages.plugin.common.EmptyResultAllowingRegexParser;
import name.li.wages.plugin.common.ParsingResult;

public class HhSalaryParser {

	private EmptyResultAllowingRegexParser<Salary> parser;
	private static String SPACES = "\\s"
			+ "\\u202F"; // nbsp
	private static String POSSIBLE_NUMBER_CHARS = "\\d" + SPACES;
	private static String NUMBER_REGEX = "[" + POSSIBLE_NUMBER_CHARS + "]+";
	private static String ANY_SPACE_REGEX = "[" + SPACES + "]*";

	public HhSalaryParser() {
		this.parser = new EmptyResultAllowingRegexParser<>(new CompositeRegexParser<>(Arrays.asList(
				new SingleRegexParser<Salary>(
						"^(" + NUMBER_REGEX + ")-(" + NUMBER_REGEX + ")\\s*(.*)$",
						matcher -> Salary.builder()
								.monthly()
								.setMinSalary(Integer.parseInt(matcher.group(1).replaceAll(ANY_SPACE_REGEX, "")))
								.setMaxSalary(Integer.parseInt(matcher.group(2).replaceAll(ANY_SPACE_REGEX, "")))
								.setCurrency(HhCurrencyDict.findIsoCode(matcher.group(3).trim()))
								.build()),
				new SingleRegexParser<Salary>(
						"^от\\s*(" + NUMBER_REGEX + ")\\s*(.*)$",
						matcher -> Salary.builder()
								.monthly()
								.setMinSalary(Integer.parseInt(matcher.group(1).replaceAll(ANY_SPACE_REGEX, "")))
								.setCurrency(HhCurrencyDict.findIsoCode(matcher.group(2).trim()))
								.build()),
				new SingleRegexParser<Salary>(
						"^до\\s*(" + NUMBER_REGEX + ")\\s*(.*)$",
						matcher -> Salary.builder()
								.monthly()
								.setMaxSalary(Integer.parseInt(matcher.group(1).replaceAll(ANY_SPACE_REGEX, "")))
								.setCurrency(HhCurrencyDict.findIsoCode(matcher.group(2).trim()))
								.build()))),
				Arrays.asList("з/п не указана"));

	}

	public ParsingResult<Salary> parse(String str) {
		return parser.tryParse(str);
	}
}
