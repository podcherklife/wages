package name.li.wages.plugin.hh.normalization;

import java.time.LocalDate;
import java.time.Month;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.inject.Named;

import name.li.wages.plugin.api.JobEntry;
import name.li.wages.plugin.hh.persistence.HhRawJobEntry;
import name.li.wages.projection.CyrillicRomanizer;

@Named
public class HhDataNormalizer {

	private final static Map<String, Month> monthDict = new HashMap<>() {
		private static final long serialVersionUID = 1L;

		{
			put("декабря", Month.DECEMBER);
			put("ноября", Month.NOVEMBER);
			put("октября", Month.OCTOBER);
			put("сентября", Month.SEPTEMBER);
			put("августа", Month.AUGUST);
			put("июля", Month.JULY);
			put("июня", Month.JUNE);
			put("мая", Month.MAY);
			put("апреля", Month.APRIL);
			put("марта", Month.MARCH);
			put("февраля", Month.FEBRUARY);
			put("января", Month.JANUARY);
		}
	};

	private HhSalaryParser salaryParser = new HhSalaryParser();
	private CyrillicRomanizer romanizer = new CyrillicRomanizer();

	public Optional<JobEntry> normalize(HhRawJobEntry rawEntry) {
		var result = new JobEntry();

		salaryParser.parse(rawEntry.getSalary())
				.ifMatchedOrElse(
						result::setSalary,
						() -> result.addParsingProblem("Failed to parse salary: " + rawEntry.getSalary()));

		result.setCompany(rawEntry.getEmployer());
		result.setPosition(rawEntry.getTitle());
		result.setLocation(
				new HhLocationParser(romanizer.romanize(rawEntry.getCountry().toLowerCase()))
						.parseLocation(romanizer.romanize(rawEntry.getLocation().toLowerCase())));

		parseDate(rawEntry.getCreationDate())
				.ifPresentOrElse(
						result::setCreatedOn,
						() -> result.addParsingProblem("Failed to parse creation date"));
		result.setExternalId("hh|" + rawEntry.getExternalId());
		result.setJobUrl(rawEntry.getJobUrl());
		result.setBuzzwords(rawEntry.getBuzzwords());
		return Optional.of(result);
	}

	private Optional<LocalDate> parseDate(String dateStr) {
		try {
			var parts = dateStr.split(" ");
			var day = Integer.parseInt(parts[0]);
			var month = monthDict.get(parts[1]);
			return Optional.of(LocalDate.now().withMonth(month.getValue()).withDayOfMonth(day));
		} catch (Exception e) {
			return Optional.empty();
		}
	}

}
