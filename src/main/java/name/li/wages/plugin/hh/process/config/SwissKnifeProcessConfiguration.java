package name.li.wages.plugin.hh.process.config;

import java.io.Serializable;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import javax.persistence.EntityManager;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.plugin.api.GeneratorStatus;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.hh.normalization.HhDataNormalizer;
import name.li.wages.plugin.hh.parsing.HhUrlIdExtractor;
import name.li.wages.plugin.hh.persistence.HhRawJobEntryRepository;
import name.li.wages.selenium.SeleniumProvider;
import name.li.wages.spark.ServiceLocator;
import name.li.wages.spark.SparkPipeline;

@Configuration
public class SwissKnifeProcessConfiguration {

	@Bean
	public ProcessDescriptor swissKnife(
			HhRawJobEntryRepository rawJobRepo,
			HhDataNormalizer normalizer,
			PlatformTransactionManager ptm,
			SeleniumProvider selenium,
			EntityManager em,
			ApplicationContext springContext) {
		return new ProcessDescriptor() {

			@Override
			public boolean ownsToken(ContinuationToken token) {
				return token.stepId().equals("swissKnife");
			}

			@Override
			public String id() {
				return "swissKnife";
			}

			@Override
			public Step createExecution(Optional<ContinuationToken> token, Optional<Generation> lastGeneration,
					Generation ongoingGeneration) {
				return new Step() {

					@Override
					public GeneratorStatus status() {
						return GeneratorStatus.simple(() -> 100);
					}

					@Override
					public void start() throws ProcessError {

						SparkPipeline p = new SparkPipeline();
						try {
							p.doSparkMagic(new SpringServiceLocator(springContext));
						} catch (InterruptedException e) {
							throw new RuntimeException();
						}
					}

					@Override
					public String id() {
						return "doMagic";
					}
				};
			}
		};
	}

	public static class SpringServiceLocator implements Serializable, ServiceLocator {

		private static final long serialVersionUID = 1L;

		private static AtomicReference<ApplicationContext> appContext = new AtomicReference<>();

		public SpringServiceLocator(ApplicationContext appContext) {
			SpringServiceLocator.appContext.compareAndExchange(null, appContext);
		}

		@Override
		public <T> T get(Class<T> clazz) {
			return appContext.get().getBean(clazz);
		}

	}
}
