package name.li.wages.plugin.hh.process.config;

import java.util.Arrays;
import java.util.Collection;

import javax.inject.Named;

import name.li.wages.plugin.hh.parsing.HhClient.Location;
import name.li.wages.plugin.hh.parsing.HhClient.SearchConfiguration;

@Named
public class HhSearchConfigurations {
	public Collection<SearchConfiguration> getConfigurations() {
		return Arrays.asList(
				SearchConfiguration.builder().setLocation(Location.MOSCOW).setTerm("java").build(),
				SearchConfiguration.builder().setLocation(Location.RYAZAN).setTerm("java").build(),
				SearchConfiguration.builder().setLocation(Location.KAZAKHSTAN).setTerm("java").build());
	}
}
