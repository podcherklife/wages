package name.li.wages.plugin.hh.normalization;

import com.google.common.collect.ImmutableMap;

public class HhCurrencyDict {
	private static String RUB = "RUB";
	private static String USD = "USD";
	private static String KZT = "KZT";
	private static String EUR = "EUR";
	private static String BYR = "BYR";
	private static String UAH = "UAH";
	private static final ImmutableMap<String, String> map = ImmutableMap.<String, String>builder()
			.put("руб.", RUB)
			.put("грн.", UAH)
			.put("USD", USD)
			.put("KZT", KZT)
			.put("EUR", EUR)
			.put("бел. руб.", BYR)
			.build();

	public static String findIsoCode(String hhCurrencyValue) {
		var result = map.get(hhCurrencyValue);
		if (result == null) {
			throw new IllegalStateException("Failed to find currency ISO code for " + hhCurrencyValue);
		}
		return result;
	}
}
