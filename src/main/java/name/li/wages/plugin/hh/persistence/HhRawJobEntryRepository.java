package name.li.wages.plugin.hh.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import name.li.wages.plugin.common.persistence.DefaultParsingFlowRawRepository;

public interface HhRawJobEntryRepository extends DefaultParsingFlowRawRepository<HhRawJobEntry, String> {

	@Query("select t from HhRawJobEntry t where generation.id = ?1  and length(externalId) > 0 ") // fix for ads
	List<HhRawJobEntry> getAllEntriesForGeneration(String generationId);

}
