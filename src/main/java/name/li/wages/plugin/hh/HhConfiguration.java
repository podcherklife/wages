package name.li.wages.plugin.hh;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HhConfiguration {
	@Value("${hh.default-search-period}")
	private long defaultSearchPeriod;

	public HhConfiguration(
			@Value("${hh.default-search-period}") long defaultSearchPeriod) {
		this.defaultSearchPeriod = defaultSearchPeriod;

	}

	public long defaultSearchPeriod() {
		return defaultSearchPeriod;
	}

}
