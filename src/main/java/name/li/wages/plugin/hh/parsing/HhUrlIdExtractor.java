package name.li.wages.plugin.hh.parsing;

import name.li.wages.plugin.common.CompositeRegexParser.SingleRegexParser;

public class HhUrlIdExtractor extends SingleRegexParser<String> {

	public HhUrlIdExtractor() {
		super("vacancy/(\\d*)[/$\\?#]?", (matcher) -> matcher.group(1));
	}

}
