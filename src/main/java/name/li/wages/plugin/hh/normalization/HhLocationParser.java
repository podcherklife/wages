package name.li.wages.plugin.hh.normalization;

import name.li.wages.plugin.api.Location;
import name.li.wages.plugin.common.CompositeRegexParser.SingleRegexParser;

public class HhLocationParser {

	private final String country;

	private SingleRegexParser<Location> actualParser;

	public HhLocationParser(String country) {
		this.country = country;
		this.actualParser = new SingleRegexParser<>("^\\s*([\\p{L}]*)\\s*,([\\p{L}0-9\\-\\s]*)$",
				matcher -> Location.builder()
						.setCity(matcher.group(1).trim())
						.setArea(matcher.group(2).trim())
						.setCountry(country)
						.build());
	}

	public Location parseLocation(String locationString) {
		return actualParser.tryParse(locationString)
				.orElse(Location.builder()
						.setCountry(country)
						.setCity(locationString)
						.withoutArea()
						.build());
	}

}
