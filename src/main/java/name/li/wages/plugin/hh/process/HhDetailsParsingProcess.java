package name.li.wages.plugin.hh.process;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.common.process.TransformTransactionallyStep;
import name.li.wages.plugin.hh.normalization.HhEnricher;
import name.li.wages.plugin.hh.persistence.HhRawJobEntry;
import name.li.wages.plugin.hh.persistence.HhRawJobEntryRepository;

public class HhDetailsParsingProcess implements ProcessDescriptor {

	private HhRawJobEntryRepository rawJobEntryRepository;
	private PlatformTransactionManager tm;
	private HhEnricher enricher;

	@Override
	public String id() {
		return "Parsing job details";
	}

	@Override
	public Collection<ProcessDescriptor> subSteps() {
		return Collections.emptyList();
	}

	@Override
	public Step createExecution(
			Optional<ContinuationToken> token,
			Optional<Generation> lastGeneration,
			Generation ongoingGeneration) {
		return new TransformTransactionallyStep<HhRawJobEntry, Optional<HhRawJobEntry>>(
				id(),
				"Parsing extended job details",
				() -> rawJobEntryRepository.getAllEntriesForGeneration(ongoingGeneration.getId()),
				(item) -> {
					if (item.hasExtendedData()) {
						return Optional.empty();
					} else {
						enricher.enrich(item);
						item.setHasExtendedData(true);
						return Optional.of(item);
					}
				},
				(maybeItem) -> maybeItem.ifPresent(rawJobEntryRepository::save),
				tm);
	}

	@Override
	public boolean ownsToken(ContinuationToken token) {
		return token.stepId().equals(id());
	}

	public HhDetailsParsingProcess(
			HhRawJobEntryRepository rawJobEntryRepository,
			PlatformTransactionManager tm,
			HhEnricher enricher) {
		this.rawJobEntryRepository = rawJobEntryRepository;
		this.tm = tm;
		this.enricher = enricher;
	}

}
