package name.li.wages.plugin.hh.process.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.persistence.JobEntryRepository;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.hh.normalization.HhDataNormalizer;
import name.li.wages.plugin.hh.persistence.HhRawJobEntryRepository;
import name.li.wages.plugin.hh.process.HhNormalizationProcess;

@Configuration
public class HhNormalizationProcessConfiguration {

	@Bean
	public ProcessDescriptor hhNormalizationProcess(
			HhDataNormalizer normalizer,
			HhRawJobEntryRepository rawJobEntryRepository,
			JobEntryRepository jobEntryRepository,
			PlatformTransactionManager ptm) {
		return new HhNormalizationProcess(normalizer, rawJobEntryRepository, jobEntryRepository, ptm);
	}

}
