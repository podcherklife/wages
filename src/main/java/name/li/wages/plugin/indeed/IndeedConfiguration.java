package name.li.wages.plugin.indeed;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IndeedConfiguration {

	private long defaultSearchPeriod;

	public IndeedConfiguration(
			@Value("${indeed.default-search-period}") long defaultSearchPeriod) {
		this.defaultSearchPeriod = defaultSearchPeriod;

	}

	public long defaultSearchPeriod() {
		return defaultSearchPeriod;
	}

}