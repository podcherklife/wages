package name.li.wages.plugin.indeed.process.config;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.plugin.api.process.CompositeProcess;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.indeed.IndeedConfiguration;
import name.li.wages.plugin.indeed.IndeedSearchConfiguration;
import name.li.wages.plugin.indeed.IndeedSearchConfiguration.Location;
import name.li.wages.plugin.indeed.parsing.IndeedClient;
import name.li.wages.plugin.indeed.persistence.IndeedRawJobEntryRepository;
import name.li.wages.plugin.indeed.process.IndeeedSearchParsingProcess;
import name.li.wages.selenium.SeleniumProvider;

@Configuration
public class IndeedSearchParsingProcessConfiguration {

	@Bean
	public ProcessDescriptor indeedParsing(
			IndeedRawJobEntryRepository rawRepository,
			PlatformTransactionManager ptm,
			IndeedClient indeedClient) {
		return new CompositeProcess("indeedParsing",
				"Parsing Indeed",
				Arrays.asList(
						new IndeeedSearchParsingProcess(rawRepository, ptm, indeedClient,
								IndeedSearchConfiguration.builder()
										.setLocation(Location.NEW_YORK)
										.setTerm("java")
										.build()),
						new IndeeedSearchParsingProcess(rawRepository, ptm, indeedClient,
								IndeedSearchConfiguration.builder()
										.setLocation(Location.CHICAGO)
										.setTerm("java")
										.build()),
						new IndeeedSearchParsingProcess(rawRepository, ptm, indeedClient,
								IndeedSearchConfiguration.builder()
										.setLocation(Location.SAN_FRANCISCO)
										.setTerm("java")
										.build())));
	}

	@Bean
	public IndeedClient indeedClient(SeleniumProvider selenium, IndeedConfiguration indeedConfiguration) {
		return new IndeedClient(selenium, indeedConfiguration);
	}

}
