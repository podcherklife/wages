package name.li.wages.plugin.indeed.parsing;

import java.net.URI;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import javax.ws.rs.core.UriBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.common.parsing.SearchResultsParser;
import name.li.wages.plugin.common.parsing.paginated.PaginatedSearchParser;
import name.li.wages.plugin.indeed.IndeedConfiguration;
import name.li.wages.plugin.indeed.IndeedSearchConfiguration;
import name.li.wages.plugin.indeed.persistence.IndeedRawJobEntry;
import name.li.wages.selenium.SeleniumProvider;

public class IndeedClient {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public static final int ITEMS_PER_PAGE = 15; // over values do not seem to work
	public static final int MAX_POSSIBLE_ELEMENTS = 1000;

	private final SeleniumProvider selenium;
	private final IndeedConfiguration indeedConfig;

	public IndeedClient(
			SeleniumProvider selenium,
			IndeedConfiguration indeedConfig) {
		this.selenium = selenium;
		this.indeedConfig = indeedConfig;
	}

	public SearchResultsParser<IndeedRawJobEntry> getSearchResultForLocationAndTerm(
			IndeedSearchConfiguration searchConfiguration,
			Optional<Generation> lastGeneration) {
		return new PaginatedSearchParser<>(
				getSearchResult(searchConfiguration, lastGeneration),
				String.format(
						"Parsing Indeed raw data for location '%s' and term '%s' ",
						searchConfiguration.location().name(),
						searchConfiguration.term()),
				selenium::getPooledDriver,
				new IndeedPaginationHandler(),
				new IndeedSearchPageHandler(searchConfiguration));
	}

	private URI getSearchResult(
			IndeedSearchConfiguration searchConfguration,
			Optional<Generation> lastGeneration) {
		try {
			var defaultSearchPeriod = indeedConfig.defaultSearchPeriod();
			var searchPeriod = lastGeneration.map(gen -> {
				var daysSinceLastGeneration = getDaysSinceLastGeneration(gen);
				logger.info("Last generation was performed on {}, {} days ago, using {} as search_period",
						gen.getDate(), daysSinceLastGeneration, daysSinceLastGeneration);
				return daysSinceLastGeneration;
			}).orElseGet(() -> {
				logger.info("No last generation found, using {} as search_period", defaultSearchPeriod);
				return defaultSearchPeriod;
			});
			return UriBuilder
					.fromPath(indeedBaseUrl())
					.path("jobs")
					.queryParam("sort", "date")
					.queryParam("limit", ITEMS_PER_PAGE)
					.queryParam("st", "searchVacancy")
					.queryParam("fromage", searchPeriod)
					.queryParam("radius", 25)
					.queryParam("l", searchConfguration.location().locationStr())
					.queryParam("q", searchConfguration.term()).build();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private String indeedBaseUrl() {
		return "https://www.indeed.com/";
	}

	private long getDaysSinceLastGeneration(Generation generation) {
		return Math.max(ChronoUnit.DAYS.between(generation.getDate(), LocalDate.now()), 1);
	}
}
