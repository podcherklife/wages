package name.li.wages.plugin.indeed.normalization;

import java.util.Arrays;

import name.li.wages.plugin.api.Salary;
import name.li.wages.plugin.common.CompositeRegexParser;
import name.li.wages.plugin.common.CompositeRegexParser.SingleRegexParser;
import name.li.wages.plugin.common.EmptyResultAllowingRegexParser;
import name.li.wages.plugin.common.ParsingResult;

public class IndeedSalaryParser {

	private final EmptyResultAllowingRegexParser<Salary> compositeParser;

	public IndeedSalaryParser() {
		this.compositeParser = new EmptyResultAllowingRegexParser<>(new CompositeRegexParser<>(Arrays.asList(
				new SingleRegexParser<Salary>(
						"^([,$\\d\\s]+)-([,$\\d\\s]+) an hour",
						matcher -> Salary.builder()
								.hourly()
								.setMinSalary(parseVal(matcher.group(1)))
								.setMaxSalary(parseVal(matcher.group(2)))
								.setCurrency("USD")
								.build()),
				new SingleRegexParser<Salary>(
						"^([,$\\d\\s]+) an hour",
						matcher -> Salary.builder()
								.hourly()
								.setMinSalary(parseVal(matcher.group(1)))
								.setMaxSalary(parseVal(matcher.group(1)))
								.setCurrency("USD")
								.build()),
				new SingleRegexParser<Salary>(
						"^([,$\\d\\s]+)-([,$\\d\\s]+) a year",
						matcher -> Salary.builder()
								.yearly()
								.setMinSalary(parseVal(matcher.group(1)))
								.setMaxSalary(parseVal(matcher.group(2)))
								.setCurrency("USD")
								.build()),
				new SingleRegexParser<Salary>(
						"^([,$\\d\\s]+) a year",
						matcher -> Salary.builder()
								.yearly()
								.setMaxSalary(parseVal(matcher.group(1)))
								.setMinSalary(parseVal(matcher.group(1)))
								.setCurrency("USD")
								.build()))),
				Arrays.asList("^$"));

	}

	private Integer parseVal(String valueStr) {
		return Integer.parseInt(valueStr.replaceAll("[\\s,$]", ""));
	}

	public ParsingResult<Salary> parse(String str) {
		return compositeParser.tryParse(str);
	}
}
