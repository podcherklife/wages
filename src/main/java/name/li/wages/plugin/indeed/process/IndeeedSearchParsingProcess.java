package name.li.wages.plugin.indeed.process;

import java.util.Optional;

import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.common.process.DefaultFlowRawParsingStep;
import name.li.wages.plugin.indeed.IndeedSearchConfiguration;
import name.li.wages.plugin.indeed.parsing.IndeedClient;
import name.li.wages.plugin.indeed.persistence.IndeedRawJobEntryRepository;

public class IndeeedSearchParsingProcess implements ProcessDescriptor {

	private final IndeedRawJobEntryRepository rawRepository;
	private final PlatformTransactionManager ptm;
	private final IndeedClient indeedClient;
	private final IndeedSearchConfiguration searchConfiguration;

	public IndeeedSearchParsingProcess(
			IndeedRawJobEntryRepository rawRepository,
			PlatformTransactionManager ptm,
			IndeedClient indeedClient,
			IndeedSearchConfiguration searchConfiguration) {
		this.rawRepository = rawRepository;
		this.ptm = ptm;
		this.indeedClient = indeedClient;
		this.searchConfiguration = searchConfiguration;
	}

	@Override
	public String id() {
		return "indeed search results parsing " + searchConfiguration.term() + " in " + searchConfiguration.location();
	}

	@Override
	public Step createExecution(
			Optional<ContinuationToken> token,
			Optional<Generation> lastGeneration,
			Generation ongoingGeneration) {
		return new DefaultFlowRawParsingStep<>(
				ongoingGeneration,
				id(),
				indeedClient.getSearchResultForLocationAndTerm(searchConfiguration, lastGeneration),
				rawRepository,
				ptm);
	}

	@Override
	public boolean ownsToken(ContinuationToken token) {
		return token.stepId().equals(id());
	}

}
