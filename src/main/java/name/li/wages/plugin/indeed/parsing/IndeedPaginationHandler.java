package name.li.wages.plugin.indeed.parsing;

import java.util.Optional;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import name.li.wages.plugin.common.parsing.paginated.PaginationHandler;

public class IndeedPaginationHandler implements PaginationHandler {

	private static Pattern pageCounterRegex = Pattern.compile("page ([0-9,]*) of ([0-9,]*) jobs");

	@Override
	public int getPagesTotal(WebDriver page) {
		return calculatePageCount(page.findElement(By.id("searchCountPages")).getText());
	}

	@Override
	public Optional<String> getNextPageUrl(WebDriver page) {
		try {
			return Optional.of(page.findElement(By.cssSelector("[aria-label=Next]")).getAttribute("href"));
		} catch (NoSuchElementException e) {
			return Optional.empty();
		}
	}

	public int calculatePageCount(String elementCounter) {
		try {
			var matcher = pageCounterRegex.matcher(elementCounter.toLowerCase());
			matcher.find();
			var totalItemsString = matcher.group(2);
			var totalItems = Integer.parseInt(totalItemsString.replace(",", ""));
			var totalEffectiveItems = Math.min(IndeedClient.MAX_POSSIBLE_ELEMENTS, totalItems);
			var totalPages = (int) Math.ceil((double) totalEffectiveItems / IndeedClient.ITEMS_PER_PAGE);
			return totalPages;
		} catch (Exception e) {
			throw new RuntimeException("Failed to parse page count for string " + elementCounter, e);
		}
	}
}
