package name.li.wages.plugin.indeed.process.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.persistence.JobEntryRepository;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.indeed.normalization.IndeedNormalizer;
import name.li.wages.plugin.indeed.persistence.IndeedRawJobEntryRepository;
import name.li.wages.plugin.indeed.process.IndeedNormalizationProcess;

@Configuration
public class IndeedNormalizationProcessConfig {

	@Bean
	public ProcessDescriptor indeedNormalizationProcess(
			IndeedRawJobEntryRepository rawRepository,
			JobEntryRepository normalizedRepository,
			PlatformTransactionManager ptm) {
		return new IndeedNormalizationProcess(rawRepository, normalizedRepository, ptm, new IndeedNormalizer());
	}

}
