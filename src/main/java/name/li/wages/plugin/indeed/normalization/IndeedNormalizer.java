package name.li.wages.plugin.indeed.normalization;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

import name.li.wages.plugin.api.JobEntry;
import name.li.wages.plugin.api.Location;
import name.li.wages.plugin.indeed.persistence.IndeedRawJobEntry;

public class IndeedNormalizer {

	private final IndeedAgeParser ageParser = new IndeedAgeParser();
	private final IndeedSalaryParser salaryParser = new IndeedSalaryParser();

	public Optional<JobEntry> normalize(IndeedRawJobEntry rawEntry) {
		var result = new JobEntry();

		result.setBuzzwords(Collections.emptySet());
		result.setCompany(rawEntry.getEmployer());
		calculateCreatedOn(rawEntry).ifPresentOrElse(
				result::setCreatedOn,
				() -> result.addParsingProblem("Failed to calculate creation date"));
		parseLocation(rawEntry).ifPresentOrElse(
				result::setLocation,
				() -> result.addParsingProblem("Failed to parse location"));
		result.setPosition(rawEntry.getTitle());

		result.setExternalId("id|" + rawEntry.getExternalId());
		result.setJobUrl(rawEntry.getJobUrl());
		salaryParser.parse(rawEntry.getSalary())
				.ifMatchedOrElse(
						result::setSalary,
						() -> result.addParsingProblem("Failed to parse salary"));

		return Optional.of(result);
	}

	private Optional<LocalDate> calculateCreatedOn(IndeedRawJobEntry rawEntry) {
		var parsedOnDate = rawEntry.getDateParsed();
		var maybeAge = ageParser.parseAgeInDays(rawEntry.getAge());
		return maybeAge.map(age -> parsedOnDate.minusDays(age));
	}

	private Optional<Location> parseLocation(IndeedRawJobEntry rawEntry) {
		return new IndeedLocationParser(rawEntry.getCountry()).parse(rawEntry.getLocation());
	}

}
