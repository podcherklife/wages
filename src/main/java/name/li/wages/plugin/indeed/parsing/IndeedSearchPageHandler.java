package name.li.wages.plugin.indeed.parsing;

import java.time.LocalDate;
import java.util.stream.Stream;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import name.li.wages.plugin.common.CompositeRegexParser.SingleRegexParser;
import name.li.wages.plugin.common.parsing.paginated.SearchPageHandler;
import name.li.wages.plugin.indeed.IndeedSearchConfiguration;
import name.li.wages.plugin.indeed.persistence.IndeedRawJobEntry;

public class IndeedSearchPageHandler implements SearchPageHandler<IndeedRawJobEntry> {

	private final IndeedSearchConfiguration searchConfiguration;
	private final SingleRegexParser<String> idParser = new SingleRegexParser<>("^jl_(.*)$",
			(matcher) -> matcher.group(1));

	public IndeedSearchPageHandler(IndeedSearchConfiguration searchConfiguration) {
		this.searchConfiguration = searchConfiguration;
	}

	@Override
	public Stream<IndeedRawJobEntry> parseItems(WebDriver page) {
		return page.findElements(By.className("jobsearch-SerpJobCard"))
				.stream().map(this::generateEntryFromBlock);

	}

	private IndeedRawJobEntry generateEntryFromBlock(WebElement block) {
		var result = new IndeedRawJobEntry();

		result.setTitle(block.findElement(By.className("jobtitle")).getText());
		result.setJobUrl(block.findElement(By.className("jobtitle")).getAttribute("href"));
		result.setExternalId(idParser.tryParse(block.findElement(By.className("jobtitle")).getAttribute("id")).get());
		result.setEmployer(block.findElement(By.className("company")).getText());
		result.setLocation(block.findElement(By.className("location")).getText());
		result.setShortDescription(block.findElement(By.className("summary")).getText());
		result.setAge(block.findElement(By.className("date")).getText());
		result.setDateParsed(LocalDate.now());

		try {
			result.setSalary(block.findElement(By.className("salary")).getText());
		} catch (NoSuchElementException e) {
			// no salary for this job
		}

		result.setSearchedBy(searchConfiguration.term());
		result.setCountry(searchConfiguration.location().country());

		return result;
	}

}
