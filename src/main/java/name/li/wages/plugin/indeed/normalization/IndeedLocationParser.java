package name.li.wages.plugin.indeed.normalization;

import java.util.Arrays;
import java.util.Optional;

import name.li.wages.plugin.api.Location;
import name.li.wages.plugin.common.CompositeRegexParser;
import name.li.wages.plugin.common.CompositeRegexParser.SingleRegexParser;

public class IndeedLocationParser {

	private final CompositeRegexParser<Location> compositeParser;

	public IndeedLocationParser(String country) {
		this.compositeParser = new CompositeRegexParser<>(Arrays.asList(
				new SingleRegexParser<>(
						"^([\\w\\d\\s]+),([\\w\\d\\s\\(\\)]+)$",
						matcher -> Location.builder()
								.setCountry(country)
								.setCity(matcher.group(1).trim())
								.setArea(matcher.group(2).trim())
								.build())));

	}

	public Optional<Location> parse(String str) {
		return compositeParser.parse(str);
	}
}
