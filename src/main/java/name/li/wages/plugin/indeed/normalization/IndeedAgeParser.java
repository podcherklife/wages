package name.li.wages.plugin.indeed.normalization;

import java.util.Arrays;
import java.util.Optional;

import name.li.wages.plugin.common.CompositeRegexParser;
import name.li.wages.plugin.common.CompositeRegexParser.SingleRegexParser;

public class IndeedAgeParser {

	private final CompositeRegexParser<Integer> compositeParser;

	public IndeedAgeParser() {
		this.compositeParser = new CompositeRegexParser<>(Arrays.asList(
				new SingleRegexParser<>("^(\\d*) days? ago$", matcher -> Integer.parseInt(matcher.group(1))),
				new SingleRegexParser<>("^Just posted$", matcher -> 0),
				new SingleRegexParser<>("^Today$", matcher -> 0)));
	}

	public Optional<Integer> parseAgeInDays(String str) {
		return compositeParser.parse(str);
	}
}
