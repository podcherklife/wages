package name.li.wages.plugin.indeed.process;

import java.util.Optional;

import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.persistence.JobEntryRepository;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.common.process.DefaultFlowNormalizationStep;
import name.li.wages.plugin.indeed.normalization.IndeedNormalizer;
import name.li.wages.plugin.indeed.persistence.IndeedRawJobEntryRepository;

public class IndeedNormalizationProcess implements ProcessDescriptor {

	private final IndeedRawJobEntryRepository rawRepository;
	private final JobEntryRepository normalizedRepository;
	private final PlatformTransactionManager ptm;
	private final IndeedNormalizer normalizer;

	public IndeedNormalizationProcess(
			IndeedRawJobEntryRepository rawRepository,
			JobEntryRepository normalizedRepository,
			PlatformTransactionManager ptm,
			IndeedNormalizer normalizer) {
		this.rawRepository = rawRepository;
		this.normalizedRepository = normalizedRepository;
		this.ptm = ptm;
		this.normalizer = normalizer;
	}

	@Override
	public String id() {
		return "indeedNormalization";
	}

	@Override
	public Step createExecution(Optional<ContinuationToken> token, Optional<Generation> lastGeneration,
			Generation ongoingGeneration) {
		return new DefaultFlowNormalizationStep<>(
				id(),
				rawRepository,
				normalizedRepository,
				normalizer::normalize,
				ongoingGeneration,
				ptm);
	}

	@Override
	public boolean ownsToken(ContinuationToken token) {
		// TODO Auto-generated method stub
		return false;
	}

}
