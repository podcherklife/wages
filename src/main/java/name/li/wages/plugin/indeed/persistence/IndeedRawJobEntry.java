package name.li.wages.plugin.indeed.persistence;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.google.common.collect.Lists;

import name.li.wages.plugin.common.persistence.DefaultFlowRawEntity;

@Entity
@Table(name = "indeed_raw_job_entry")
public class IndeedRawJobEntry extends DefaultFlowRawEntity {

	@Column(name = "searched_by")
	private String searchedBy = "";

	@Column(name = "job_url")
	private String jobUrl = "";

	@Column(name = "salary")
	private String salary = "";

	@Column(name = "title")
	private String title = "";

	@Lob
	@Column(name = "short_description")
	private String shortDescription = "";

	@Lob
	@Column(name = "long_description")
	private String longDescription = "";

	@Column(name = "employer")
	private String employer = "";

	@Column(name = "location")
	private String location = "";

	@Column(name = "country")
	private String country = "";

	@Column(name = "age")
	private String age = "";

	@Column(name = "date_parsed")
	private LocalDate dateParsed;

	@Column(name = "has_extended_data")
	private boolean hasExtendedData = false;

	@ElementCollection
	private Collection<String> parsingProblems = Lists.newArrayList();

	public String getJobUrl() {
		return jobUrl;
	}

	public void setJobUrl(String url) {
		this.jobUrl = url;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public String getEmployer() {
		return employer;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void addParsingProblem(String problem) {
		parsingProblems.add(problem);
	}

	public Collection<String> getParsingProblems() {
		return Collections.unmodifiableCollection(parsingProblems);
	}

	public void clearParsingProblems() {
		parsingProblems = new ArrayList<>();
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setSearchedBy(String searchedBy) {
		this.searchedBy = searchedBy;
	}

	public String getSearchedBy() {
		return searchedBy;
	}

	public boolean hasExtendedData() {
		return hasExtendedData;
	}

	public void setHasExtendedData(boolean hasExtendedData) {
		this.hasExtendedData = hasExtendedData;
	}

	public LocalDate getDateParsed() {
		return dateParsed;
	}

	public void setDateParsed(LocalDate dateParsed) {
		this.dateParsed = dateParsed;
	}

	@Override
	public String toString() {
		return baseToString(getClass())
				.add("jobUrl", jobUrl)
				.add("country", country)
				.add("location", location)
				.add("salary", salary)
				.add("employer", employer)
				.add("shortDescription", shortDescription)
				.add("longDescription", longDescription)
				.add("title", title)
				.add("age", age)
				.add("dateParsed", dateParsed)
				.add("parsingProblems", parsingProblems)
				.add("searchedBy", searchedBy)
				.add("hasExtendedData", hasExtendedData)
				.toString();
	}

}
