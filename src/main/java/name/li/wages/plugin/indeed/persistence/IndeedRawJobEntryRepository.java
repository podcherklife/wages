package name.li.wages.plugin.indeed.persistence;

import name.li.wages.plugin.common.persistence.DefaultParsingFlowRawRepository;
import name.li.wages.plugin.indeed.persistence.IndeedRawJobEntry;

public interface IndeedRawJobEntryRepository extends DefaultParsingFlowRawRepository<IndeedRawJobEntry, String> {

}
