package name.li.wages.plugin.indeed;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class IndeedSearchConfiguration {

	public enum Location {
		NEW_YORK("New York State", "USA"),
		CHICAGO("Chicago, IL", "USA"),
		SAN_FRANCISCO("San Francisco, CA", "USA");

		private String locationStr;
		private String country;

		Location(String locationStr, String country) {
			this.locationStr = locationStr;
			this.country = country;
		}

		public String locationStr() {
			return locationStr;
		}

		public String country() {
			return country;
		}
	}

	public abstract Location location();

	public abstract String term();

	public static Builder builder() {
		return new AutoValue_IndeedSearchConfiguration.Builder();
	}

	@AutoValue.Builder
	public static abstract class Builder {
		public abstract Builder setLocation(Location location);

		public abstract Builder setTerm(String term);

		public abstract IndeedSearchConfiguration build();
	}

}
