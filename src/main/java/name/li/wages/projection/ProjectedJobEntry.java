package name.li.wages.projection;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.google.common.base.MoreObjects;

import name.li.wages.plugin.api.BuzzWordFlattener;
import name.li.wages.plugin.api.Buzzword;
import name.li.wages.plugin.api.BuzzwordEntity;
import name.li.wages.plugin.api.Location;
import name.li.wages.plugin.api.LocationEmbeddable;
import name.li.wages.plugin.api.Salary;
import name.li.wages.plugin.api.SalaryEmbeddable;
import name.li.wages.plugin.api.process.Generation;

@Entity
public class ProjectedJobEntry {

	@Id
	@Column(name = "id")
	private String externalId = "";

	@Column(name = "job_url")
	private String jobUrl = "";

	@Column(name = "created_on")
	private LocalDate createdOn;

	@ManyToOne
	private Generation generation;

	@ElementCollection
	private Collection<String> parsingProblems;

	@ManyToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	private Set<BuzzwordEntity> properBuzzwords = new HashSet<>();

	@Convert(converter = BuzzWordFlattener.class)
	@Column(name = "flat_buzzwords", length = 10_000)
	private Set<Buzzword> flatBuzzwords = new HashSet<>();

	@AttributeOverrides({
			@AttributeOverride(name = "from", column = @Column(name = "salary_original_min")),
			@AttributeOverride(name = "to", column = @Column(name = "salary_original_max")),
			@AttributeOverride(name = "currency", column = @Column(name = "salary_original_currency")),
			@AttributeOverride(name = "period", column = @Column(name = "salary_original_period")),
	})
	@Embedded
	private SalaryEmbeddable salary;

	@AttributeOverrides({
			@AttributeOverride(name = "from", column = @Column(name = "salary_usd_min")),
			@AttributeOverride(name = "to", column = @Column(name = "salary_usd_max")),
			@AttributeOverride(name = "currency", column = @Column(name = "salary_usd_currency")),
			@AttributeOverride(name = "period", column = @Column(name = "salary_usd_period")),
	})
	@Embedded
	private SalaryEmbeddable usdYearlySalary;

	@Embedded
	private LocationEmbeddable location;

	@Column(name = "company")
	private String company;

	@Column(name = "position")
	private String position;

	public void setCreatedOn(LocalDate createdOn) {
		this.createdOn = createdOn;
	}

	public void setCreatedOn(Optional<LocalDate> maybeCreatedOn) {
		maybeCreatedOn.ifPresent(this::setCreatedOn);
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public void setGeneration(Generation generation) {
		this.generation = generation;
	}

	public void setJobUrl(String jobUrl) {
		this.jobUrl = jobUrl;
	}

	public void setParsingProblems(Collection<String> parsingProblems) {
		this.parsingProblems = parsingProblems;
	}

	public void setBuzzwords(Set<Buzzword> properBuzzwords) {
		this.properBuzzwords = properBuzzwords.stream().map(BuzzwordEntity::from).collect(Collectors.toSet());
		this.flatBuzzwords = properBuzzwords;
	}

	public Set<Buzzword> getBuzzwords() {
		return Objects.requireNonNullElse(flatBuzzwords, Collections.emptySet());
	}

	public LocalDate getCreatedOn() {
		return createdOn;
	}

	public String getExternalId() {
		return externalId;
	}

	public Generation getGeneration() {
		return generation;
	}

	public String getJobUrl() {
		return jobUrl;
	}

	public Collection<String> getParsingProblems() {
		return parsingProblems;
	}

	public Optional<Salary> getSalary() {
		return Optional.ofNullable(salary).flatMap(SalaryEmbeddable::salary);
	}

	public void setSalary(Optional<Salary> mayBeSalary) {
		this.salary = mayBeSalary.map(SalaryEmbeddable::from).orElse(null);
	}

	public void setSalary(Salary salary) {
		this.salary = SalaryEmbeddable.from(salary);
	}

	public Optional<Salary> getUsdSalary() {
		return Optional.ofNullable(usdYearlySalary).flatMap(SalaryEmbeddable::salary);
	}

	public void setUsdYearlySalary(Optional<Salary> mayBeSalary) {
		this.usdYearlySalary = mayBeSalary.map(SalaryEmbeddable::from).orElse(null);
	}

	public void setUsdYearlySalary(Salary salary) {
		this.usdYearlySalary = SalaryEmbeddable.from(salary);
	}

	public void addParsingProblem(String problem) {
		if (this.parsingProblems == null) {
			this.parsingProblems = new ArrayList<>();
		}
		this.parsingProblems.add(problem);
	}

	public Location getLocation() {
		return Optional.ofNullable(location)
				.orElse(new LocationEmbeddable())
				.location();
	}

	public void setLocation(Location location) {
		this.location = LocationEmbeddable.from(location);
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public static ProjectedJobEntry fromProjectedJob(ProjectedJob job) {
		var result = new ProjectedJobEntry();
		result.setBuzzwords(job.buzzwords());
		result.setCompany(job.company());
		result.setCreatedOn(job.creationDate());
		result.setExternalId(job.externalId());
		result.setJobUrl(job.jobUrl());
		result.setLocation(job.location());
		result.setParsingProblems(job.parsingProblems());
		result.setPosition(job.position());
		result.setSalary(job.salary());
		result.setUsdYearlySalary(job.usdYearlySalary());
		return result;
	}

	public String toString() {
		return MoreObjects.toStringHelper(getClass())
				.add("id", externalId)
				.add("jobUrl", jobUrl)
				.add("company", company)
				.add("location", location)
				.add("position", position)
				.add("salary", salary)
				.add("usdYearlySalary", usdYearlySalary)
				.add("createdOn", createdOn)
				.add("parsingProblems", parsingProblems)
				.add("generation", generation)
				.add("buzzwords", flatBuzzwords)
				.toString();
	}

}
