package name.li.wages.projection;

import java.util.OptionalInt;

import javax.money.CurrencyUnit;
import javax.money.Monetary;

import org.javamoney.moneta.Money;

import name.li.wages.plugin.api.Salary;
import name.li.wages.projection.currency.CurrencyConverter.ToCurrencyConverter;

public class SalaryCurrencyConverter {

	private ToCurrencyConverter currencyConverter;

	public SalaryCurrencyConverter(ToCurrencyConverter currencyConverter) {
		this.currencyConverter = currencyConverter;
	}

	public Salary convert(Salary salary) {
		var fromCurrency = Monetary.getCurrency(salary.currency());
		return salary.toBuilder()
				.setCurrency(currencyConverter.currency().getCurrencyCode())
				.setMaxSalary(convert(salary.maxSalary(), fromCurrency))
				.setMinSalary(convert(salary.minSalary(), fromCurrency))
				.build();
	}

	private OptionalInt convert(OptionalInt mayBeValue, CurrencyUnit fromCurrency) {
		return mayBeValue.stream()
				.map(val -> currencyConverter.convert(Money.of(val, fromCurrency)).getNumber().intValue())
				.findFirst();
	}

}
