package name.li.wages.projection.currency;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CurrConvComConfig {

	@Value("${currency.currconv.apikey}")
	private String apiKey;

	@Bean
	public CurrConvComRatesProvider currConvComRatesProvider() {
		return new CurrConvComRatesProvider(apiKey);
	}

}
