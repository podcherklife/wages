package name.li.wages.projection.currency;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.persistence.AttributeConverter;

public class CurrencyUnitConverter implements AttributeConverter<CurrencyUnit, String> {

	@Override
	public String convertToDatabaseColumn(CurrencyUnit attribute) {
		return attribute == null ? null : attribute.getCurrencyCode();
	}

	@Override
	public CurrencyUnit convertToEntityAttribute(String dbData) {
		return dbData == null ? null : Monetary.getCurrency(dbData);
	}
}