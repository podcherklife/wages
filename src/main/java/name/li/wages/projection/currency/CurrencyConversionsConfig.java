package name.li.wages.projection.currency;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import name.li.wages.persistence.RateEntryRepository;
import name.li.wages.projection.SalaryConverterFactory;

@Configuration
public class CurrencyConversionsConfig {

	@Bean
	public DbRatesProviderDecorator defaultRatesProvider(
			RateEntryRepository ratesRepo,
			CurrConvComRatesProvider currConvRatesProvider) {
		return new DbRatesProviderDecorator(
				ratesRepo,
				new CompositeRatesProvider(Arrays.asList(
						new ExchangeRatesApiRatesProvider(),
						currConvRatesProvider)));
	}

	@Bean
	public CurrencyConverter defaultCurrencyConverter(DbRatesProviderDecorator ratesProvider) {
		return new SimpleCurrencyConverter(ratesProvider);
	}

	@Bean
	public SalaryConverterFactory defaultSalaryConversionFactory(CurrencyConverter currencyConverter) {
		return new SalaryConverterFactory(currencyConverter);
	}

}
