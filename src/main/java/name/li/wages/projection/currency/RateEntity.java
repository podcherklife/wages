package name.li.wages.projection.currency;

import java.time.LocalDate;
import java.util.UUID;

import javax.money.CurrencyUnit;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.common.base.MoreObjects;

import name.li.wages.projection.currency.RatesProvider.Rate;

@Entity
@Table(name = "currency_rate")
public class RateEntity {

	@Column(name = "rate")
	private Double rate;

	@Convert(converter = CurrencyUnitConverter.class)
	@Column(name = "fromCurrency")
	private CurrencyUnit from;

	@Convert(converter = CurrencyUnitConverter.class)
	@Column(name = "toCurrency")
	private CurrencyUnit to;

	@Column(name = "date")
	private LocalDate date;

	@Id
	@Column(name = "id")
	private String id = UUID.randomUUID().toString();

	public RateEntity() {
	}

	public CurrencyUnit getFrom() {
		return from;
	}

	public Double getRate() {
		return rate;
	}

	public CurrencyUnit getTo() {
		return to;
	}

	public void setFrom(CurrencyUnit from) {
		this.from = from;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public void setTo(CurrencyUnit to) {
		this.to = to;
	}

	public String getId() {
		return id;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalDate getDate() {
		return date;
	}

	public Rate toRate() {
		return Rate.builder()
				.setFrom(from)
				.setTo(to)
				.setRate(rate)
				.setOnDate(date)
				.build();
	}

	public static RateEntity fromRate(Rate rate) {
		var result = new RateEntity();
		result.setRate(rate.rate());
		result.setFrom(rate.from());
		result.setTo(rate.to());
		result.setDate(rate.onDate());
		return result;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(getClass())
				.add("id", id)
				.add("rate", rate)
				.add("from", from)
				.add("to", to)
				.add("date", date)
				.toString();
	}

}
