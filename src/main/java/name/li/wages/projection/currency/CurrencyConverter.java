package name.li.wages.projection.currency;

import java.time.LocalDate;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;

import org.javamoney.moneta.Money;

public interface CurrencyConverter {

	public interface ToCurrencyConverter {
		Money convert(MonetaryAmount amount, LocalDate onDate);

		default Money convert(MonetaryAmount amount) {
			return convert(amount, LocalDate.now());
		}

		CurrencyUnit currency();
	}

	Money convert(MonetaryAmount amount, CurrencyUnit toCurrency, LocalDate onDate);

	default Money convert(MonetaryAmount amount, CurrencyUnit toCurrency) {
		return convert(amount, toCurrency, LocalDate.now());
	}

	default ToCurrencyConverter toCurrency(CurrencyUnit toCurrency) {
		return new ToCurrencyConverter() {

			@Override
			public Money convert(MonetaryAmount amount, LocalDate onDate) {
				return CurrencyConverter.this.convert(amount, toCurrency, onDate);
			}

			@Override
			public CurrencyUnit currency() {
				return toCurrency;
			}
		};
	}

}
