package name.li.wages.projection.currency;

import java.time.LocalDate;

import javax.money.CurrencyUnit;

import com.google.auto.value.AutoValue;

public interface RatesProvider {
	@AutoValue
	public abstract class Rate {

		public abstract LocalDate onDate();

		public abstract CurrencyUnit from();

		public abstract CurrencyUnit to();

		public abstract double rate();

		public static Builder builder() {
			return new AutoValue_RatesProvider_Rate.Builder();
		}

		@AutoValue.Builder
		public abstract static class Builder {

			public abstract Builder setOnDate(LocalDate onDate);

			public abstract Builder setFrom(CurrencyUnit from);

			public abstract Builder setTo(CurrencyUnit to);

			public abstract Builder setRate(double rate);

			public abstract Rate build();
		}
	}

	public Rate getRate(CurrencyUnit from, CurrencyUnit to, LocalDate onDate) throws RetrievingRatesException;
}
