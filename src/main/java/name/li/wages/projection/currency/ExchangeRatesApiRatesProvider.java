package name.li.wages.projection.currency;

import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import javax.annotation.Nullable;
import javax.money.CurrencyUnit;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

public class ExchangeRatesApiRatesProvider implements RatesProvider {

	@AutoValue
	@JsonDeserialize(builder = AutoValue_ExchangeRatesApiRatesProvider_RatesResponse.Builder.class)
	public abstract static class RatesResponse {

		@Nullable
		public abstract Map<String, Double> rates();

		public abstract String base();

		public abstract String date();

		public static Builder builder() {
			return new AutoValue_ExchangeRatesApiRatesProvider_RatesResponse.Builder();
		}

		@AutoValue.Builder
		@JsonPOJOBuilder(buildMethodName = "build", withPrefix = "set")
		@JsonIgnoreProperties(ignoreUnknown = true)
		public abstract static class Builder {
			public abstract Builder setRates(Map<String, Double> rates);

			public abstract Builder setBase(String base);

			public abstract Builder setDate(String date);

			public abstract RatesResponse build();
		}
	}

	private Client restClient;
	private ObjectMapper objectMapper;
	private DateTimeFormatter dateFormatter;

	public ExchangeRatesApiRatesProvider() {
		this.restClient = ClientBuilder.newClient();
		this.objectMapper = new ObjectMapper();
		this.dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	}

	@Override
	public Rate getRate(CurrencyUnit from, CurrencyUnit to, LocalDate onDate) {
		var rates = getRates(to, onDate);
		var rate = rates.get(from.getCurrencyCode());

		if (rate == null) {
			throw new RetrievingRatesException(
					String.format("Something went wrong, failed to get rate for currency %s and date %s",
							from,
							onDate));
		}

		return Rate.builder().setFrom(from).setTo(to).setRate(rate).setOnDate(onDate).build();
	}

	private Map<String, Double> getRates(CurrencyUnit base, LocalDate date) {
		try {
			var response = objectMapper
					.readValue(restClient.target("https://api.exchangeratesapi.io/")
							.path(date.format(dateFormatter))
							.queryParam("base", base.getCurrencyCode())
							.request(MediaType.APPLICATION_JSON).get(InputStream.class), RatesResponse.class);
			return response.rates();
		} catch (Exception e) {
			throw new RetrievingRatesException(e);
		}
	}

}
