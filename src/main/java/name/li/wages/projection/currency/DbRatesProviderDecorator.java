package name.li.wages.projection.currency;

import java.time.LocalDate;

import javax.money.CurrencyUnit;

import name.li.wages.persistence.RateEntryRepository;

public class DbRatesProviderDecorator implements RatesProvider {

	private RateEntryRepository repository;
	private RatesProvider ratesProvider;

	public DbRatesProviderDecorator(
			RateEntryRepository repository,
			RatesProvider ratesProvider) {
		this.repository = repository;
		this.ratesProvider = ratesProvider;
	}

	@Override
	public Rate getRate(CurrencyUnit from, CurrencyUnit to, LocalDate onDate) {
		return repository
				.getRate(from, to, onDate)
				.map(RateEntity::toRate)
				.orElseGet(() -> {
					var providedRate = ratesProvider.getRate(from, to, onDate);
					repository.save(RateEntity.fromRate(providedRate));
					return providedRate;
				});
	}

}
