package name.li.wages.projection.currency;

import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import javax.money.CurrencyUnit;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;

public class CurrConvComRatesProvider implements RatesProvider {

	private final Client restClient;
	private final ObjectMapper objectMapper;
	private final DateTimeFormatter dateFormatter;
	private final String apiKey;

	public CurrConvComRatesProvider(String apiKey) {
		this.restClient = ClientBuilder.newClient();
		this.objectMapper = new ObjectMapper();
		this.dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		this.apiKey = apiKey;
	}

	@Override
	public Rate getRate(CurrencyUnit from, CurrencyUnit to, LocalDate onDate) {
		try {
			var rateStr = String.format("%s_%s", to.getCurrencyCode(), from.getCurrencyCode());
			var dateStr = onDate.format(dateFormatter);
			var response = objectMapper
					.readValue(restClient.target("https://free.currconv.com/api/v7/convert")
							.queryParam("apiKey", apiKey)
							.queryParam("date", dateStr)
							.queryParam("q", rateStr)
							.queryParam("compact", "ultra")
							.request(MediaType.APPLICATION_JSON).get(InputStream.class), Map.class);
			var ratesForDates = response.get(rateStr);
			if (ratesForDates == null) {
				throw new IllegalStateException(
						String.format("Service did not return any rates for currency pair %s", rateStr));
			}
			if (!(ratesForDates instanceof Map)) {
				throw new IllegalStateException(String.format("Unexpected response: %s", ratesForDates.toString()));
			}
			var rate = ((Map<?, ?>) ratesForDates).get(dateStr);
			if (rate == null) {
				throw new IllegalStateException(String.format("Service did not return rate for date %s", dateStr));
			}
			if (!(rate instanceof Double)) {
				throw new IllegalStateException(String.format("Unexected rate: %s", rate));
			}
			return Rate.builder()
					.setFrom(from)
					.setTo(to)
					.setOnDate(onDate)
					.setRate((double) rate)
					.build();
		} catch (RetrievingRatesException e) {
			throw e;
		} catch (Exception e) {
			throw new RetrievingRatesException(e);
		}
	}

}
