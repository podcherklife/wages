package name.li.wages.projection.currency;

import java.time.LocalDate;
import java.util.Collection;

import javax.money.CurrencyUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CompositeRatesProvider implements RatesProvider {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private final Collection<RatesProvider> ratesProviders;

	public CompositeRatesProvider(Collection<RatesProvider> ratesProviders) {
		this.ratesProviders = ratesProviders;
	}

	@Override
	public Rate getRate(CurrencyUnit from, CurrencyUnit to, LocalDate onDate) throws RetrievingRatesException {
		for (var provider : ratesProviders) {
			try {
				return provider.getRate(from, to, onDate);
			} catch (RetrievingRatesException e) {
				logger.info("Failed to get rate from provider {}:", provider, e.getMessage());
			}
		}
		throw new RetrievingRatesException(
				String.format("Failed to get rate %s-%s-%s  from any provider", from, to, onDate));
	}

}
