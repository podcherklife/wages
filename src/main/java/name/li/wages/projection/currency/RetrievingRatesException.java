package name.li.wages.projection.currency;

public class RetrievingRatesException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public RetrievingRatesException(String message) {
		super(message);
	}

	public RetrievingRatesException(Throwable reason) {
		super(reason);
	}

}
