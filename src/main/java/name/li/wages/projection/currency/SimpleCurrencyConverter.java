package name.li.wages.projection.currency;

import java.time.LocalDate;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;

import org.javamoney.moneta.Money;

public class SimpleCurrencyConverter implements CurrencyConverter {

	private RatesProvider ratesProvider;

	public SimpleCurrencyConverter(RatesProvider ratesProvider) {
		this.ratesProvider = ratesProvider;
	}

	@Override
	public Money convert(MonetaryAmount amount, CurrencyUnit toCurrency, LocalDate onDate) {
		var rate = ratesProvider.getRate(amount.getCurrency(), toCurrency, onDate);
		return Money.of(amount.getNumber().doubleValue() / rate.rate(), toCurrency);
	}

}
