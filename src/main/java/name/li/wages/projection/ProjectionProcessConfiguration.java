package name.li.wages.projection;

import java.util.Optional;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import name.li.wages.persistence.JobEntryRepository;
import name.li.wages.persistence.ProjectedJobEntryRepository;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.api.process.Step;

@Configuration
public class ProjectionProcessConfiguration {

	@Bean
	public ProcessDescriptor projectionProcess(
			JobEntryRepository jobEntryRepository,
			ProjectedJobEntryRepository projectedJobEntryRepository,
			SalaryConverterFactory salaryConverterFactory) {
		return new ProcessDescriptor() {

			@Override
			public boolean ownsToken(ContinuationToken token) {
				return token.stepId().equals(id());
			}

			@Override
			public String id() {
				return "jobEntriesProjecting";
			}

			@Override
			public Step createExecution(
					Optional<ContinuationToken> token,
					Optional<Generation> lastGeneration,
					Generation ongoingGeneration) {
				return new ProjectionStep(
						ongoingGeneration,
						id(),
						jobEntryRepository,
						projectedJobEntryRepository,
						salaryConverterFactory,
						new SalaryPeriodConverterFactory());
			}
		};
	}

}
