package name.li.wages.projection;

import java.util.Map;
import java.util.OptionalInt;

import com.google.common.collect.ImmutableMap;

import name.li.wages.plugin.api.Salary;
import name.li.wages.plugin.api.Salary.Period;

public class SalaryPeriodConverter {

	private static final Map<Period, Integer> PERIOD_RATIOS = ImmutableMap.<Period, Integer>builder()
			.put(Period.HOUR, 1)
			.put(Period.MONTH, 160)
			.put(Period.YEAR, 160 * 12)
			.build();

	private final Period targetPeriod;

	public SalaryPeriodConverter(Period period) {
		this.targetPeriod = period;
	}

	public Salary convert(Salary salary) {
		return salary.toBuilder()
				.setPeriod(targetPeriod)
				.setMaxSalary(convertVal(salary.maxSalary(), salary.period()))
				.setMinSalary(convertVal(salary.minSalary(), salary.period()))
				.build();

	}

	private OptionalInt convertVal(OptionalInt maybeVal, Period fromPeriod) {
		return maybeVal.stream().map(val -> {
			var toHourRatio = PERIOD_RATIOS.get(fromPeriod);
			return (int) Math.floor(((double) val / toHourRatio) * PERIOD_RATIOS.get(targetPeriod));
		}).findFirst();
	}

}
