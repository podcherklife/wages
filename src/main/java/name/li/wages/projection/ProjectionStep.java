package name.li.wages.projection;

import name.li.wages.persistence.JobEntryRepository;
import name.li.wages.persistence.ProjectedJobEntryRepository;
import name.li.wages.plugin.api.GeneratorStatus;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.Step;

public class ProjectionStep implements Step {

	private Generation ongoingGeneration;
	private String id;
	private JobEntryRepository jobEntryRepository;
	private ProjectedJobEntryRepository projectedJobEntryRepository;
	private volatile int state;
	private SalaryConverterFactory salaryConverterFactory;
	private SalaryPeriodConverterFactory salaryPeriodConverterFactory;

	public ProjectionStep(
			Generation ongoingGeneration,
			String id,
			JobEntryRepository jobEntryRepository,
			ProjectedJobEntryRepository projectedJobEntryRepository,
			SalaryConverterFactory salaryConverterFactory,
			SalaryPeriodConverterFactory salaryPeriodConverterFactory) {
		this.ongoingGeneration = ongoingGeneration;
		this.id = id;
		this.jobEntryRepository = jobEntryRepository;
		this.projectedJobEntryRepository = projectedJobEntryRepository;
		this.salaryConverterFactory = salaryConverterFactory;
		this.salaryPeriodConverterFactory = salaryPeriodConverterFactory;
	}

	@Override
	public String id() {
		return id;
	}

	@Override
	public GeneratorStatus status() {
		return GeneratorStatus.simple(() -> state);
	}

	@Override
	public void start() throws ProcessError {
		var projector = new JobEntryProjector(ongoingGeneration, salaryConverterFactory, salaryPeriodConverterFactory);
		state = 50;
		try {
			var items = jobEntryRepository
					.getAllEntriesWithDifferentProjectionTokenValue(projector.projectionToken());
			items
					.stream()
					.map(projector::project)
					.forEach(item -> {
						item.setGeneration(ongoingGeneration);
						projectedJobEntryRepository.save(item);
					});
		} finally {
			state = 100;
		}
	}

}
