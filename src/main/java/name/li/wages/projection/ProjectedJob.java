package name.li.wages.projection;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import com.esotericsoftware.kryo.DefaultSerializer;
import com.esotericsoftware.kryo.serializers.FieldSerializer;
import com.google.auto.value.AutoValue;

import name.li.wages.plugin.api.Buzzword;
import name.li.wages.plugin.api.Location;
import name.li.wages.plugin.api.Salary;

@AutoValue
public abstract class ProjectedJob {

	public abstract String externalId();

	public abstract String jobUrl();

	public abstract Optional<LocalDate> creationDate();

	public abstract Collection<String> parsingProblems();

	public abstract Set<Buzzword> buzzwords();

	public abstract Optional<Salary> salary();

	public abstract Optional<Salary> usdYearlySalary();

	public abstract Location location();

	public abstract String company();

	public abstract String position();

	public abstract Builder toBuilder();

	public static ProjectedJob.Builder builder() {
		return new AutoValue_ProjectedJob.Builder();
	}

	@AutoValue.Builder
	@DefaultSerializer(FieldSerializer.class)
	public static abstract class Builder {

		public abstract ProjectedJob.Builder setExternalId(String externalId);

		public abstract ProjectedJob.Builder setJobUrl(String jobUrl);

		public abstract ProjectedJob.Builder setCreationDate(Optional<LocalDate> creationDate);

		public abstract ProjectedJob.Builder setParsingProblems(Collection<String> parsingProblems);

		public abstract ProjectedJob.Builder setBuzzwords(Set<Buzzword> buzzwords);

		public abstract ProjectedJob.Builder setSalary(Optional<Salary> salary);

		public abstract ProjectedJob.Builder setUsdYearlySalary(Optional<Salary> usdYearlySalary);

		public abstract ProjectedJob.Builder setLocation(Location location);

		public abstract ProjectedJob.Builder setCompany(String company);

		public abstract ProjectedJob.Builder setPosition(String position);

		public abstract ProjectedJob build();
	}
}
