package name.li.wages.projection;

import javax.money.CurrencyUnit;

import name.li.wages.projection.currency.CurrencyConverter;

public class SalaryConverterFactory {

	private CurrencyConverter converter;

	public SalaryConverterFactory(CurrencyConverter converter) {
		this.converter = converter;
	}

	public SalaryCurrencyConverter toCurrency(CurrencyUnit currency) {
		return new SalaryCurrencyConverter(converter.toCurrency(currency));
	}

}
