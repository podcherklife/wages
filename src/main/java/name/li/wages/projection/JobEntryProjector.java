package name.li.wages.projection;

import javax.money.Monetary;

import name.li.wages.plugin.api.JobEntry;
import name.li.wages.plugin.api.Salary.Period;
import name.li.wages.plugin.api.process.Generation;

public class JobEntryProjector {

	private final Generation generation;
	private final SalaryCurrencyConverter salaryToUsdConverter;
	private final SalaryPeriodConverter salaryToYearlyConverter;

	public JobEntryProjector(
			Generation generation,
			SalaryConverterFactory salaryConverterFactory,
			SalaryPeriodConverterFactory salaryPeriodConverterFactory) {
		this.generation = generation;
		this.salaryToYearlyConverter = salaryPeriodConverterFactory.toPeriod(Period.YEAR);
		this.salaryToUsdConverter = salaryConverterFactory.toCurrency(Monetary.getCurrency("USD"));
	}

	public String projectionToken() {
		return "v0";
	}

	public ProjectedJobEntry project(JobEntry jobEntry) {
		var projectedJobEntry = new ProjectedJobEntry();

		projectedJobEntry.setExternalId(jobEntry.getExternalId());
		projectedJobEntry.setCreatedOn(jobEntry.getCreatedOn());
		projectedJobEntry.setParsingProblems(jobEntry.getParsingProblems());
		projectedJobEntry.setSalary(jobEntry.getSalary());
		projectedJobEntry.setUsdYearlySalary(
				jobEntry.getSalary()
						.map(salaryToUsdConverter::convert)
						.map(salaryToYearlyConverter::convert));
		projectedJobEntry.setBuzzwords(jobEntry.getBuzzwords());
		projectedJobEntry.setPosition(jobEntry.getPosition());
		projectedJobEntry.setLocation(jobEntry.getLocation());
		projectedJobEntry.setCompany(jobEntry.getCompany());
		projectedJobEntry.setJobUrl(jobEntry.getJobUrl());

		jobEntry.setProjectionToken(projectionToken());

		projectedJobEntry.setGeneration(generation);

		return projectedJobEntry;
	}

}
