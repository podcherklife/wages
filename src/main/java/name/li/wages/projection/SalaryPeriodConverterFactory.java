package name.li.wages.projection;

import name.li.wages.plugin.api.Salary.Period;

public class SalaryPeriodConverterFactory {

	public SalaryPeriodConverter toPeriod(Period period) {
		return new SalaryPeriodConverter(period);
	}

}
