package name.li.wages.spark;

import javax.money.Monetary;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import name.li.wages.plugin.api.Salary.Period;
import name.li.wages.projection.SalaryConverterFactory;
import name.li.wages.projection.SalaryCurrencyConverter;
import name.li.wages.projection.SalaryPeriodConverter;
import name.li.wages.projection.currency.CurrencyConverter;

@Configuration
public class SparkBeans {

	@Bean
	public SalaryCurrencyConverter salaryToUsdConverter(CurrencyConverter currencyConverter) {
		return new SalaryConverterFactory(currencyConverter).toCurrency(Monetary.getCurrency("USD"));
	}

	@Bean
	public SalaryPeriodConverter salaryToYearlyConverter() {
		return new SalaryPeriodConverter(Period.YEAR);
	}
}
