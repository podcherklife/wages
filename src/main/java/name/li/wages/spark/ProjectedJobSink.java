package name.li.wages.spark;

import java.io.Serializable;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.VoidFunction;

import name.li.wages.persistence.ProjectedJobEntryRepository;
import name.li.wages.projection.ProjectedJob;
import name.li.wages.projection.ProjectedJobEntry;

public class ProjectedJobSink implements VoidFunction<JavaRDD<ProjectedJob>>, Serializable {

	private static final long serialVersionUID = 1L;
	private final ServiceLocator locator;

	public ProjectedJobSink(ServiceLocator locator) {
		this.locator = locator;
	}

	@Override
	public void call(JavaRDD<ProjectedJob> t) throws Exception {
		t.foreach(e -> {
			var repo = locator.get(ProjectedJobEntryRepository.class);
			repo.save(ProjectedJobEntry.fromProjectedJob(e));
		});
	}

}
