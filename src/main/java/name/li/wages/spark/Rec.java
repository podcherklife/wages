package name.li.wages.spark;

import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.receiver.Receiver;

import name.li.wages.plugin.api.Salary;

public class Rec extends Receiver<Salary> {

	public Rec(StorageLevel storageLevel) {
		super(storageLevel);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void onStart() {
		new Thread(() -> {
			store(Salary.builder()
					.setMinSalary(100)
					.setMaxSalary(200)
					.setCurrency("foo")
					.yearly()
					.build());
		}).start();

	}

	@Override
	public void onStop() {

	}

}
