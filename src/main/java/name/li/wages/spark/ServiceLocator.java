package name.li.wages.spark;

public interface ServiceLocator {

	public <T> T get(Class<T> clazz);

}
