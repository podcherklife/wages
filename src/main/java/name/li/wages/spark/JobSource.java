package name.li.wages.spark;

import java.util.concurrent.CountDownLatch;

import org.apache.spark.api.java.StorageLevels;
import org.apache.spark.streaming.receiver.Receiver;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import name.li.wages.persistence.JobEntryRepository;
import name.li.wages.plugin.api.Job;

public class JobSource extends Receiver<Job> {

	private static final long serialVersionUID = 1L;

	private final ServiceLocator locator;
	private static CountDownLatch latch = new CountDownLatch(1);

	public JobSource(ServiceLocator locator) {
		super(StorageLevels.MEMORY_ONLY);
		this.locator = locator;
	}

	public CountDownLatch latch() {
		return latch;
	}

	@Override
	public void onStart() {
		new Thread(this::readDataIntoSpark).start();
	}

	public void readDataIntoSpark() {
		new TransactionTemplate(locator.get(PlatformTransactionManager.class))
				.executeWithoutResult(transaction -> {
					locator.get(JobEntryRepository.class).findAll().forEach(item -> {
						store(item.job());
					});
				});
		latch.countDown();
	}

	@Override
	public void onStop() {

	}

}
