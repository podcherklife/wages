package name.li.wages.spark;

import java.util.concurrent.CountDownLatch;

import org.apache.spark.SparkConf;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaStreamingContext;

import name.li.wages.projection.ProjectedJob;
import name.li.wages.projection.SalaryCurrencyConverter;
import name.li.wages.projection.SalaryPeriodConverter;
import scala.Tuple2;

public class SparkPipeline {

	public static void main(String[] args) throws InterruptedException {
		var conf = new SparkConf().setAppName("fooBar").setMaster("local[*]")
				.set("spark.kryo.registrator", "name.li.wages.spark.Registrator")
				.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");

		JavaStreamingContext ssc = new JavaStreamingContext(conf, new Duration(10_000));

		ssc.receiverStream(new Rec(StorageLevel.MEMORY_ONLY()))
				.map(e -> e.minSalary())
				.print();

		ssc.start();
		ssc.awaitTermination();
		ssc.close();
	}

	public void doSparkMagic(ServiceLocator locator) throws InterruptedException {
		var conf = new SparkConf()
				.setAppName("fooBar")
				.setMaster("local[*]")
				.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");

		JavaStreamingContext ssc = new JavaStreamingContext(conf, new Duration(10_000));

		var latch = custom(ssc, locator);

		ssc.start();
		latch.await();
		ssc.stop(true, true);

		ssc.awaitTermination();
	}

	public CountDownLatch custom(JavaStreamingContext ssc, ServiceLocator locator) {
		var source = new JobSource(locator);
		var jobs = ssc.receiverStream(source).mapToPair(e -> new Tuple2<>(e.externalId(), e));

		var salaries = jobs
				.mapValues(e -> e.salary())
				.mapValues(e -> e.map(salary -> locator.get(SalaryPeriodConverter.class).convert(salary)))
				.mapValues(e -> e.map(salary -> locator.get(SalaryCurrencyConverter.class).convert(salary)));

		var titles = jobs.mapValues(e -> e.position().toLowerCase());

		jobs
				.mapValues(e -> ProjectedJob.builder()
						.setBuzzwords(e.buzzwords())
						.setCompany(e.company())
						.setCreationDate(e.creationDate())
						.setExternalId(e.externalId())
						.setJobUrl(e.jobUrl())
						.setLocation(e.location())
						.setParsingProblems(e.parsingProblems())
						.setPosition(e.position())
						.setSalary(e.salary()))
				.join(salaries)
				.mapValues(e -> e._1.setSalary(e._2))
				.join(titles)
				.mapValues(e -> e._1.setPosition(e._2))
				.mapValues(e -> e.build())
				.map(e -> e._2)
				.foreachRDD(new ProjectedJobSink(locator));
		return source.latch();
	}

}
