package name.li.wages.spark;

import java.util.Optional;
import java.util.OptionalInt;

import org.apache.spark.serializer.KryoRegistrator;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.serializers.FieldSerializer;

public class Registrator implements KryoRegistrator {

	@Override
	public void registerClasses(Kryo kryo) {
		kryo.addDefaultSerializer(OptionalInt.class, FieldSerializer.class);
		kryo.addDefaultSerializer(Optional.class, FieldSerializer.class);
	}

}
