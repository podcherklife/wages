package name.li.wages.persistence;

import org.springframework.data.repository.CrudRepository;

import name.li.wages.projection.ProjectedJobEntry;

public interface ProjectedJobEntryRepository extends CrudRepository<ProjectedJobEntry, String> {
}
