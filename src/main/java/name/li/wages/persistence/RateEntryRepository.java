package name.li.wages.persistence;

import java.time.LocalDate;
import java.util.Optional;

import javax.money.CurrencyUnit;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import name.li.wages.projection.currency.RateEntity;

public interface RateEntryRepository extends CrudRepository<RateEntity, String> {

	@Query("select t from RateEntity t where t.from = ?1 and t.to = ?2 and t.date = ?3")
	Optional<RateEntity> getRate(CurrencyUnit from, CurrencyUnit to, LocalDate onDate);

}
