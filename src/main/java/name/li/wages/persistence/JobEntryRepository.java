package name.li.wages.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import name.li.wages.plugin.api.JobEntry;

public interface JobEntryRepository extends CrudRepository<JobEntry, String> {

	@Query("select t from JobEntry t where t.projectionToken != ?1")
	List<JobEntry> getAllEntriesWithDifferentProjectionTokenValue(String projectionToken);

}
