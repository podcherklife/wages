package name.li.wages.grapqhl.process;

import java.util.List;
import java.util.stream.Collectors;

import com.google.common.base.Predicates;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import name.li.wages.ProcessManager;
import name.li.wages.grapqhl.GraphQlConfiguration;
import name.li.wages.grapqhl.process.objectgraph.Process;

public class ProcessDataFetcher implements DataFetcher<List<Process>> {

	@Override
	public List<Process> get(DataFetchingEnvironment environment) throws Exception {
		ProcessManager pm = GraphQlConfiguration.resolve(ProcessManager.class);

		var idArg = environment.getArgument("id");

		return pm.allProcesses()
				.filter(idArg == null ? Predicates.alwaysTrue() : e -> e.id().equals(idArg))
				.map(Process::fromProcessDescriptor)
				.collect(Collectors.toList());
	}

}
