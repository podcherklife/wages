package name.li.wages.grapqhl.process.objectgraph;

import java.util.Collection;

import com.google.auto.value.AutoValue;

import graphql.annotations.annotationTypes.GraphQLDataFetcher;
import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLName;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import name.li.wages.grapqhl.process.ProcessDataFetcher;

@AutoValue
public abstract class Query {

	@GraphQLField
	@GraphQLDataFetcher(value = ProcessDataFetcher.class)
	@GraphQLNonNull
	public Collection<@GraphQLNonNull Process> processes(@GraphQLName("id") String id) {
		return null;
	}

	public abstract Collection<Process> processes();

	public static Builder builder() {
		return new AutoValue_Query.Builder();
	}

	@AutoValue.Builder
	public static abstract class Builder {

		public abstract Builder setProcesses(Collection<Process> processes);

		public abstract Query build();
	}
}
