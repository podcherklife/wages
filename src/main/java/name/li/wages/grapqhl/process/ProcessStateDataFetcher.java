package name.li.wages.grapqhl.process;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import name.li.wages.ProcessManager;
import name.li.wages.grapqhl.GraphQlConfiguration;
import name.li.wages.grapqhl.process.objectgraph.Process;
import name.li.wages.grapqhl.process.objectgraph.ProcessState;

public class ProcessStateDataFetcher implements DataFetcher<ProcessState> {

	@Override
	public ProcessState get(DataFetchingEnvironment environment) throws Exception {
		var pm = GraphQlConfiguration.resolve(ProcessManager.class);
		var actualStatus = pm.info(environment.<Process>getSource().id());
		return ProcessState.fromGeneratorStatus(actualStatus);
	}
}
