package name.li.wages.grapqhl.process.objectgraph;

import graphql.annotations.annotationTypes.GraphQLDataFetcher;
import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLName;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import name.li.wages.grapqhl.process.ManageProcessDataFetcher;
import name.li.wages.grapqhl.process.ManageProcessDataFetcher.ProcessAction;

public class Mutation {

	@GraphQLField
	@GraphQLNonNull
	@GraphQLDataFetcher(ManageProcessDataFetcher.class)
	public Process manageProcess(@GraphQLName("id") String id, @GraphQLName("action") ProcessAction action) {
		return null;
	}
}
