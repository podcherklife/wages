package name.li.wages.grapqhl.process.objectgraph;

import java.util.Collection;

import com.google.auto.value.AutoValue;

import graphql.annotations.annotationTypes.GraphQLDataFetcher;
import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLID;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import name.li.wages.grapqhl.process.ProcessHistoryDataFetcher;
import name.li.wages.grapqhl.process.ProcessStateDataFetcher;
import name.li.wages.plugin.api.process.ProcessDescriptor;

@AutoValue
public abstract class Process {

	@GraphQLField
	@GraphQLID
	@GraphQLNonNull
	public abstract String id();

	@GraphQLField
	@GraphQLNonNull
	public abstract String description();

	@GraphQLField
	@GraphQLDataFetcher(ProcessStateDataFetcher.class)
	public ProcessState state() {
		return null;
	}

	@GraphQLField
	@GraphQLNonNull
	@GraphQLDataFetcher(ProcessHistoryDataFetcher.class)
	public Collection<@GraphQLNonNull ProcessGeneration> history() {
		return null;
	}

	public static Builder builder() {
		return new AutoValue_Process.Builder();
	}

	public static Process fromProcessDescriptor(ProcessDescriptor pd) {
		return Process.builder().setId(pd.id()).setDescription(pd.id()).build();
	}

	@AutoValue.Builder
	public static abstract class Builder {

		public abstract Builder setId(String id);

		public abstract Builder setDescription(String description);

		public abstract Process build();
	}
}
