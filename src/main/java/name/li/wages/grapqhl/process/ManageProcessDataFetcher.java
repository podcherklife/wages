package name.li.wages.grapqhl.process;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import name.li.wages.ProcessManager;
import name.li.wages.grapqhl.GraphQlConfiguration;
import name.li.wages.grapqhl.process.objectgraph.Process;

public class ManageProcessDataFetcher implements DataFetcher<Process> {

	public enum ProcessAction {
		CREATE_NEW_OR_CONTINUE_FAILED_EXECUTION,
		START_PENDING_EXECUTION,
		CANCEL_PENDING_EXECUTION,
		TRY_STOP_EXECUTION_IN_PROGRESS,
	}

	@Override
	public Process get(DataFetchingEnvironment environment) throws Exception {
		ProcessManager pm = GraphQlConfiguration.resolve(ProcessManager.class);

		var action = environment.<ProcessAction>getArgument("action");
		var id = environment.<String>getArgument("id");

		if (action == ProcessAction.CREATE_NEW_OR_CONTINUE_FAILED_EXECUTION) {
			pm.tryContinueExecution(id);
		} else if (action == ProcessAction.CANCEL_PENDING_EXECUTION) {
			pm.cancelUnfinishedExecution(id);
		} else if (action == ProcessAction.START_PENDING_EXECUTION) {
			pm.startExecution(id);
		} else if (action == ProcessAction.TRY_STOP_EXECUTION_IN_PROGRESS) {
			throw new UnsupportedOperationException("Not implemented");
		}
		return Process
				.fromProcessDescriptor(
						pm.allProcesses().filter(e -> e.id().equals(environment.getArgument("id"))).findFirst().get());
	}

}
