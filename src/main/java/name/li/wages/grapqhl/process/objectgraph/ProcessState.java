package name.li.wages.grapqhl.process.objectgraph;

import java.util.Optional;

import javax.annotation.Nullable;

import com.google.auto.value.AutoValue;

import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import name.li.wages.plugin.api.GeneratorStatus;

@AutoValue
public abstract class ProcessState {

	public enum ProcessStatus {
		NO_ACTIVE_EXECUTIONS,
		EXECUTION_PENDING,
		EXECUTION_IN_PROGRESS
	}

	@GraphQLField
	@GraphQLNonNull
	public abstract ProcessStatus generalStatus();

	@GraphQLField
	@Nullable
	public abstract ExecutionStepDetails details();

	public static Builder builder() {
		return new AutoValue_ProcessState.Builder();
	}

	public static ProcessState fromGeneratorStatus(Optional<GeneratorStatus> generatorStatus) {
		return generatorStatus.map(status -> {
			if (status.working()) {
				return ProcessState.builder().setGeneralStatus(ProcessStatus.EXECUTION_IN_PROGRESS)
						.setDetails(ExecutionStepDetails.fromGeneratorStatus(status));
			} else {
				return ProcessState.builder().setGeneralStatus(ProcessStatus.EXECUTION_PENDING)
						.setDetails(ExecutionStepDetails.fromGeneratorStatus(status));
			}
		}).orElse(ProcessState.builder().setGeneralStatus(ProcessStatus.NO_ACTIVE_EXECUTIONS)).build();
	}

	@AutoValue.Builder
	public static abstract class Builder {

		public abstract Builder setDetails(ExecutionStepDetails details);

		public abstract Builder setGeneralStatus(ProcessStatus status);

		public abstract ProcessState build();
	}

}
