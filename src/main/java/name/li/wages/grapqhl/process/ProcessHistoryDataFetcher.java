package name.li.wages.grapqhl.process;

import java.util.Collection;
import java.util.stream.Collectors;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import name.li.wages.grapqhl.GraphQlConfiguration;
import name.li.wages.grapqhl.process.objectgraph.Process;
import name.li.wages.grapqhl.process.objectgraph.ProcessGeneration;
import name.li.wages.persistence.GenerationRepository;

public class ProcessHistoryDataFetcher implements DataFetcher<Collection<ProcessGeneration>> {

	@Override
	public Collection<ProcessGeneration> get(DataFetchingEnvironment environment) throws Exception {
		var generationsRepository = GraphQlConfiguration.resolve(GenerationRepository.class);
		return generationsRepository.getGenerations(environment.<Process>getSource().id())
				.stream()
				.map(ProcessGeneration::fromGeneration)
				.collect(Collectors.toList());
	}

}
