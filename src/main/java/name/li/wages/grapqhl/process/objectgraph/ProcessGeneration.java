package name.li.wages.grapqhl.process.objectgraph;

import java.util.Collection;
import java.util.stream.Collectors;

import com.google.auto.value.AutoValue;

import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLID;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.Generation.Status;

@AutoValue
public abstract class ProcessGeneration {

	@GraphQLID
	@GraphQLField
	@GraphQLNonNull
	public abstract String id();

	@GraphQLField
	@GraphQLNonNull
	public abstract String description();

	@GraphQLField
	@GraphQLNonNull
	public abstract Status status();

	@GraphQLField
	@GraphQLNonNull
	public abstract String date();

	@GraphQLField
	@GraphQLNonNull
	public abstract Collection<ProcessGenerationAttempt> attempts();

	public static Builder builder() {
		return new AutoValue_ProcessGeneration.Builder();
	}

	public static ProcessGeneration fromGeneration(Generation generation) {
		return ProcessGeneration.builder()
				.setId(generation.getId())
				.setDate(generation.getDate().toString())
				.setDescription(generation.getDescription())
				.setStatus(generation.getStatus())
				.setAttempts(generation.getAttempts().stream()
						.map(ProcessGenerationAttempt::fromGenerationAttempt)
						.collect(Collectors.toList()))
				.build();
	}

	@AutoValue.Builder
	public static abstract class Builder {

		public abstract Builder setId(String id);

		public abstract Builder setDescription(String description);

		public abstract Builder setDate(String date);

		public abstract Builder setStatus(Status status);

		public abstract Builder setAttempts(Collection<ProcessGenerationAttempt> attempts);

		public abstract ProcessGeneration build();
	}

}
