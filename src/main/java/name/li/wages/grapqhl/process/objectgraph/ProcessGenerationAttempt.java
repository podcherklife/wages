package name.li.wages.grapqhl.process.objectgraph;

import com.google.auto.value.AutoValue;

import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLID;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import name.li.wages.plugin.api.GenerationAttempt;

@AutoValue
public abstract class ProcessGenerationAttempt {

	@GraphQLID
	@GraphQLField
	@GraphQLNonNull
	public abstract String id();

	@GraphQLField
	@GraphQLNonNull
	public abstract String description();

	@GraphQLField
	@GraphQLNonNull
	public abstract String date();

	public static Builder builder() {
		return new AutoValue_ProcessGenerationAttempt.Builder();
	}

	public static ProcessGenerationAttempt fromGenerationAttempt(GenerationAttempt attempt) {
		return ProcessGenerationAttempt.builder()
				.setId(attempt.getId())
				.setDescription(attempt.getDescription())
				.setDate(attempt.getDate().toString())
				.build();
	}

	@AutoValue.Builder
	public static abstract class Builder {

		public abstract Builder setId(String id);

		public abstract Builder setDescription(String description);

		public abstract Builder setDate(String date);

		public abstract ProcessGenerationAttempt build();
	}

}
