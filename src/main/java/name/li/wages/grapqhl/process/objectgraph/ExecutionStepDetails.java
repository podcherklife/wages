package name.li.wages.grapqhl.process.objectgraph;

import java.util.Collection;
import java.util.stream.Collectors;

import com.google.auto.value.AutoValue;

import graphql.annotations.annotationTypes.GraphQLField;
import graphql.annotations.annotationTypes.GraphQLNonNull;
import name.li.wages.plugin.api.GeneratorStatus;

@AutoValue
public abstract class ExecutionStepDetails {

	@GraphQLField
	@GraphQLNonNull
	public abstract String description();

	@GraphQLField
	@GraphQLNonNull
	public abstract Collection<@GraphQLNonNull ExecutionStepDetails> substeps();

	@GraphQLField
	@GraphQLNonNull
	public abstract int percentsCompleted();

	@GraphQLField
	@GraphQLNonNull
	public abstract boolean working();

	public static Builder builder() {
		return new AutoValue_ExecutionStepDetails.Builder();
	}

	public static ExecutionStepDetails fromGeneratorStatus(GeneratorStatus status) {
		return ExecutionStepDetails.builder()
				.setDescription(status.description())
				.setPercentsCompleted(status.percentsCompleted())
				.setWorking(status.working())
				.setSubsteps(
						status.substatuses().stream().map(ExecutionStepDetails::fromGeneratorStatus)
								.collect(Collectors.toList()))
				.build();
	}

	@AutoValue.Builder
	public static abstract class Builder {

		public abstract Builder setDescription(String description);

		public abstract Builder setWorking(boolean working);

		public abstract Builder setSubsteps(Collection<ExecutionStepDetails> substeps);

		public abstract Builder setPercentsCompleted(int percent);

		public abstract ExecutionStepDetails build();
	}
}
