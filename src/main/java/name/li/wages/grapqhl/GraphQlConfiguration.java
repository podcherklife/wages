package name.li.wages.grapqhl;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import graphql.GraphQL;
import graphql.annotations.AnnotationsSchemaCreator;
import name.li.wages.grapqhl.process.objectgraph.Mutation;
import name.li.wages.grapqhl.process.objectgraph.Query;

@Configuration
public class GraphQlConfiguration {

	public static volatile ApplicationContext appContext;

	public GraphQlConfiguration(ApplicationContext context) {
		appContext = context;
	}

	public static <T> T resolve(Class<T> clazz) {
		return appContext.getBean(clazz);
	}

	@Bean
	public GraphQL graphQl() {
		var annotations = AnnotationsSchemaCreator.newAnnotationsSchema();
		var schema = annotations
				.query(Query.class)
				.mutation(Mutation.class)
				.build();
		return GraphQL.newGraphQL(schema).build();
	}
	
	
	// XXX: graphql.servlet.corsEnabled=true doesnt seem to work
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/graphql");
            }
        };
    }

}
