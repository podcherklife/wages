# Wages

### Running without dependencies

0. Have Java 13 installed
1. Checkout source
2. Build application, skip tests: `./mvnw clean package -Dmaven.test.skip=true`
3. Run with in-memory database `java -jar -Dspring.profiles.active=inmemory target/wages.jar`